<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);
$customerName = $comiteg->getTableValue('id,ragione_sociale', 'customers', 'id', 'ragione_sociale', $_GET['customer_id']);

if (isset($_GET['customer_id'])) {
$customer_id = $_GET['customer_id'];
$title_page = 'Modifica Cliente';
$cName = ' (# '.$_GET['customer_id'].') | <strong>'.$customerName.'</strong>';
}
else if (!isset($_GET['customer_id'])) {
//$customer_id = $comiteg->getNewCustomerId($user_id);

$customer_id = NULL;
$title_page = 'Aggiungi nuovo Cliente';
$cName = '';
}
//var_dump($customer_id);
if ($customer_id == '' && isset($_GET['customer_id'])) {
?>
<script type="text/javascript">
window.location.replace('/customers.php');
</script>
<?php
}

if (isset($_GET['customer_id']) && !isset($_POST['submit'])) {
$array_customer = $comiteg->getCustomer($customer_id);

$ragione_sociale = $array_customer['ragione_sociale'];
$nome = $array_customer['nome'];
$cognome = $array_customer['cognome'];
$indirizzo = $array_customer['indirizzo'];
$cap = $array_customer['cap'];
$prov = $array_customer['prov'];
$citta = $array_customer['citta'];
$p_iva = $array_customer['p_iva'];
$ali_iva = $array_customer['ali_iva'];
$auto_notifica = $array_customer['auto_notifica'];
$cod_fisc = $array_customer['cod_fisc'];
$cod_ipa = $array_customer['cod_ipa'];
$cad_fattura = $array_customer['cad_fattura'];
$note_fattura = $array_customer['note_fattura'];
$appunti = $array_customer['appunti'];
$note_testata = $array_customer['note_testata'];
$email = $array_customer['email'];
$email_amministr = $array_customer['email_amministr'];
$telefono_1 =  $array_customer['telefono_1'];
$telefono_2 = $array_customer['telefono_2'];
$indirizzo_1 = $array_customer['indirizzo_1'];
$indirizzo_2 = $array_customer['indirizzo_2'];
$indirizzo_3 = $array_customer['indirizzo_3'];
$indirizzo_4 = $array_customer['indirizzo_4'];
$indirizzo_5 = $array_customer['indirizzo_5'];
}
else if (isset($_POST['submit'])) {
$ragione_sociale = $_POST['ragione_sociale'];
$nome = $_POST['nome'];
$cognome = $_POST['cognome'];
$indirizzo = $_POST['indirizzo'];
$cap = $_POST['cap'];
$prov = $_POST['prov'];
$citta = $_POST['citta'];
$p_iva = $_POST['p_iva'];
$ali_iva = $_POST['ali_iva'];
$auto_notifica = $_POST['auto_notifica'];
$cod_fisc = $_POST['cod_fisc'];
$cod_ipa = $_POST['cod_ipa'];
$cad_fattura = $_POST['cad_fattura'];
$note_fattura = $_POST['note_fattura'];
$appunti = $_POST['appunti'];
$note_testata = $_POST['note_testata'];
$email = $_POST['email'];
$email_amministr = $_POST['email_amministr'];
$telefono_1 =  $_POST['telefono_1'];
$telefono_2 =  $_POST['telefono_2'];
$indirizzo_1 = $_POST['indirizzo_1_1'].' * '.$_POST['indirizzo_1_2'].' * '.$_POST['indirizzo_1_3'].' * '.$_POST['indirizzo_1_4'].' * '.$_POST['indirizzo_1_5'];
$indirizzo_2 = $_POST['indirizzo_2_1'].' * '.$_POST['indirizzo_2_2'].' * '.$_POST['indirizzo_2_3'].' * '.$_POST['indirizzo_2_4'].' * '.$_POST['indirizzo_2_5'];
$indirizzo_3 = $_POST['indirizzo_3_1'].' * '.$_POST['indirizzo_3_2'].' * '.$_POST['indirizzo_3_3'].' * '.$_POST['indirizzo_3_4'].' * '.$_POST['indirizzo_3_5'];
$indirizzo_4 = $_POST['indirizzo_4_1'].' * '.$_POST['indirizzo_4_2'].' * '.$_POST['indirizzo_4_3'].' * '.$_POST['indirizzo_4_4'].' * '.$_POST['indirizzo_4_5'];
$indirizzo_5 = $_POST['indirizzo_5_1'].' * '.$_POST['indirizzo_5_2'].' * '.$_POST['indirizzo_5_3'].' * '.$_POST['indirizzo_5_4'].' * '.$_POST['indirizzo_5_5'];
}

$array_customer = array('customer_id' => $customer_id,
                        'user_id' => $user_id,
                        'ragione_sociale' => $ragione_sociale,
                        'nome' => $nome,
                        'cognome' => $cognome,
                        'indirizzo' => $indirizzo,
                        'cap' => $cap,
                        'prov' => $prov,
                        'citta' => $citta,
                        'p_iva' => $p_iva,
                        'ali_iva' => $ali_iva,
                        'auto_notifica' => $auto_notifica,
                        'cod_fisc' => $cod_fisc,
                        'cod_ipa' => $cod_ipa,
                        'cad_fattura' => $cad_fattura,
                        'note_fattura' => $note_fattura,
                        'appunti' => $appunti,
                        'note_testata' => $note_testata,
                        'email' => $email,
                        'email_amministr' => $email_amministr,
                        'telefono_1' => $telefono_1,
                        'telefono_2' => $telefono_2,
                        'indirizzo_1' => $indirizzo_1,
                        'indirizzo_2' => $indirizzo_2,
                        'indirizzo_3' => $indirizzo_3,
                        'indirizzo_4' => $indirizzo_4,
                        'indirizzo_5' => $indirizzo_5
                        );
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Clienti</title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>
  <div id="main_container">

<?php
if (isset($_GET['customer_id'])) {
?>
  <div id="wrapper">

  <?php
  $active_page = '/new_customer.php';
  require_once('templates/new_customer_left.php');
  ?>

<?php
$backlink = $_SERVER['HTTP_REFERER'];
}
else {
$backlink = $_SERVER['HTTP_REFERER'];
}
?>
<div class="anchor_customer" id="anagrafica">

    <div class="col-xs-12 breadcrumb_category">

        <a href="<?php echo $backlink ?>">
        <button class="btn btn-default button_back">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
        </a>


      <?php
      echo $title_page.$cName;
      ?>
    </div>
    <div class="col-md-6 col-xs-12" style="padding:0">
    <form id="save_customer" method="POST" action="/new_customer.php?customer_id=<?php echo $customer_id ?>">
      <div class="col-xs-12 input_customers">
        <strong>DATI SOCIETARI</strong>
      </div>
      <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
      <div class="col-xs-12 input_customers">
        <label class="small_label_form">RAGIONE SOCIALE</label>
        <input id="ragione_sociale" class="input_modal" type="text" value="<?php echo $ragione_sociale ?>" name="ragione_sociale">
      </div>
      <div class="col-xs-12 col-md-6 input_customers">
      <label class="small_label_form">NOME</label>
        <input class="input_modal" type="text" value="<?php echo $nome ?>" name="nome">
      </div>
      <div class="col-xs-12 col-md-6 input_customers">
      <label class="small_label_form">COGNOME</label>
        <input class="input_modal" type="text" value="<?php echo $cognome ?>" name="cognome">
      </div>
      <div class="col-xs-12 input_customers">
      <label class="small_label_form">INDIRIZZO</label>
        <input class="input_modal" type="text" value="<?php echo $indirizzo ?>" name="indirizzo">
      </div>
      <div class="col-xs-12 col-md-4 input_customers">
      <label class="small_label_form">C.A.P.</label>
        <input class="input_modal" type="text" value="<?php echo $cap ?>" name="cap">
      </div>
      <div class="col-xs-12 col-md-4 input_customers">
      <label class="small_label_form">PROVINCIA</label>
        <input class="input_modal" type="text" value="<?php echo $prov ?>" name="prov">
      </div>
      <div class="col-xs-12 col-md-4 input_customers">
      <label class="small_label_form">CITT&Agrave;</label>
        <input class="input_modal" type="text" value="<?php echo $citta ?>" name="citta">
      </div>
      <div class="col-xs-12 col-md-6 input_customers">
      <label class="small_label_form">PARTITA IVA</label>
        <input class="input_modal" type="text" value="<?php echo $p_iva ?>" name="p_iva">
      </div>
      <div class="col-xs-12 col-md-6 input_customers">
      <label class="small_label_form">ALIQUOTA IVA</label>
        <input class="input_modal" type="text" value="<?php echo $ali_iva ?>" name="ali_iva">
      </div>
      <!--div class="col-xs-12 col-md-4 input_customers">
      <label class="small_label_form">NOTIFICA AUTO FATTURA</label>
        <select name="auto_notifica" class="input_modal">
          <option <?php //if ($auto_notifica == 0) echo 'selected="selected"' ?> value="0">No</option>
          <option <?php //if ($auto_notifica == 1) echo 'selected="selected"' ?> value="1">Sì</option>
        </select>
      </div-->
      <div style="clear:both"></div>
      <div class="col-xs-12 col-md-6 input_customers">
      <label class="small_label_form">COD. FISCALE</label>
        <input class="input_modal" type="text" value="<?php echo $cod_fisc ?>" name="cod_fisc">
      </div>
      <div class="col-xs-12 col-md-6 input_customers">
      <label class="small_label_form">COD. IPA</label>
        <input class="input_modal" type="text" value="<?php echo $cod_ipa ?>" name="cod_ipa">
      </div>
      <!--div class="col-xs-12 col-md-4 input_customers">
      <label class="small_label_form">PERIODO FATTURA</label>
        <input class="input_modal" type="text" value="<?php //echo $cad_fattura ?>" name="cad_fattura">
      </div-->
      <div class="col-xs-12 input_customers">
      <label class="small_label_form">NOTE FATTURA</label>
        <textarea class="input_modal" rows="5" name="note_fattura"><?php echo $note_fattura ?></textarea>
      </div>
      <div class="col-xs-12 input_customers">
      <label class="small_label_form">APPUNTI</label>
        <textarea class="input_modal" rows="5" name="appunti"><?php echo $appunti ?></textarea>
      </div>
    </div>
    <div class="col-md-6 col-xs-12" style="padding:0">

      <div class="col-xs-12 col-md-12 input_customers">
      <label class="small_label_form">NOTE CORRIERE (max 30 caratteri)</label>
        <input class="input_modal" type="text" value="<?php echo $note_testata ?>" name="note_testata">
      </div>

      <div class="col-xs-12 input_customers">
        <strong>CONTATTI</strong>
      </div>
      <div class="col-xs-12 col-md-6 input_customers">
      <label class="small_label_form">EMAIL PRINCIPALE</label>
        <input class="input_modal" type="email" value="<?php echo $email ?>" name="email">
      </div>
      <div class="col-xs-12 col-md-6 input_customers">
      <label class="small_label_form">EMAIL AMMINISTRAZIONE</label>
        <input class="input_modal" type="email" value="<?php echo $email_amministr ?>" name="email_amministr">
      </div>
      <div class="col-xs-12 col-md-6 input_customers">
      <label class="small_label_form">TELEFONO #1</label>
        <input class="input_modal" type="text" value="<?php echo $telefono_1 ?>" name="telefono_1">
      </div>
      <div class="col-xs-12 col-md-6 input_customers">
      <label class="small_label_form">TELEFONO #2</label>
        <input class="input_modal" type="text" value="<?php echo $telefono_2 ?>" name="telefono_2">
      </div>

      <div class="col-xs-12 input_customers">
        <strong>INDIRIZZI DI SPEDIZIONE</strong>
      </div>

      <div class="input_customers">
      <div class="col-xs-12">
      <label class="small_label_form"><strong>INDIRIZZO DI SPEDIZIONE #1</strong></label>
      </div>
      <div class="col-xs-12 col-md-4" style="padding-right:0">
         <label class="small_label_form">INDIRIZZO</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_1)[0]) ?>" name="indirizzo_1_1">
      </div>
      <div class="col-xs-12 col-md-2" style="padding-right:0">
         <label class="small_label_form">CAP</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_1)[1]) ?>" name="indirizzo_1_2">
      </div>
      <div class="col-xs-12 col-md-4">
         <label class="small_label_form">CITT&Agrave;</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_1)[2]) ?>" name="indirizzo_1_3">
      </div>
      <div class="col-xs-12 col-md-1" style="padding-left:0">
         <label class="small_label_form">PROV.</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_1)[3]) ?>" name="indirizzo_1_4">
      </div>
      <div class="col-xs-12 col-md-1" style="padding-left:0">
         <label class="small_label_form">NAZ.</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_1)[4]) ?>" name="indirizzo_1_5">
      </div>
      </div>

      <div class="input_customers">
      <div class="col-xs-12">
      <label class="small_label_form"><strong>INDIRIZZO DI SPEDIZIONE #2</strong></label>
      </div>
      <div class="col-xs-12 col-md-4" style="padding-right:0">
         <label class="small_label_form">INDIRIZZO</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_2)[0]) ?>" name="indirizzo_2_1">
      </div>
      <div class="col-xs-12 col-md-2" style="padding-right:0">
         <label class="small_label_form">CAP</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_2)[1]) ?>" name="indirizzo_2_2">
      </div>
      <div class="col-xs-12 col-md-4">
         <label class="small_label_form">CITT&Agrave;</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_2)[2]) ?>" name="indirizzo_2_3">
      </div>
      <div class="col-xs-12 col-md-1" style="padding-left:0">
         <label class="small_label_form">PROV.</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_2)[3]) ?>" name="indirizzo_2_4">
      </div>
      <div class="col-xs-12 col-md-1" style="padding-left:0">
         <label class="small_label_form">NAZ.</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_2)[4]) ?>" name="indirizzo_2_5">
      </div>
      </div>

      <div class="input_customers">
      <div class="col-xs-12">
      <label class="small_label_form"><strong>INDIRIZZO DI SPEDIZIONE #3</strong></label>
      </div>
      <div class="col-xs-12 col-md-4" style="padding-right:0">
         <label class="small_label_form">INDIRIZZO</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_3)[0]) ?>" name="indirizzo_3_1">
      </div>
      <div class="col-xs-12 col-md-2" style="padding-right:0">
         <label class="small_label_form">CAP</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_3)[1]) ?>" name="indirizzo_3_2">
      </div>
      <div class="col-xs-12 col-md-4">
         <label class="small_label_form">CITT&Agrave;</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_3)[2]) ?>" name="indirizzo_3_3">
      </div>
      <div class="col-xs-12 col-md-1" style="padding-left:0">
         <label class="small_label_form">PROV.</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_3)[3]) ?>" name="indirizzo_3_4">
      </div>
      <div class="col-xs-12 col-md-1" style="padding-left:0">
         <label class="small_label_form">NAZ.</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_3)[4]) ?>" name="indirizzo_3_5">
      </div>
      </div>

      <div class="input_customers">
      <div class="col-xs-12">
      <label class="small_label_form"><strong>INDIRIZZO DI SPEDIZIONE #4</strong></label>
      </div>
      <div class="col-xs-12 col-md-4" style="padding-right:0">
         <label class="small_label_form">INDIRIZZO</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_4)[0]) ?>" name="indirizzo_4_1">
      </div>
      <div class="col-xs-12 col-md-2" style="padding-right:0">
         <label class="small_label_form">CAP</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_4)[1]) ?>" name="indirizzo_4_2">
      </div>
      <div class="col-xs-12 col-md-4">
         <label class="small_label_form">CITT&Agrave;</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_4)[2]) ?>" name="indirizzo_4_3">
      </div>
      <div class="col-xs-12 col-md-1" style="padding-left:0">
         <label class="small_label_form">PROV.</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_4)[3]) ?>" name="indirizzo_4_4">
      </div>
      <div class="col-xs-12 col-md-1" style="padding-left:0">
         <label class="small_label_form">NAZ.</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_4)[4]) ?>" name="indirizzo_4_5">
      </div>
      </div>

      <div class="input_customers">
      <div class="col-xs-12">
      <label class="small_label_form"><strong>INDIRIZZO DI SPEDIZIONE #5</strong></label>
      </div>
      <div class="col-xs-12 col-md-4" style="padding-right:0">
         <label class="small_label_form">INDIRIZZO</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_5)[0]) ?>" name="indirizzo_5_1">
      </div>
      <div class="col-xs-12 col-md-2" style="padding-right:0">
         <label class="small_label_form">CAP</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_5)[1]) ?>" name="indirizzo_5_2">
      </div>
      <div class="col-xs-12 col-md-4">
         <label class="small_label_form">CITT&Agrave;</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_5)[2]) ?>" name="indirizzo_5_3">
      </div>
      <div class="col-xs-12 col-md-1" style="padding-left:0">
         <label class="small_label_form">PROV.</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_5)[3]) ?>" name="indirizzo_5_4">
      </div>
      <div class="col-xs-12 col-md-1" style="padding-left:0">
         <label class="small_label_form">NAZ.</label>
         <input class="input_modal" type="text" value="<?php echo trim(explode('*', $indirizzo_5)[4]) ?>" name="indirizzo_5_5">
      </div>
      </div>

      <div class="col-xs-12">
        <?php
        if (isset($_GET['customer_id'])) {
        ?>
        <a href="/new_customer.php">
        <button style="float:left;margin-top:10px;" class="btn btn-default" type="button">AGGIUNGI UN NUOVO CLIENTE</button>
        </a>
        <?php
        }
        ?>
        <button style="float:right;margin-top:10px;" class="btn btn-default" type="submit" name="submit">SALVA</button>
      </div>
    </form>

    <div class="col-xs-12" style="text-align:center">
    <?php
    if (isset($_POST['submit'])) {
    $comiteg->addUpdateCustomer($array_customer);
    /*echo '<pre>';
    print_r($array_customer);
    echo '</pre>';*/
    }
    ?>
    </div>
    </div>
</div>

<!--div class="anchor_customer" id="carrelli" style="display:none">

    <div class="col-xs-12 breadcrumb_category">

        <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">

          <i class="fa fa-chevron-left" aria-hidden="true"></i>

        </button>
        </a>
      Carrelli Cliente <?php echo $cName ?>
    </div-->
<?php
$link_page = '/saved_carts.php';

$_saved_carts_collection = $comiteg->getSavedOrdersCustomer('saved_carts', 'saved_cart_items', $user_id, $_GET['customer_id']);
?>
<!--div class="col-md-12 scrolling_x_cart">
<div class="fixed_width_700">
          <div class="col-md-1 col-xs-1 cart_label_tab" style="padding-left:0;text-align:right">
          CAR. #
          </div>
          <div class="col-md-4 col-xs-4">
          CLIENTE
          </div>
          <div class="col-md-2 col-xs-2" style="text-align:right">
          IMPORTO
          </div>
          <div class="col-md-3 col-xs-3" style="text-align:center">
          DATA
          </div>
          <div class="col-md-1 col-xs-1">
          &nbsp;
          </div>
          <div class="col-md-1 col-xs-1">
          &nbsp;
          </div>
          <div style="clear:both"></div>
          <hr class="hr_thin">
<form method="POST" action="" class="saved_carts_form saved_carts_form_saved" id="saved_carts_form">
<input type="hidden" name="table_1" value="saved_carts">
<input type="hidden" name="table_2" value="saved_cart_items">
<input type="hidden" name="is_page" id="is_page" value="<?php echo $link_page ?>">
<input type="hidden" name="user_id" value="<?php echo $user_id ?>">
<input type="hidden" name="customer_id" value="<?php echo $_GET['customer_id'] ?>">
<div id="savedCartsContent_saved">
<?php include('cart/list_saved_carts.php') ?>
</div>
</form>

</div>
</div>

</div-->








<?php
if (isset($_GET['customer_id'])) { // wrapper
?>
  </div>
<?php
}
?>


  </div>
  <div id="refresh_saved_cart_orders" style="display:none"></div>
  <div id="refresh_saved_cart_estimates" style="display:none"></div>
  <div id="refresh_saved_cart_saved" style="display:none"></div>
  <?php //require_once('templates/ajax_cart_message.php') // contiene il popup della risposta e il form per database ?>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>
<?php
if (!isset($_GET['customer_id'])) {
?>
<script type="text/javascript">
// cancella reserved_id nella tabella customers
$(window).bind('beforeunload', function() {
$.ajax({
url: '/customers/remove_reserved_id.php',
async:false
});
});

$('form#save_customer').submit(function() {
   $(window).unbind('beforeunload');
});

</script>
<?php
}
?>
<script type="text/javascript">
$(function() {
    $(document).ajaxComplete(function(){
        $('#update_right_bar').trigger('click');
    });
});
</script>
</body>
</html>
