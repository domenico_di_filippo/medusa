<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);
$customerName = $comiteg->getTableValue('id,ragione_sociale', 'customers', 'id', 'ragione_sociale', $_GET['customer_id']);
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Prodotti da fatturare a <?php echo $customerName ?></title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>
  <div id="main_container">

  <div id="wrapper">

  <?php
  $active_page = '/customer_to_invoice.php';
  require_once('templates/new_customer_left.php');
  ?>

<?php
$link_page = '/invoices.php';
$_items_collection = $comiteg->getItemsForInvoice($_GET['customer_id'], $user_id);
$customer_tax = $comiteg->getTableValue('id,ali_iva', 'customers', 'id', 'ali_iva', $_GET['customer_id']);
?>

<div class="anchor_customer" id="invoiceproducts">

    <div class="col-xs-12 breadcrumb_category">

        <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">

          <i class="fa fa-chevron-left" aria-hidden="true"></i>

        </button>
        </a>
      Prodotti da fatturare a <strong><?php echo $customerName ?></strong> (<?php echo count($_items_collection) ?>)
    </div>

<div class="col-xs-12">
<div class="col-md-9 scrolling_x_cart" style="padding-left:0">
<div class="fixed_width_700">

<form method="POST" action="" class="saved_carts_form saved_carts_form_toinvoice" id="saved_carts_form">
<input type="hidden" name="table_1" value="saved_invoices">
<input type="hidden" name="table_2" value="saved_invoice_items">
<input type="hidden" name="is_page" id="is_page" value="<?php echo $link_page ?>">
<input type="hidden" name="user_id" value="<?php echo $user_id ?>">
<input type="hidden" name="customer_id" value="<?php echo $_GET['customer_id'] ?>">
<input type="hidden" id="customer_iva" name="customer_iva" value="<?php echo $customer_tax ?>">
<input type="hidden" name="email_send_to_invoice" id="email_send_to_invoice" value="">
<input type="hidden" name="email_send_me_invoice" id="email_send_me_invoice" value="">
<div id="savedCartsContent_toinvoice">
<?php
include('cart/list_to_invoice_items.php');
?>
</div>
</form>


<?php
// risposta prodotto aggiunto al carrello.
?>
<div id="save_invoice" class="modal fade modal_ok" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">

        <h4 class="modal-title cart_added_msg">Stampa Fattura o invia tramite email.</h4>
      </div>
      <div class="modal-body">

      <?php
      echo $comiteg->getCustomerEmail($customer_id, $user_id);
      ?>

      <input type="checkbox" name="check_my_mail" id="check_my_mail" value="1">
      <label for="check_my_mail">Invia una copia alla mia email.</label>


      <div id="get_invoice" style="margin-top:15px;float:right"></div>
      </div>
      <div class="modal-footer">

        <button type="button" id="stampa_invoice_chiudi" class="btn btn-default stampa_invoice_chiudi" data-dismiss="modal">Chiudi</button>

        <button type="button" id="stampa_invoice" class="btn btn-default">Crea Fattura e mandala al Cliente</button>

      </div>
    </div>
  </div>
</div>


<div id="save_invoice_black" class="modal fade modal_ok" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">

        <h4 class="modal-title cart_added_msg">Stampa Pro Forma o invia tramite email.</h4>
      </div>
      <div class="modal-body">

      <?php
      echo $comiteg->getCustomerEmailNoInvoice($customer_id, $user_id);
      ?>

      <input type="checkbox" name="check_my_mail_black" id="check_my_mail_black" value="1">
      <label for="check_my_mail_black">Invia una copia alla mia email.</label>


      <div id="get_invoice_black" style="margin-top:15px;float:right"></div>
      </div>
      <div class="modal-footer">

        <button type="button" id="stampa_invoice_chiudi_black" class="btn btn-default stampa_invoice_chiudi" data-dismiss="modal">Chiudi</button>

        <button type="button" id="stampa_invoice_black" class="btn btn-default">Crea Pro Forma e manda al Cliente</button>

      </div>
    </div>
  </div>
</div>

</div>
</div>

<div class="col-md-3">
  <?php
  if (!is_array($totalPrice)) {
  $totalPrice = array();
  }
  ?>
  <div class="col-xs-12 totals_cart_block effect2">

        SUBTOTALE
        <div class="cart_block_price_container"><strong class="totals_cart_table" id="update_subtotal">€ <?php echo number_format(array_sum($totalPrice), 2, '.', '') ?></strong></div>
        <div class="cart_block_price_container_label">
          IVA
          <span id="update_ali_iva">
          <?php
          if ($customer_tax !== NULL) {
          echo '&nbsp;('.$customer_tax.'%)';
          }
          ?>
          </span>
        </div>

        <div class="cart_block_price_container"><strong class="totals_cart_table" id="update_tax" <?php if ($customer_tax == 0) echo 'style="text-decoration:line-through"' ?>>€ <?php echo number_format($tax_amount, 2, '.', '') ?></strong></div>
        <div class="cart_block_price_container_label">TOTALE</div>
        <div class="cart_block_price_container"><strong class="totals_cart_table_big" id="update_total">€ <?php echo $comiteg->getPriceTax(array_sum($totalPrice), $customer_tax) ?></strong></div>

        <button type="button" class="button_info_cart" data-toggle="collapse" data-target="#gain"><i class="fa fa-info-circle" aria-hidden="true"></i></button>

          <div id="gain" class="collapse">
            <div class="cart_block_price_container_label">
              GUADAGNO STIMATO
              <div class="cart_block_price_container"><strong class="totals_cart_table" id="update_gain">€ <?php echo number_format($gain, 2, '.', '') ?></strong></div>
            </div>
          </div>

</div>

<div style="color:green">
<strong id="result_save_invoices"><?php echo $resultInsertCart ?></strong>
</div>

<button id="ripristina_invoice_items" type="button" class="button_action_cart_page btn btn-default">RIPRISTINA PRODOTTI CANCELLATI</button>
<button <?php if (!isset($_GET['customer_id']) || count($_items_collection) == 0) echo 'disabled' ?> data-toggle="modal" data-target="#save_invoice" id="fattura_items_to_invoice" type="button" class="button_action_cart_page btn btn-default">GENERA FATTURA</button>
<button <?php if (!isset($_GET['customer_id']) || count($_items_collection) == 0) echo 'disabled' ?> data-toggle="modal" data-target="#save_invoice_black" id="fattura_items_to_black" type="button" class="button_action_cart_page btn btn-default">FATTURA ELETTRONICA</button>



</div>
</div>
</div>
  </div>



  </div>
  <div id="refresh_saved_cart_orders" style="display:none"></div>
  <div id="refresh_saved_cart_estimates" style="display:none"></div>
  <div id="refresh_saved_cart_saved" style="display:none"></div>
  <?php //require_once('templates/ajax_cart_message.php') // contiene il popup della risposta e il form per database ?>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>
<script type="text/javascript">
$(function() {
    $(document).ajaxComplete(function(){
        $('#update_right_bar').trigger('click');
    });
});
</script>

</body>
</html>
