<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);
$currentYear = date('Y');

$table_1 = 'invoices_'.$currentYear;
$table_2 = 'invoice_items_'.$currentYear;
$table_3 = 'noinvoices_'.$currentYear;
$table_4 = 'noinvoice_items_'.$currentYear;
$link_page = '/top_seller.php';
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Top Seller</title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>

  <div id="main_container">
  <div id="wrapper">
  <?php require_once('templates/orders_left.php') ?>

    <div class="col-xs-12 breadcrumb_category">
      <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
      </a>
      Top Seller
    </div>


    <div class="col-md-9 scrolling_x_cart">
    <div class="fixed_width_700">

        <div id="searchOrderedProductResult">
        <?php
        $_poductCollection = $comiteg->getProductsOrderedByCustomer(NULL, NULL, NULL);

        foreach ($_poductCollection as $key => $row) {
        $mid[$key]  = $row['info']['occurr'];
        }
        array_multisort($mid, SORT_DESC, $_poductCollection);
        ?>
          <?php
          include('templates/list_top_seller.php');
          ?>
        </div>
    </div>

    </div>
    <div class="col-md-3">
    CERCA PRODOTTO
    <?php include('customers/search_input_top_seller.php') ?>
    <div style="color:green">
    <strong><?php echo $resultInsertCart ?></strong>
    </div>


    <div class="col-xs-12 totals_cart_block effect2" style="margin-top:25px;">
        PRODOTTI
        <div class="cart_block_price_container">
          <strong class="totals_cart_table">
            <span id="total_invoices"><?php echo $total_invoices ?></span>
          </strong>
        </div>

        <div class="cart_block_price_container_label">
        TOTALE VENDITE
        </div>
        <div class="cart_block_price_container">
          <strong class="totals_cart_table">
            € <span id="total_prices"><?php echo $tot_price ?></span>
          </strong>
        </div>

        <div class="cart_block_price_container_label">
        TOTALE COSTI
        </div>
        <div class="cart_block_price_container">
          <strong class="totals_cart_table">
            € <span id="total_costs"><?php echo $tot_costs ?></span>
          </strong>
        </div>

        <div class="cart_block_price_container_label">
        TOTALE RICAVI
        </div>
        <div class="cart_block_price_container">
          <strong class="totals_cart_table_big">
          <?php
          $multipl = $total_gain / $tot_costs;
          $percent = ($multipl * 100) - 100;
          if ($percent > 0) {
          $perc_sym = '+';
          }
          else {
          $perc_sym = '-';
          }
          ?>
          <span style="font-size:16px;color:#333" id="gain_percent">(<?php echo $perc_sym.number_format($percent,1 ,'.', '') ?>%)</span>  € <span id="total_gain"><?php echo $total_gain ?></span>
          </strong>
        </div>

      </div>
    </div>


<?php
include('templates/ajax_print_saved.php');
?>

<span id="refresh_cart" style="display:none"></span>
<span id="refresh_saved_cart" style="display:none"></span>


  </div>
  </div>
<?php require_once('templates/ajax_cart_message.php') // contiene il popup della risposta e il form per database ?>
  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>
<script type="text/javascript">
// quando finisce ajax update right block prezzi
$(function() {
    $(document).ajaxComplete(function(){
        $('#update_right_bar').trigger('click');
    });
});
</script>
</body>
</html>
