<?php
@set_time_limit(0);
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);

if (isset($_GET['cat'])) {
$link_page = '/admin.php?cat=g';
}
else if (isset($_GET['product']) && $_GET['product'] == 'new') {
$link_page = '/admin.php?product=new';
}
else if (isset($_GET['product']) && $_GET['product'] == 'update') {
$link_page = '/admin.php?product=update';
}

?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Admin</title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>

  <div id="main_container">
  <div id="wrapper">
  <?php require_once('templates/admin_left.php') ?>

    <?php
    if (isset($_GET['cat'])) {
    ?>
    <div class="col-xs-12 breadcrumb_category">
      <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
      </a>
      Gestisci Categorie
    </div>


    <?php
    include('templates/manage_categories.php');
    }
    ?>

    <?php
    if (isset($_GET['product']) && $_GET['product'] == 'new') {
    ?>
    <div class="col-xs-12 breadcrumb_category">
      <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
      </a>
      Aggiungi nuovo prodotto
    </div>


    <?php
    include('templates/add_product.php');
    }
    ?>

    <?php
    if (isset($_GET['product']) && $_GET['product'] == 'update' && !isset($_GET['id'])) {
    ?>
    <div class="col-xs-12 breadcrumb_category">
      <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
      </a>
      Modifica prodotti
    </div>


    <?php
    $product_collection = $comiteg->getProductsOwnAdded('A-2' , 'like');
      include ('templates/list_own_products.php');
    }
    ?>

    <?php
    if (isset($_GET['product']) && $_GET['product'] == 'update' && isset($_GET['id'])) {
    ?>
    <div class="col-xs-12 breadcrumb_category">
      <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
      </a>
      Modifica prodotto
    </div>


    <?php
    $_product = $comiteg->getProductsOwnAdded($_GET['id'], 'exact');
    include('templates/update_product.php');
    }
    ?>



  </div>
  </div>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>
<script type="text/javascript">
// quando finisce ajax update right block prezzi
$(function() {
    $(document).ajaxComplete(function(){
        $('#update_right_bar').trigger('click');
    });
});
</script>
</body>
</html>

