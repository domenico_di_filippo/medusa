// controllo left bar in new_customer.php
function anchorCustomer(idDiv) {
var id = $(idDiv).attr('id').split('_')[0];
$('.anchor_customer').css('display', 'none');
$('.category_item_sx').removeClass('active');
$('#'+id).css('display', 'block');
$('#'+id+'_left').addClass('active');

if ($('#'+id+'_left').hasClass('category_item_sx')) {
event.preventDefault();
}
}

// cambia icona nel menu categoria
function changeIcon(el) {
var buttonId = $(el).attr('data-target').split('_')[1];
  if ($(el).hasClass('collapsed')) {
  $('#span_'+buttonId).html('<i class="fa fa-minus" aria-hidden="true"></i>');
  }
  else {
  $('#span_'+buttonId).html('<i class="fa fa-plus" aria-hidden="true"></i>');
  }
};

// aggiorna il numero di oggetti nel carrello
$(function () {
  $('#update_minicart_response').on('click', function () {
    $.ajax({
    url: '/cart/add_cart_list.php',
    dataType: 'html',
    success: function(data) {
      var id = data;
      $('#number_items_cart').text(id);
    }
    });
  });
});

// minicart
/*
$(function () {

  $('.minicart_button').on('click', function () {

    $.ajax({
    url: '/cart/get_minicart_items.php',
    dataType: 'html',
    success: function(data) {
      var content = data;
      $('#modal_body_content').html(content);
    }
    });
  });
});
*/


// cambia prezzo on-the-fly
function changeProductPrice(ele) {
var idfield = $(ele).attr('id').split('_')[1];
var input = parseFloat($(ele).val());
var value = parseFloat(input).toFixed(2);
var cost = parseFloat($('#cost_'+idfield).val());

$('#product_price_pdf').val(value);
$('#changepr_'+idfield).text(value);
$('#changepr_product_'+idfield).text(value);

	if (value <= cost) {
  $(ele).css('color', 'red');
  $('#changepr_'+idfield).parent().css('background', 'red');
  $('#changepr_product_'+idfield).parent().css('color', 'red');
	}
	else {
  $(ele).css('color', 'inherit');
  $('#changepr_'+idfield).parent().css('background', '#53d453');
  $('#changepr_product_'+idfield).parent().css('color', 'green');
	}
}

// messaggi di errore carrello ajax
function addControls(ele) {
var idfield = $(ele).attr('id').split('_')[1];

var inputVal = $(ele).val();
var minVal = parseInt($(ele).attr('min'));
var maxVal = parseInt($(ele).attr('max'));
var stepVal = parseInt($(ele).attr('step'));

if (inputVal > maxVal) {
$('#cart_alert_keuyup_'+idfield).text('IMPOSSIBILE AGGIUNGERE AL CARRELLO: Quantità massima disponibile '+maxVal+' pezzi');
$('#btn_'+idfield).attr('type', 'button');
}

else if (inputVal < minVal) {
$('#cart_alert_keuyup_'+idfield).text('IMPOSSIBILE AGGIUNGERE AL CARRELLO: Quantità minima acquistabile '+minVal+' pezzi');
$('#btn_'+idfield).attr('type', 'button');
}

else if (inputVal % stepVal !== 0) {
$('#cart_alert_keuyup_'+idfield).text('IMPOSSIBILE AGGIUNGERE AL CARRELLO: Prodotto acquistabile a multipli di '+stepVal);
$('#btn_'+idfield).attr('type', 'button');
}

else if (inputVal <= maxVal && inputVal >= minVal) {
$('#cart_alert_keuyup_'+idfield).text('');
$('#btn_'+idfield).attr('type', 'submit');
}
}

// gestione input prezzo pagina carrello
function addControlsCart(ele) {
var idfield = $(ele).attr('id').split('_')[1];
var input = parseFloat($(ele).val());
var value = parseFloat(input).toFixed(2);
var cost = parseFloat($('#cost_'+idfield).val());

	if (value <= cost) {
  $(ele).css('color', 'red');
	}
	else {
  $(ele).css('color', 'inherit');
	}
//console.log(cost);
}


// invia il '#cart_delete_items' per eliminare dal carrello
$(function () {
$('body').on('click', '.invisible_btn_savedcarts', function () {

  var formData = $(this).closest('form').serializeArray();
  formData.push({ name: this.name, value: this.value });

  $('form.saved_carts_form').on('submit', function (e) {
  e.preventDefault();

    $.ajax({
      type: 'post',
      url: '/cart/delete_saved_carts.php',
      data: formData,
      success: function (data) {
      $('#refresh_saved_cart').trigger('click');
			$('#refresh_saved_cart_saved').trigger('click');
      },
      error: function () {
      alert("Problema con il carrello");
      }
    });
  });
});
});


// invia il '#cart_delete_items' per eliminare dal carrello
$(function () {
$('body').on('click', '.invisible_btn_estimates', function () {

  var formData = $(this).closest('form').serializeArray();
  formData.push({ name: this.name, value: this.value });

  $('form.saved_carts_form').on('submit', function (e) {
  e.preventDefault();

    $.ajax({
      type: 'post',
      url: '/cart/delete_saved_carts.php',
      data: formData,
      success: function (data) {
      $('#refresh_saved_cart').trigger('click');
      $('#refresh_saved_cart_estimates').trigger('click');
      },
      error: function () {
      alert("Problema con il carrello");
      }
    });
  });
});
});

// invia il '#cart_delete_items' per eliminare dal carrello
$(function () {
$('body').on('click', '.invisible_btn_orders', function () {

  var formData = $(this).closest('form').serializeArray();
  formData.push({ name: this.name, value: this.value });

  $('form.saved_carts_form').on('submit', function (e) {
  e.preventDefault();

    $.ajax({
      type: 'post',
      url: '/cart/delete_saved_carts.php',
      data: formData,
      success: function (data) {
      $('#refresh_saved_cart').trigger('click');
      $('#refresh_saved_cart_orders').trigger('click');
      },
      error: function () {
      alert("Problema con il carrello");
      }
    });
  });
});
});

/*
// invia il '#cart_delete_items' per eliminare dal carrello
$(function () {
$('body').on('click', '.invisible_btn_cart', function () {

  var formData = $(this).closest('form').serializeArray();
  formData.push({ name: this.name, value: this.value });

  $('form.saved_carts_form').on('submit', function (e) {
  e.preventDefault();

    $.ajax({
      type: 'post',
      url: '/cart/delete_saved_carts.php',
      data: formData,
      success: function (data) {
      $('#refresh_saved_cart').trigger('click');
      },
      error: function () {
      alert("Problema con il carrello");
      }
    });
  });
});
});
*/

/*$(function () {
$('body').on('click', '.invisible_btn_cart', function () {

  var formData = $(this).closest('form').serializeArray();
  formData.push({ name: this.name, value: this.value });

  $('form.saved_cart_items').on('submit', function (e) {
  e.preventDefault();

    $.ajax({
      type: 'post',
      url: '/cart/delete_saved_item.php',
      data: formData,
      dataType: 'html',
      success: function (data) {
      $('#savedItemsContent').html(data);
      $('#update_right_bar').trigger('click');
      },
      error: function () {
      alert("Problema con il carrello");
      }
    });
  });
});
});*/

// invia il '#cart_delete_items' per eliminare dal carrello
$(function () {
$('body').on('click', '.invisible_btn', function () {

  var formData = $(this).closest('form').serializeArray();
  formData.push({ name: this.name, value: this.value });

  $('form.cart_delete_items').on('submit', function (e) {
  e.preventDefault();

    $.ajax({
      type: 'post',
      url: '/cart/delete_minicart_items.php',
      data: formData,
      success: function (data) {
      //$('#modal_response').trigger('click');
      $('#refresh_cart').trigger('click');
      $('#update_minicart_response').trigger('click'); //footer.php
      $('#update_right_bar').trigger('click');
      },
      error: function () {
      alert("Problema con il carrello");
      }
    });
	});
});
});


// rimuove prodotti salvati nella sezione da fatturare
$(function () {
$('body').on('click', '.invisible_btn_to_invoice', function () {

  var formData = $(this).closest('form').serializeArray();
  formData.push({ name: this.name, value: this.value });

  $('form.saved_carts_form_toinvoice').on('submit', function (e) {
  e.preventDefault();

    $.ajax({
      type: 'post',
      url: '/cart/delete_saved_to invoice.php',
      data: formData,
      dataType: 'html',
      success: function (data) {
      $('#savedCartsContent_toinvoice').html(data);
      updateCartValues('.get_price');
        if ($('.invisible_btn_to_invoice').length > 0) {
        $('#fattura_items_to_invoice').removeAttr('disabled');
        $('#fattura_items_to_black').removeAttr('disabled');
      	}
      	else {
        $('#fattura_items_to_invoice').attr('disabled', 'disabled');
        $('#fattura_items_to_black').attr('disabled', 'disabled');
				}
				//console.log(formData);
      },
      error: function () {
      alert("Si è verificato un problema...");
      }
    });
  });
});
});

// rimuove prodotti salvati nel carrello salvato
$(function () {
$('body').on('click', '.invisible_btn_item_list', function () {

  var formData = $(this).closest('form').serializeArray();
  formData.push({ name: this.name, value: this.value });

  $('form.saved_cart_items').on('submit', function (e) {
  e.preventDefault();

    $.ajax({
      type: 'post',
      url: '/cart/delete_saved_item.php',
      data: formData,
      dataType: 'html',
      success: function (data) {
      $('#savedItemsContent').html(data);
      },
      error: function () {
      alert("Problema con il carrello");
      }
    });
  });
});
});

// svuota carrello
$(function () {
  $('#flush_cart').on('click', function () {

  $(this).attr('disabled', 'disabled');
  $('#chiudi_preventivo').attr('disabled', 'disabled');
  $('#chiudi_ordine').attr('disabled', 'disabled');
  $('#salva_carrello').attr('disabled', 'disabled');
  $('.form_search_customer').css('display', 'none');

    $.ajax({
    url: '/cart/flush_cart.php',
    success: function() {
      $('#update_minicart_response').trigger('click'); //footer.php
      $('#refresh_cart').trigger('click');
      $('#update_right_bar').trigger('click');
      $('#associato_a').css('display', 'none');
      $('#rimuovi_associa').css('display', 'none');
			$('#flush_cart').attr('disabled', 'disabled');
      window.history.replaceState(null, null, '/cart.php');  // NO REFRESH
    }
    });
  });
});

// salva carrello saved_carts.php
$('#salva_carrello_savedcarts').on('click', function () {
var dataForm = $('form.saved_cart_items').serialize();

$.ajax({
    url: '/cart/save_cart.php',
    type: 'post',
    dataType: 'text',
    data : dataForm,
    success: function() {
      $('#result_save_cart').text('Salvato correttamente');
      //document.location = '/cart.php';
    }
    });
});

// invia il form salva ordine
// cancellazione da carrello salvato avviene nella pagina /orders.php
$('#chiudi_ordine_savedcarts').on('click', function () {
$('#saved_cart_items').attr('action', '/orders.php');
$('#saved_cart_items').unbind('submit').submit();
});

// invia il form salva preventivo
// cancellazione da carrello salvato avviene nella pagina /estimates.php
$('#chiudi_preventivo_savedcarts').on('click', function () {
$('#saved_cart_items').attr('action', '/estimates.php');
$('#saved_cart_items').unbind('submit').submit();
});

// rimette carrello salvato nel carrello live
// cancellazione da carrello salvato avviene nella richiesta ajax
$('#rimetti_carrello_savedcarts').on('click', function () {
var dataForm = $('form.saved_cart_items').serialize();

$.ajax({
    url: '/cart/restore_cart.php',
    type: 'post',
    dataType: 'text',
    data : dataForm,
    success: function() {
      document.location = '/cart.php';
    }
    });
});




// invia il form salva carrello
$('#salva_carrello').on('click', function () {
$('#cart_delete_items').attr('action', '/saved_carts.php');
$('#cart_delete_items').unbind('submit').submit();
});

// invia il form salva ordine
$('#chiudi_ordine').on('click', function () {
$('#cart_delete_items').attr('action', '/orders.php');
$('#cart_delete_items').unbind('submit').submit();
});

// invia il form salva preventivo
$('#chiudi_preventivo').on('click', function () {
$('#cart_delete_items').attr('action', '/estimates.php');
$('#cart_delete_items').unbind('submit').submit();
});

// rimette carrello salvato nel carrello live
$('#rimetti_carrello').on('click', function () {
var dataForm = $('form.saved_cart_items').serialize();

$.ajax({
    url: '/cart/restore_cart_nodelete.php',
    type: 'post',
    dataType: 'text',
    data : dataForm,
    success: function() {
      document.location = '/cart.php';
    }
    });
});

// aggiorna il numero di oggetti nel carrello
$(function () {

  $('#refresh_cart_savedorders').on('click', function () {

    $.ajax({
    url: '/cart/add_cart_list.php',
    dataType: 'html',
    success: function(data) {
      var id = data;
      $('#number_items_cart').text(id);
    }
    });
  });
});

// preleva info per render prodotti nel carrello
$(function () {
  $('#refresh_cart').on('click', function () {
    $.ajax({
		type: 'post',
    url: '/cart/get_cart_items.php',
    data: $('#cart_delete_items').serialize(),
    dataType: 'text',
    success: function(data) {
      var content = data;
      $('#cartContent').html(content);
    }
    });
  });
});


// preleva info per render prodotti nel carrello
$(function () {
  $('#refresh_saved_cart').on('click', function () {

  var dataForm = $('#saved_carts_form').serialize();

    $.ajax({
    type: 'post',
    url: '/cart/get_saved_carts.php',
    data: dataForm,
    success: function(content) {
      $('#savedCartsContent').html(content);
    }
    });
});
});

$(function () {
  $('#refresh_saved_cart_estimates').on('click', function () {

  var dataForm = $('form.saved_carts_form_estimates').serialize();

    $.ajax({
    type: 'post',
    url: '/cart/get_saved_carts_customer.php',
    data: dataForm,
    success: function(content) {
      $('#savedCartsContent_estimates').html(content);
    }
    });
});
});

$(function () {
  $('#refresh_saved_cart_saved').on('click', function () {

  var dataForm = $('form.saved_carts_form_saved').serialize();

    $.ajax({
    type: 'post',
    url: '/cart/get_saved_carts_customer.php',
    data: dataForm,
    success: function(content) {
      $('#savedCartsContent_saved').html(content);
    }
    });
});
});

$(function () {
  $('#refresh_saved_cart_orders').on('click', function () {

  var dataForm = $('form.saved_carts_form_orders').serialize();

    $.ajax({
    type: 'post',
    url: '/cart/get_saved_carts_customer.php',
    data: dataForm,
    success: function(content) {
      $('#savedCartsContent_orders').html(content);
    }
    });
});
});


// update right bar con totali
function updateCartValues(classParse) {
var total = 0;
var totalCost = 0;
var customerTaxField = $('#customer_iva').val();

if (customerTaxField == null) {
var customerTaxValue = "<?php echo $customer_tax ?>";
}
else {
var customerTaxValue = $('#customer_iva').val();
}

if (customerTaxValue == '') {
$('#update_ali_iva').html('');
}
else {
$('#update_ali_iva').html('&nbsp;('+customerTaxValue+'%)');
}

if (customerTaxValue == 0.00) {
$('#update_tax').css('text-decoration', 'line-through');
}
else {
$('#update_tax').css('text-decoration', 'none');
}

var customerTax = 1 + customerTaxValue / 100;
$(classParse).each(function() {

var id = $(this).attr('id').split('_')[1];
var qty = parseInt($('#qty_'+id).val());
var price = parseFloat($('#price_'+id).val());
var cost = parseFloat($('#cost_'+id).val());

var inc_qty = parseInt($('#qty_'+id).attr('step'));
var max_qty = parseInt($('#ctrl_qty_'+id).val());
var min_qty = parseInt($('#qty_'+id).attr('min'));

  if (isNaN(price)) {
  var price = 0;
  $('#price_'+id).css('color', 'red');
  }

  if (qty % inc_qty !== 0 || qty > max_qty || qty < min_qty) {
  $('#qty_'+id).css('color', 'red');
  }
  else {
  $('#qty_'+id).css('color', 'inherit');
  }

var subtot = (qty * price);
var costs = (qty * cost);
total += parseFloat(subtot);
totalCost += parseFloat(costs);
$('#subtotal_row_'+id).text('€ '+parseFloat(subtot).toFixed(2));

});
$('#update_subtotal').text('€ '+parseFloat(total).toFixed(2));
var totalTax = parseFloat(total * customerTax).toFixed(2);
var gain = parseFloat(total - totalCost).toFixed(2);
$('#update_total').text('€ '+totalTax);
$('#update_tax').text('€ '+parseFloat(totalTax - total).toFixed(2));
$('#update_gain').text('€ '+gain);
}

// controllo valori per right bar cart, orders, preventivi, bozze
function sendIdToForm(id, name, ali_iva) {
$('#chiudi_preventivo').removeAttr('disabled');
$('#chiudi_ordine').removeAttr('disabled');
$('#salva_carrello').removeAttr('disabled');
$('#search_customer').css('display', 'none');
$('#search_customer_cart').css('display', 'none');
$('#search_customer').val(name);
$('#select_customer').hide();
$('#customer_id').val(id);
$('#customer_iva').val(ali_iva);
$('#rimuovi_associa').css('display', 'block');
$('#associato_a').html('CARRELLO ASSOCIATO A:<br><strong>'+name+'</strong>');
/*var url = location.href;
var baseurl = url.split('?')[0];
document.location = baseurl+'?customer_id='+id;*/
window.history.replaceState(null, null, '?customer_id='+id);  // NO REFRESH


$('#update_right_bar').trigger('click');

		$.ajax({
		type: 'post',
    url: '/cart/get_cart_items.php',
    data: $('#cart_delete_items').serialize(),
    dataType: 'text',
    success: function(data) {
      var content = data;
      $('#cartContent').html(content);
    }
    });
}


// controllo bottoni e search right bar cart, orders, preventivi, bozze
$('#rimuovi_associa').on('click', function() {
$(this).css('display', 'none');
//$('.form_search_customer').css('display', 'block');
$('#chiudi_preventivo').attr('disabled', 'disabled');
$('#chiudi_ordine').attr('disabled', 'disabled');
$('#salva_carrello').attr('disabled', 'disabled');
$('#search_customer').css('display', 'block');
$('#search_customer_cart').css('display', 'block');
$('#search_customer').val('');
$('#search_customer_cart').val('');
$('#customer_id').val('');
$('#customer_iva').val('');
$('#associato_a').html('ASSOCIA CARRELLO A CLIENTE');
window.history.replaceState(null, null, '/cart.php');  // NO REFRESH
$('#update_right_bar').trigger('click');

		$.ajax({
		type: 'post',
    url: '/cart/get_cart_items.php',
    data: $('#cart_delete_items').serialize(),
    dataType: 'text',
    success: function(data) {
      var content = data;
      $('#cartContent').html(content);
    }
    });
});

// controllo menu laterale per il mobile
function showHideLeftMobile() {
var windowWidth = $(window).width();

  if (windowWidth < 768) {

    if ($('#wrapper').hasClass('active')) {
    $('#sidebar-wrapper').css('display', 'none');

    }
    else {
    $('#sidebar-wrapper').css('display', 'block');
    }
  }
}

// popola '#form_cart' per scrittura database
function addToCart(el) {
  var idProd = $(el).attr('id').split('_')[1];

  var user = $('#user_'+idProd).val();
  var id = $('#id_'+idProd).val();
  var qty = $('#qty_'+idProd).val();
  var price = $('#price_'+idProd).val();
  var cost = $('#cost_'+idProd).val();

  $('#user_form').val(user);
  $('#id_form').val(id);
  $('#qty_form').val(qty);
  $('#price_form').val(price);
  $('#cost_form').val(cost);
}


// invia il '.form_cart' per scrittura e restituisce le risposte
$(function () {
  $('.form_cart').on('submit', function (e) {

  e.preventDefault();

  $.ajax({
    type: 'post',
    url: '/cart/add_cart_list.php',
    data: $('#form_cart').serialize(),
    success: function () {
      $('.modal_cart').modal('hide');
      $('#update_minicart_response').trigger('click');  // rimuovi se togli commento sotto, mostra popup conferma inserito carrello
      /*setTimeout(function(){
        $('.modal_ok').modal('show');
         //footer.php
        },500);*/
      },
      error: function () {
        alert("Problema con il carrello");
      }
    });
  });
});

// cancella clienti
$(function () {
$('body').on('click', '.invisible_btn_customers', function () {

  var formData = $('form.customers_form').serializeArray();
  formData.push({ name: this.name, value: this.value });

  $('form.customers_form').on('submit', function (e) {
  e.preventDefault();

    //if (confirm('Sei sicuro?')) {
    $('#cancel_customer').modal('show');
    $('button#modal_yes').click(function(e){
        $('#deleteModal').modal('hide');

    $.ajax({
      type: 'post',
      url: '/customers/delete_customer.php',
      data: formData,
      success: function (data) {
      $('#customerContent').html(data);
      },
      error: function () {
      alert("C'è stato un problema, riprova.");
      }
    });
    });
  });
});
});

// set indirizzo spedizione
var address = $('#address').val();
$('#address_send').val(address);

$('#address').on('change', function () {
var address_change = $('#address').val();
$('#address_send').val(address_change);
});

// ricerca carrelli salvati, ordini e preventivi
var searchRequest = null;
$(function () {
    var minlength = 3;

    $('#search_saved').keyup(function () {
        var that = this,
        value = $(this).val();

        var dataForm = $('form.form_search_saved').serialize();

        if (value.length > minlength ) {
            if (searchRequest != null)
                searchRequest.abort();

            searchRequest = $.ajax({
                type: "post",
                url: "/cart/search_saved.php",
                data: dataForm,
                dataType: "text",
                success: function(content){
                $('#savedCartsContent').html(content);
                }
            });
        }
        else if (value.length == 0 ) {

            searchRequest = $.ajax({
                type: "post",
                url: "/cart/search_saved.php",
                data: dataForm,
                dataType: "text",
                success: function(content){
                $('#savedCartsContent').html(content);
                }
            });
        }

    });
});



// ricerca top_seller
var searchRequest = null;
$(function () {
    var minlength = 3;

    $('#search_topSeller').on('click', function () {

  	$('form.form_search_ordered_products').on('submit', function (e) {
  	e.preventDefault();
		});

				var that = this,
        value = $(this).val();

        var dataForm = $('form.form_search_ordered_products').serialize();

        if (value.length > minlength ) {
            if (searchRequest != null)
                searchRequest.abort();

            searchRequest = $.ajax({
                type: "post",
                url: "/customers/search_top_seller.php",
                data: dataForm,
                dataType: "text",
                success: function(content){
                $('#searchOrderedProductResult').html(content);
                $('#total_invoices').text($('#total_span_invoices').text());
                $('#total_prices').text($('#total_span_prices').text());
                $('#total_costs').text($('#total_span_costs').text());
                $('#total_gain').text($('#total_span_gain').text());

                var multipl = parseFloat($('#total_span_gain').text()) / parseFloat($('#total_span_costs').text());
                var percent = (multipl * 100) - 100;
									if (multipl > 0) {
									var perc_sym = '+';
									}
									else {
                  var perc_sym = '-';
									}
								$('#gain_percent').text('('+perc_sym+percent.toFixed(1)+'%)');

                }
            });
        }
        else if (value.length == 0 ) {

            searchRequest = $.ajax({
                type: "post",
                url: "/customers/search_top_seller.php",
                data: dataForm,
                dataType: "text",
                success: function(content){
                $('#searchOrderedProductResult').html(content);
                $('#total_invoices').text($('#total_span_invoices').text());
                $('#total_prices').text($('#total_span_prices').text());
                $('#total_costs').text($('#total_span_costs').text());
                $('#total_gain').text($('#total_span_gain').text());

                var multipl = parseFloat($('#total_span_gain').text()) / parseFloat($('#total_span_costs').text());
                var percent = (multipl * 100) - 100;
									if (multipl > 0) {
									var perc_sym = '+';
									}
									else {
                  var perc_sym = '-';
									}
								$('#gain_percent').text('('+perc_sym+percent.toFixed(1)+'%)');


                }
            });
        }

    });
});


// ricerca prodotti ordinati
var searchRequest = null;
$(function () {
    var minlength = 3;

    $('#search_ordered_products').keyup(function () {
        var that = this,
        value = $(this).val();

        var dataForm = $('form.form_search_ordered_products').serialize();

        if (value.length > minlength ) {
            if (searchRequest != null)
                searchRequest.abort();

            searchRequest = $.ajax({
                type: "post",
                url: "/customers/search_ordered_products.php",
                data: dataForm,
                dataType: "text",
                success: function(content){
                $('#searchOrderedProductResult').html(content);
                }
            });
        }
        else if (value.length == 0 ) {

            searchRequest = $.ajax({
                type: "post",
                url: "/customers/search_ordered_products.php",
                data: dataForm,
                dataType: "text",
                success: function(content){
                $('#searchOrderedProductResult').html(content);
                }
            });
        }

    });
});

// ricerca carrelli salvati, ordini e preventivi
var searchRequest = null;
$(function () {
    var minlength = 3;

    $('#search_saved_invoices').keyup(function () {
        var that = this,
        value = $(this).val();

        var dataForm = $('form.form_search_invoices').serialize();

        if (value.length > minlength ) {
            if (searchRequest != null)
                searchRequest.abort();

            searchRequest = $.ajax({
                type: "post",
                url: "/cart/search_saved_invoices.php",
                data: dataForm,
                dataType: "text",
                success: function(content){
                $('#invoicesContent').html(content);
								$('#total_invoices').text($('#total_span_invoices').text());
                $('#total_prices').text($('#total_span_prices').text());
                $('#total_costs').text($('#total_span_costs').text());
                $('#total_gain').text($('#total_span_gain').text());

                /*var multipl = parseFloat($('#total_span_gain').text()) / parseFloat($('#total_span_costs').text());
                var percent = (multipl * 100) - 100;
									if (multipl > 0) {
									var perc_sym = '+';
									}
									else {
                  var perc_sym = '-';
									}
								$('#gain_percent').text(perc_sym+percent.toFixed(1)+'%');*/

								}
            });
        }
        else if (value.length == 0 ) {

            searchRequest = $.ajax({
                type: "post",
                url: "/cart/search_saved_invoices.php",
                data: dataForm,
                dataType: "text",
                success: function(content){
                $('#invoicesContent').html(content);
                $('#total_invoices').text($('#total_span_invoices').text());
                $('#total_prices').text($('#total_span_prices').text());
                $('#total_costs').text($('#total_span_costs').text());
                $('#total_gain').text($('#total_span_gain').text());

								/*var multipl = parseFloat($('#total_span_gain').text()) / parseFloat($('#total_span_costs').text());
                var percent = (multipl * 100) - 100;
									if (multipl > 0) {
									var perc_sym = '+';
									}
									else {
                  var perc_sym = '-';
									}
								$('#gain_percent').text(perc_sym+percent.toFixed(1)+'%');*/

								}
            });
        }

    });
});

// cerca cliente nel carrello

/*$('#select_customer').on('click', function (){
$('.form_search_customer').css('display', 'none');
$('#search_customer_cart').val('');
$('#associato_a').css('margin-bottom', '25px');
}); */

var searchRequest = null;
$(function () {
    var minlength = 3;

    $('#search_customer_cart').keyup(function () {
        var that = this,
        value = $(this).val();

        var dataForm = $('form.form_search_customer').serialize();

        if (value.length > minlength ) {
            if (searchRequest != null)
                searchRequest.abort();
            $('#select_customer').show();
            searchRequest = $.ajax({
                type: "post",
                url: "/customers/search_customers_cart.php",
                data: dataForm,
                dataType: "text",
                success: function(content){
                $('#select_customer').html(content);
                }
            });
        }
        else if (value.length <= minlength ) {
        $('#select_customer').hide();
        }

    });
});



// cerca clienti nella pagina customers.php
var searchRequest = null;

$(function () {
    var minlength = 3;

    $('#search_customer').keyup(function () {
        var that = this,
        value = $(this).val();

        var dataForm = $('form.form_search_customer').serialize();

        if (value.length > minlength ) {
            if (searchRequest != null)
                searchRequest.abort();

            searchRequest = $.ajax({
                type: "post",
                url: "/customers/search_customers.php",
                data: dataForm,
                dataType: "text",
                success: function(content){
                $('#customerContent').html(content);
                }
            });
        }
        else if (value.length == 0 ) {


            searchRequest = $.ajax({
                type: "post",
                url: "/customers/search_customers.php",
                data: dataForm,
                dataType: "text",
                success: function(content){
                $('#customerContent').html(content);
                }
            });
        }

    });
});


// stampa e mail prodotto
$('#stampa_scheda').on('click', function () {

var value = $('#email_to_pdf_form').val();
$('#product_email_pdf').val(value);

var valueCheck = $('#invia_copy:checked').val();
$('#inviami_copia').val(valueCheck);

var dataForm = $('#form_print').serialize();

$.ajax({
    url: '/templates/pdf_product.php',
    type: 'post',
    dataType: 'text',
    data : dataForm,
    success: function(content) {
    $('#stampa_scheda').attr('disabled', 'disabled');
		$('#get_pdf_product').html(content);
		//console.log(content);
    }
    });
});


// stampa e mail ordine, preventivo, carrello
$('#stampa_saved_confirm').on('click', function () {

var value = $('#email_to_pdf_saved').val();
$('#email_send_to').val(value);

var valueCheck = $('#invia_copy_saved:checked').val();
$('#email_send_me').val(valueCheck);

var dataForm = $('#saved_cart_items').serialize();

$.ajax({
    url: '/templates/pdf_saved.php',
    type: 'post',
    dataType: 'text',
    data : dataForm,
    success: function(content) {
    $('#stampa_saved_confirm').attr('disabled', 'disabled');
		$('#get_pdf_saved').html(content);
    }
    });
});


// stampa e mail fattura
$('#stampa_invoice').on('click', function () {

var value = $('#email_to_pdf_saved').val();
$('#email_send_to_invoice').val(value);

var valueCheck = $('#check_my_mail:checked').val();
$('#email_send_me_invoice').val(valueCheck);


var dataForm = $('form#saved_carts_form').serializeArray();
dataForm.push({ name: this.name, value: this.value });

		$.ajax({
    url: '/templates/invoice_saved.php',
    type: 'post',
    dataType: 'text',
    data : dataForm,
    success: function(content) {
		$('#get_invoice').html(content);
		$('#stampa_invoice').attr('disabled', 'disabled');
    }
    });
});

// fattura in black_year
$('#stampa_invoice_black').on('click', function () {

var value = $('#email_to_pdf_saved_black').val();
$('#email_send_to_invoice').val(value);

var valueCheck = $('#check_my_mail_black:checked').val();
$('#email_send_me_invoice').val(valueCheck);

var dataForm = $('form#saved_carts_form').serializeArray();
dataForm.push({ name: this.name, value: this.value });

		$.ajax({
    url: '/templates/noinvoices_saved.php',
    type: 'post',
    dataType: 'text',
    data : dataForm,
    success: function(content) {
		$('#get_invoice_black').html(content);
		$('#stampa_invoice_black').attr('disabled', 'disabled');
    }
    });
});


$('.stampa_invoice_chiudi').on('click', function () {
var dataForm = $('form#saved_carts_form').serializeArray();
dataForm.push({ name: this.name, value: this.value });

$.ajax({
type: 'post',
url: '/cart/get_items_to_invoice.php',
data: dataForm,
dataType: 'html',
success: function (data) {
$('#savedCartsContent_toinvoice').html(data);
updateCartValues('.get_price');
$('#stampa_invoice').removeAttr('disabled');
//console.log(formData);
},
error: function () {
alert("Si è verificato un problema...");
}
});

});


$('#ripristina_invoice_items').on('click', function () {
var dataForm = $('form#saved_carts_form').serializeArray();
dataForm.push({ name: this.name, value: this.value });

	$.ajax({
		type: 'post',
		url: '/cart/reset_items_to_invoice.php',
		data: dataForm,
		dataType: 'html',
		success: function (data) {
		$('#savedCartsContent_toinvoice').html(data);
		updateCartValues('.get_price');
    	if ($('.invisible_btn_to_invoice').length > 0) {
      $('#fattura_items_to_invoice').removeAttr('disabled');
      $('#fattura_items_to_black').removeAttr('disabled');
      }
      else {
      $('#fattura_items_to_invoice').attr('disabled', 'disabled');
      $('#fattura_items_to_black').attr('disabled', 'disabled');
			}
		//console.log(formData);
		},
		error: function () {
		alert("Si è verificato un problema...");
		}
	});

});


$('.close_invoice_creation').on('click', function () {
$('.btn_stampa_invoice').removeAttr('disabled');
});



// stampa veloce fattura invoices.php
$('body').on('click', '.btn_stampa_invoice_fast', function () {

var id = $(this).attr('id').split('_')[1];

var dataForm = $(this).closest('form').serializeArray();
  dataForm.push({ name: 'fast_print', value: '1' });
	dataForm.push({ name: this.name, value: this.value });


//console.log(dataForm);

$.ajax({
    url: '/templates/print_saved_invoice.php',
    type: 'post',
    dataType: 'text',
    data : dataForm,
    success: function(content) {
		$('#get_invoice_fast').html(content);
    $('#remove_invoice_creation').trigger('click');
		//$('.btn_stampa_invoice').attr('disabled', 'disabled');
		//console.log('#get_invoice-'+id);
		}
    });
});


// stampa e mail fattura in invoices.php
$('body').on('click', '.btn_stampa_invoice', function () {

var id = $(this).attr('id').split('_')[1];

var dataForm = $(this).closest('form').serializeArray();
  dataForm.push({ name: this.name, value: this.value });
console.log(dataForm);

$.ajax({
    url: '/templates/print_saved_invoice.php',
    type: 'post',
    dataType: 'text',
    data : dataForm,
    success: function(content) {
		$('#get_invoice-'+id).html(content);
		$('.btn_stampa_invoice').attr('disabled', 'disabled');
		//console.log('#get_invoice-'+id);
		}
    });
});

$('#remove_invoice_creation').on('click', function () {
$('#stampa_invoice').attr('disabled', 'disabled');
});



// invia ordine al fornitore
$('#invia_ordine').on('click', function () {
var dataForm = $('form.saved_cart_items').serialize();

$.ajax({
    url: '/cart/send_order.php',
    type: 'post',
    dataType: 'text',
    data : dataForm,
    success: function() {
      $('#result_save_cart').text('Ordine inviato correttamente');
      $('#invia_ordine').attr('disabled', 'disabled');
			$('#salva_carrello').attr('disabled', 'disabled');
			$('#salva_carrello_savedcarts').attr('disabled', 'disabled');
			$('.invisible_btn ').css('display', 'none');
      $('.get_qty').attr('readonly', 'readonly');
      $('.get_price').attr('readonly', 'readonly');
			var addressName = $('#address option:selected').text();
			$('#label_address_search').text('INDIRIZZO DI SPEDIZIONE:');
			$('#address').css('display', 'none');
			$('.invisible_btn_item_list').css('display', 'none');
			$('#label_hide_search_address').css('display', 'block').html('<strong>'+addressName+'</strong>');
    }
    });
});

// show/hide cost nel carrello ajax
$('.info_cost_cart').on('click', function () {
var id = $(this).attr('id').split('_')[1];
if ($('#infocost_'+id).hasClass('in')) {
$(this).text('+ info');
}
else {
$(this).text('Costo');
}
});

// controllo form anagrafica utente
$('#save_customer').submit(function() {

  if ($('#ragione_sociale').val().length <= 2) {
  $('#ragione_sociale').addClass('error');
  }
  else {
  $('#ragione_sociale').removeClass('error');
  }

  if ($('.error').length > 0) {
  return false;
  }
});

// loader mentre fa la query ajax
$(document).ready(function(){
    $(document).ajaxStart(function(){
        $('#wait').css('display', 'block'); //footer.php
    });
    $(document).ajaxComplete(function(){
        $('#wait').css('display', 'none'); //footer.php
    });
});
