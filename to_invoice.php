<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);


if ($_POST['table_1'] == 'saved_estimates') {
$comiteg->markEstimateAsOrdered($_POST['order_id']);
}

if ($_POST['table_1'] == 'saved_carts') {
$comiteg->deleteSavedCarts($_POST['table_1'], $_POST['table_2'], $user_id, $_POST['order_id']);
}

$table_1 = 'saved_orders';
$table_2 = 'saved_order_items';
$link_page = '/to_invoice.php';

if (isset($_POST['customer_id'])) {
$date = date('d-m-Y H:i');
$customer_id = $_POST['customer_id'];
$product_name = str_replace("'", "\'", $_POST['name']);
$order_id = $_GET['order_id'];
$product_price = $_POST['price'];
$product_qty = $_POST['qty'];

foreach ($product_name as $key => $_item) {
$name = $product_name[$key];
$price = $product_price[$key];
$qty = $product_qty[$key];
$array_prod[$key] = array('name' => $name, 'price' => $price, 'qty' => $qty);

}
$array = array('customer_id' => $customer_id, 'user_id' => $user_id, 'order_id' => $id_order, 'date' => $date, 'items' => $array_prod);
// scrivi nella tabella saved_order_items e saved_carts
$resultInsertCart = $comiteg->writeSavedCartItems($array, $table_1, $table_2);
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Da Fatturare</title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>

  <div id="main_container">
  <div id="wrapper">
  <?php require_once('templates/orders_left.php') ?>

    <div class="col-xs-12 breadcrumb_category">
      <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
      </a>
      Clienti con prodotti da fatturare
    </div>


    <div class="col-md-9 scrolling_x_cart">
    <div class="fixed_width_700">
          <div class="col-md-4 col-xs-4" style="padding-left:0">
          RAGIONE SOCIALE
          </div>
          <div class="col-md-2 col-xs-2">
          CITT&Agrave;
          </div>
          <div class="col-md-2 col-xs-2">
          TEL
          </div>
          <div class="col-md-3 col-xs-3">
          EMAIL
          </div>
          <div class="col-md-1 col-xs-1">
          &nbsp;
          </div>
          <div style="clear:both"></div>
          <hr class="hr_thin">
        <?php
        //$_saved_carts_collection = $comiteg->getSavedTable($table_1, $table_2, $user_id);
        $to_invoice_id = $comiteg->getToInvoiceSize($user_id);

        ?>
        <div id="customerContent">

          <?php
          $customers_collection = $comiteg->getToInvoiceCustomersResults($user_id, $to_invoice_id, NULL);
          $to_inv = '&ref=to_invoice';
          $dest_page = '/customer_to_invoice.php';
          include('customers/get_customers.php');
          //var_dump($customers_collection);

        //foreach ($to_invoice_id as $to_invoice) {
        ?>
          <!--div class="col-md-1 col-xs-1 cart_label_tab" style="padding-left:0;text-align:right">
          <?php echo $to_invoice ?>
          </div>
          <div class="col-md-10 col-xs-10">
          <strong><?php echo $comiteg->getCustomer($to_invoice)['ragione_sociale'] ?></strong>
          </div>
          <div class="col-md-1 col-xs-1">
          <a class="go_customer" href="new_customer.php?customer_id=<?php echo $to_invoice ?>&ref=to_invoice">
          <button type="button" class="invisible_btn minicart_delete_label">
          <i class="fa fa-pencil-square" aria-hidden="true"></i>
          </button>
          </a>
          </div>
          <div style="clear:both"></div>
          <hr class="hr_thin"-->
        <?php
        //}
        ?>

      </div>

    </div>
    </div>
    <div class="col-md-3">
    CERCA CLIENTI
    <?php include('customers/search_input_customers.php') ?>
    </div>

<?php
include('templates/ajax_print_saved.php');
?>

<span id="refresh_cart" style="display:none"></span>
<span id="refresh_saved_cart" style="display:none"></span>


  </div>
  </div>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>
<script type="text/javascript">
// quando finisce ajax update right block prezzi
$(function() {
    $(document).ajaxComplete(function(){
        $('#update_right_bar').trigger('click');
    });
});
</script>
</body>
</html>

