<?php
@set_time_limit(0);

require_once('class.import.php');
$import = new Import;

// scarica, inserisce nuovi e aggiorna esistenti da od.
$import->downloadOD();
$import->writeInfo();
$import->writePrices();
$import->writeAttributes();
$import->writeInventory();
$import->writeMedia();

// costruisce xml per albero categorie.
$import->buildCategoryXML();
?>