<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once('../config/config.php');

// classe per import nuovi e update vecchi articoli da od, una volta al giorno
// dal tracciato principale.
class Import {

  // connessione database.
  function connectDB() {
    $config = new Config;
    $db_user = Config::$db_user;
    $db_pass = Config::$db_pass;
    $db_name = Config::$db_name;
    $db_host = Config::$db_host;
    $conn = new mysqli($db_host, $db_user, $db_pass, $db_name);
    return $conn;
  }

  //scarica e controlla se ci sono aggiornamenti disponibili.
  function downloadOD() {
    $config = new Config;

    $od_track_host = Config::$od_track_host;
    $od_track_user = Config::$od_track_user;
    $od_track_password = Config::$od_track_password;
    $od_track_file_add = Config::$od_track_file_add;
    $od_track_path_add = Config::$od_track_path_add;
    $filename = 'ctrl_version.txt';

    $ftp_conn = ftp_connect($od_track_host);
    $login_result = ftp_login($ftp_conn, $od_track_user, $od_track_password);

    ftp_pasv($ftp_conn, true);

    $local_file = 'zip/'.$od_track_file_add;
    $server_file = $od_track_path_add.'/'.$od_track_file_add;

    $results = ftp_get($ftp_conn, $local_file, $server_file, FTP_BINARY);

    ftp_close($ftp_conn);

    $zip = new ZipArchive;
    $zip->open($local_file);
    $zip->extractTo('zip');
    $zip->close();

    $filepath = 'zip/'.str_replace('zip', 'xml', $od_track_file_add);
    $xml = simplexml_load_file($filepath);

    $attr = $xml->attributes();
    $newVersion = $attr['versione']."-".$attr['riferimentoAnno']."-".$attr['riferimentoMese']."-".$attr['riferimentoGiorno']."-".$attr['riferimentoOra'];

    $controlImport = file_get_contents('zip/'.$filename);

      if ($controlImport !== $newVersion) {
      $controlFile = fopen('zip/'.$filename, 'w');
      fwrite ($controlFile, $newVersion);
      fclose ($controlFile);
      //unlink('zip/'.$od_track_file_add);
      echo '<p>Aggiornamento disponibile '.$newVersion.'</p>';
      }
      else {
      unlink($filepath);
      //unlink('zip/'.$od_track_file_add);
      die ('<p>Nessun aggiornamento disponibile ('.$newVersion.')</p>');
      }
  }

  // trova e modifica il nome delle categorie da od che hanno lo stesso nome.
  function categoryDuplicate($ctrl01, $ctrl02, $cat01, $cat02) {
    $count = array_count_values($ctrl02);
    if ($count[$cat02] > 1) {
    $result = $cat02.' ('.$cat01.')';
    }
    else if ($cat01 == $cat02) {
    $result = $cat02.' ('.$cat01.')';
    }
    else if ((in_array($cat02, $ctrl01)) && ($cat01 !== $cat02)) {
    $result = $cat02.' ('.$cat01.')';
    }
    else {
    $result = $cat02;
    }
    return $result;
  }

  // scrive e fa l'update della tabella product_info.
  function writeInfo() {
    $config = new Config;

    $od_track_file_add = Config::$od_track_file_add;

    $filename = 'ctrl_version.txt';
    $xml = simplexml_load_file('zip/'.str_replace('zip', 'xml', $od_track_file_add));

    $arrayCategories = array();

      foreach($xml as $articolo) {
      $gruppo = isset($articolo->catalogo->gruppo->descrizione) ? (string) $articolo->catalogo->gruppo->descrizione : "";
      $categoria = isset($articolo->catalogo->categoria->descrizione) ? (string) $articolo->catalogo->categoria->descrizione : "";
      $sottocategoria = isset($articolo->catalogo->sottocategoria->descrizione) ? (string) $articolo->catalogo->sottocategoria->descrizione : "";
      $categories = trim(str_replace(array("/", "'"), array("-", "\'"), $gruppo))."/".trim(str_replace(array("/", "'"), array("-", "\'"), $categoria))."/".trim(str_replace(array("/", "'"), array("-", "\'"), $sottocategoria));

      $arrayCategories[] = $categories;
      }

    $uniqueCat = array_unique($arrayCategories);

      foreach ($uniqueCat as $stringToExplode) {
      $expl = explode('/', $stringToExplode);

      $ctrl_arr01[] = $expl[0];
      $duplicate02[] = $expl[0].'/'.$expl[1];
      $duplicate03[] = $expl[1].'/'.$expl[2];
      }

    $ctrl01 = array_unique($ctrl_arr01);

    $array02 = array_unique($duplicate02);

      foreach ($array02 as $arr02) {
      $expl = explode('/', $arr02);
      $ctrl02[] = $expl[1];
      }

    $array03 = array_unique($duplicate03);

      foreach ($array03 as $arr03) {
      $expl = explode('/', $arr03);
      $ctrl03[] = $expl[1];
      }

      foreach($xml as $articolo) {
      $sku = isset($articolo->codiceOD) ? (string) $articolo->codiceOD : "";
      $name = isset($articolo->descrizione) ? (string) $articolo->descrizione : "";
      $name = str_replace("'", "\'", $name);
      $description = isset($articolo->datiTecnici) ? (string) $articolo->datiTecnici : "";
      $description = str_replace("'", "\'", $description);

      $gruppo_01 = isset($articolo->catalogo->gruppo->descrizione) ? (string) $articolo->catalogo->gruppo->descrizione : "";
      $gruppo = trim(str_replace(array("/", "'"), array("-", "\'"), $gruppo_01));

      $conn = $this->connectDB();

      $exist = $conn->query("SELECT name FROM categories WHERE name = '$gruppo'");

        if ($exist->num_rows == 0 && $gruppo !== NULL) {
        $conn->query("INSERT INTO categories (name) VALUES ('$gruppo')");
        }

      $categoria_01 = isset($articolo->catalogo->categoria->descrizione) ? (string) $articolo->catalogo->categoria->descrizione : "";
      $categoria_02 = trim(str_replace(array("/", "'"), array("-", "\'"), $categoria_01));
      $categoria = $this->categoryDuplicate($ctrl01, $ctrl02, $gruppo, $categoria_02);

      $exist01 = $conn->query("SELECT name FROM categories WHERE name = '$categoria'");
      $query_dad = "SELECT id,name FROM categories WHERE name = '$gruppo'";
      $result_dad = mysqli_query($conn, $query_dad);

        while ($row01 = mysqli_fetch_array($result_dad)) {
        $dad_id = $row01['id'];
        }

        if ($exist01->num_rows == 0 && $categoria !== NULL) {
        $conn->query("INSERT INTO categories (name, dad) VALUES ('$categoria', '$dad_id')");
        }

      $sottocategoria_01 = isset($articolo->catalogo->sottocategoria->descrizione) ? (string) $articolo->catalogo->sottocategoria->descrizione : "";

      $sottocategoria_02 = trim(str_replace(array("/", "'"), array("-", "\'"), $sottocategoria_01));

        if ($sottocategoria_02 == $categoria_02 && $sottocategoria_02 == $gruppo) {
        $sottoCat = $sottocategoria_02.' - '.$gruppo;
        }
        else {
        $sottoCat = $sottocategoria_02;
        }

      $sottocategoria = $this->categoryDuplicate($ctrl02, $ctrl03, $categoria_02, $sottoCat);

        if ($categoria !== $gruppo && $categoria !== NULL) {
        $query_granpa = "SELECT id,name FROM categories WHERE name = '$categoria'";
        $result_granpa = mysqli_query($conn, $query_granpa);

          while ($row02 = mysqli_fetch_array($result_granpa)) {
          $granpa_id = $row02['id'];
          }
        }
        else {
        $granpa_id = NULL;
        }

      $exist02 = $conn->query("SELECT * FROM categories WHERE name = '$sottocategoria'");

        if ($exist02->num_rows == 0 && $sottocategoria !== NULL) {
        $conn->query("INSERT INTO categories
        (name, granpa, dad) VALUES ('$sottocategoria', '$dad_id', '$granpa_id')");
        }

        if ($sottocategoria !== $categoria && $sottocategoria !== NULL) {
        $query_last = "SELECT id,name FROM categories WHERE name = '$sottocategoria'";
        $result_last = mysqli_query($conn, $query_last);
          while ($row03 = mysqli_fetch_array($result_last)) {
          $last_id = $row03['id'];
          }
        }
        else {
        $last_id = NULL;
        }

      $categories_id = array($dad_id, $granpa_id, $last_id);
      $cat_ids = implode('/', array_filter($categories_id));

      $query_sku = "SELECT * FROM product_info WHERE id = '$sku'";
      $result_sku = mysqli_query($conn, $query_sku);

        while ($row04 = mysqli_fetch_array($result_sku)) {
        $categs_id = $row04['categories'];
        $name_table = $row04['name'];
        $desc_table = $row04['description'];
        }

      $existSku = $conn->query("SELECT id FROM product_info WHERE id = '$sku'");

        if ($existSku->num_rows == 0) {
        $conn->query("INSERT INTO product_info (id, categories, name, description) VALUES ('$sku', '$cat_ids', '$name', '$description')");
        }
        else if ($categs_id !== $cat_ids || $name_table !== $name || $desc_table !== $description) {
        $conn->query("UPDATE product_info SET categories = '$cat_ids', name = '$name', description = '$description' WHERE id = '$sku'");
        }
      mysqli_close($conn);
    }
  }

    // scrive e fa l'update della tabella product_prices.
  function writePrices() {
    $config = new Config;

    $markup = Config::$default_markup;
    $od_track_file_add = Config::$od_track_file_add;
    $xml = simplexml_load_file('zip/'.str_replace('zip', 'xml', $od_track_file_add));

      foreach($xml as $articolo) {
      $sku = isset($articolo->codiceOD) ? (string) $articolo->codiceOD : "";
      $cost = isset($articolo->prezzoNetto) ? (string) $articolo->prezzoNetto : "";
      $price = $cost * (1 + $markup/100);
      $variation = isset($articolo->variazione) ? (string) $articolo->variazione : "";

      $conn = $this->connectDB();

      $query_sku = "SELECT * FROM product_prices WHERE id = '$sku'";
      $result_sku = mysqli_query($conn, $query_sku);

        while ($row = mysqli_fetch_array($result_sku)) {
        $cost_table = $row['cost'];
        $price_table = $row['price'];
        $variation_table = $row['variation'];
        }

      $existSku = $conn->query("SELECT id FROM product_prices WHERE id = '$sku'");

        if ($existSku->num_rows == 0) {
        $conn->query("INSERT INTO product_prices (id, cost, price, variation) VALUES ('$sku', '$cost', '$price', '$variation')");
        }
        else if ($cost_table !== $cost || $price_table !== $price || $variation_table !== $variation) {
        $conn->query("UPDATE product_prices SET cost = '$cost', price = '$price', variation = '$variation' WHERE id = '$sku'");
        }
      mysqli_close($conn);
    }
  }

  // scrive e fa l'update della tabella product_attributes.
  function writeAttributes() {
    $config = new Config;

    $od_track_file_add = Config::$od_track_file_add;
    $xml = simplexml_load_file('zip/'.str_replace('zip', 'xml', $od_track_file_add));

      foreach($xml as $articolo) {
      $sku = isset($articolo->codiceOD) ? (string) $articolo->codiceOD : "";

      $brand = isset($articolo->marca) ? (string) $articolo->marca : "";
      $mpn = isset($articolo->partNumber) ? (string) $articolo->partNumber : "";
      $ean = isset($articolo->pezzo->barcode) ? (string) $articolo->pezzo->barcode : "";
      $weight = isset($articolo->pezzo->peso) ? (string) $articolo->pezzo->peso : "";
      $cod_alt_1 = isset($articolo->codiceAlternativo1) ? (string) $articolo->codiceAlternativo1 : "";
      $cod_alt_2 = isset($articolo->codiceAlternativo2) ? (string) $articolo->codiceAlternativo2 : "";

      $conn = $this->connectDB();

      $query_sku = "SELECT * FROM product_attributes WHERE id = '$sku'";
      $result_sku = mysqli_query($conn, $query_sku);

        while ($row = mysqli_fetch_array($result_sku)) {
        $brand_table = $row['brand'];
        $ean_table = $row['ean'];
        $mpn_table = $row['mpn'];
        $weight_table = $row['weight'];
        $cod_alt_1_table = $row['cod_alt_1'];
        $cod_alt_2_table = $row['cod_alt_2'];
        }

      $existSku = $conn->query("SELECT id FROM product_attributes WHERE id = '$sku'");

      if ($existSku->num_rows == 0) {
      $conn->query("INSERT INTO product_attributes (id, brand, ean, mpn,cod_alt_1, cod_alt_2, weight) VALUES ('$sku', '$brand', '$ean', '$mpn', '$cod_alt_1', '$cod_alt_2', '$weight')");
      }
      else if ($brand_table !== $brand || $ean_table !== $ean || $mpn_table !== $mpn || $cod_alt_1_table !== $cod_alt_1 || $cod_alt_2_table !== $cod_alt_1 || $weight_table !== $weight) {
      $conn->query("UPDATE product_attributes SET brand = '$brand', ean = '$ean', mpn = '$mpn', cod_alt_1 = '$cod_alt_1', cod_alt_2 = '$cod_alt_2', weight = '$weight' WHERE id = '$sku'");
      }
      mysqli_close($conn);
    }
  }

  // scrive e fa l'update della tabella product_inventory.
  function writeInventory() {
    $config = new Config;

    $od_track_file_add = Config::$od_track_file_add;
    $xml = simplexml_load_file('zip/'.str_replace('zip', 'xml', $od_track_file_add));

      foreach($xml as $articolo) {
      $sku = isset($articolo->codiceOD) ? (string) $articolo->codiceOD : "";

      $qty = isset($articolo->giacenzaDisponibile) ? (string) $articolo->giacenzaDisponibile : "";
      $min_qty = isset($articolo->minimo) ? (string) $articolo->minimo : "";
      $inc_qty = isset($articolo->delta) ? (string) $articolo->delta : "";
      $reorder = isset($articolo->previsioneArrivo) ? (string) $articolo->previsioneArrivo : "";

      $conn = $this->connectDB();

      $query_sku = "SELECT * FROM product_inventory WHERE id = '$sku'";
      $result_sku = mysqli_query($conn, $query_sku);

        while ($row = mysqli_fetch_array($result_sku)) {
        $qty_table = $row['qty'];
        $min_table = $row['min_qty'];
        $inc_table = $row['inc_qty'];
        $reorder_table = $row['reorder'];
        }

      $existSku = $conn->query("SELECT id FROM product_inventory WHERE id = '$sku'");

        if ($existSku->num_rows == 0) {
        $conn->query("INSERT INTO product_inventory (id, qty, min_qty, inc_qty, reorder) VALUES ('$sku', '$qty', '$min_qty', '$inc_qty', '$reorder')");
        }
        else if ($qty_table !== $qty || $min_table !== $min_qty || $inc_table !== $inc_qty || $reorder_table !== $reorder) {
        $conn->query("UPDATE product_inventory SET qty = '$qty', min_qty = '$min_qty', inc_qty = '$inc_qty', reorder = '$reorder' WHERE id = '$sku'");
        }
    mysqli_close($conn);
    }
  }

  // scrive e fa l'update della tabella product_media.
  function writeMedia() {
    $config = new Config;
    $od_track_file_add = Config::$od_track_file_add;
    $xml = simplexml_load_file('zip/'.str_replace('zip', 'xml', $od_track_file_add));

    $sku = array();
      foreach($xml as $articolo) {
      $sku[] = isset($articolo->codiceOD) ? (string) $articolo->codiceOD : "";
      }

    $chunks = array_chunk($sku, 5000);

    foreach ($chunks as $key => $chunk) {

      foreach ($chunk as $image) {
      $imagePath = $key.'/'.$image.'.jpg';

      $conn = $this->connectDB();
      $conn->query("INSERT INTO product_media (id, image) VALUES ('$image', '$imagePath')");
      mysqli_close($conn);
      }

    }
  }

  // trova l'immagine di default della categoria numero 2.
  function getDefaultCategoryImage($category_id) {
    $config = new Config;
    $image_base_path = Config::$img_path;

    $conn = $this->connectDB();
    $result = $conn->query("SELECT id, categories FROM product_info WHERE categories LIKE '%/$category_id/%' LIMIT 1");
    $code = mysqli_fetch_array($result)['id'];

    $result00 = $conn->query("SELECT id, image FROM product_media WHERE id = '$code'");
    $image = mysqli_fetch_array($result00)['image'];

    mysqli_close($conn);

    $cat_image = $image_base_path.$image;

    $conn = $this->connectDB();
    $conn->query("UPDATE categories SET image = '$cat_image' WHERE id = '$category_id'");
    mysqli_close($conn);

    /*$handle = curl_init($cat_image);
    curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

    $response = curl_exec($handle);
    $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
    if($httpCode == 404) {
    $cat_image = '/media/images/placeholder.jpg';
    }
    else {
    $cat_image = $image_base_path.$image;
    }
    curl_close($handle);*/
    return $cat_image;
  }

  // restituisce le informazioni sulle categorie dalla tabella categories.
  function getCategoriesFromTable() {
    $conn = $this->connectDB();

    $query = "SELECT id,name,dad FROM categories ORDER BY name";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $categories[$row['id']] = array('name' => $row['name'], 'dad' => $row['dad']);
      }
    return $categories;
  }

  // costruisce xml per albero categorie.
  function buildCategoryXML() {

    $domDocument = new DomDocument('1.0');

    $categories = $domDocument->appendChild($domDocument->createElement('categories'));

      foreach ($this->getCategoriesFromTable() as $key => $values) {
      $category = $domDocument->appendChild($domDocument->createElement('category'));
      $category_dad = $domDocument->appendChild($domDocument->createElement('dad', $values['dad']));
      $category_name = $domDocument->appendChild($domDocument->createElement('name', htmlspecialchars($values['name'])));
      $category_id = $domDocument->appendChild($domDocument->createElement('id', $key));
      $category_image = $domDocument->appendChild($domDocument->createElement('image', $this->getDefaultCategoryImage($key)));

      $categories->appendChild($category);
      $category->appendChild($category_dad);
      $category->appendChild($category_id);
      $category->appendChild($category_name);
      $category->appendChild($category_image);
      }
  $domDocument->formatOutput = true;
  $domDocument->save('../config/categories.xml');
  }

}