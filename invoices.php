<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);
$currentYear = date('Y');

$table_1 = 'invoices_'.$currentYear;
$table_2 = 'invoice_items_'.$currentYear;
$table_3 = 'noinvoices_'.$currentYear;
$table_4 = 'noinvoice_items_'.$currentYear;
$link_page = '/invoices.php';
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Fatture</title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>

  <div id="main_container">
  <div id="wrapper">
  <?php require_once('templates/orders_left.php') ?>

    <div class="col-xs-12 breadcrumb_category">
      <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
      </a>
      Fatture
    </div>


    <div class="col-md-9 scrolling_x_cart">
    <div class="fixed_width_700">
    <div id="get_invoice_fast" style="display:none"></div>
          <div class="col-md-1 col-xs-1 cart_label_tab" style="padding-left:0;text-align:right">
          FAT. #
          </div>
          <div class="col-md-4 col-xs-4">
          CLIENTE
          </div>
          <div class="col-md-2 col-xs-2" style="text-align:right">
          IMPORTO
          </div>
          <div class="col-md-3 col-xs-3" style="text-align:center">
          DATA
          </div>
          <div class="col-md-1 col-xs-1">
          &nbsp;
          </div>
          <div class="col-md-1 col-xs-1">
          &nbsp;
          </div>
          <div style="clear:both"></div>
          <hr class="hr_thin">

        <?php
        if (!isset($_POST['month_start'])) {
        $month_start = 1;
        }
        else {
        $month_start = intval($_POST['month_start']);
        }
        if (!isset($_POST['month_end'])) {
        $month_end = 12;
        }
        else {
        $month_end = intval($_POST['month_end']);
        }
        if (!isset($_POST['year_start'])) {
        $year_start = intval(date('Y'));
        }
        else {
        $year_start = intval($_POST['year_start']);
        }
        if (!isset($_POST['year_end'])) {
        $year_end = intval(date('Y'));
        }
        else {
        $year_end = intval($_POST['year_end']);
        }

        $_saved_carts_collection = array();
        //$_saved_carts_invoices = $comiteg->getInvoices($table_1, $table_2, $user_id);
        //$_saved_carts_noinvoices = $comiteg->getInvoices($table_3, $table_4, $user_id);
        $_saved_carts_invoices = $comiteg->getSavedInvoicesSearch($user_id, $_POST['search_saved'], $table_1, $table_2, $month_start, $month_end, $year_start, $year_end);
        $_saved_carts_noinvoices = $comiteg->getSavedInvoicesSearch($user_id, $_POST['search_saved'], $table_3, $table_4, $month_start, $month_end, $year_start, $year_end);

        if ($_saved_carts_noinvoices !== NULL && $_saved_carts_invoices !== NULL) {
        $_saved_carts_collection = array_merge_recursive($_saved_carts_invoices, $_saved_carts_noinvoices);
        }
        else if ($_saved_carts_noinvoices == NULL && $_saved_carts_invoices !== NULL) {
        $_saved_carts_collection = $_saved_carts_invoices;
        }
        else if ($_saved_carts_noinvoices !== NULL && $_saved_carts_invoices == NULL) {
        $_saved_carts_collection = $_saved_carts_noinvoices;
        }

        //var_dump(intval($_POST['month_start']));

        foreach ($_saved_carts_collection as $key => $row) {
        $mid[$key]  = $row['sorting'];
        }

        if (count($_saved_carts_collection) > 1) {
        array_multisort($mid, SORT_DESC, $_saved_carts_collection);
        }
        ?>

        <div id="invoicesContent">
          <?php
          include('cart/list_invoices.php');
          ?>
        </div>
    </div>
    </div>
    <div class="col-md-3">
    CERCA FATTURE
    <?php include('cart/search_input_invoices.php') ?>
    <div style="color:green">
    <strong><?php echo $resultInsertCart ?></strong>
    </div>


    <div class="col-xs-12 totals_cart_block effect2" style="margin-top:25px;">
        FATTURE EMESSE
        <div class="cart_block_price_container">
          <strong class="totals_cart_table">
            <span id="total_invoices"><?php echo $total_invoices ?></span>
          </strong>
        </div>

        <div class="cart_block_price_container_label">
        TOTALE VENDITE
        </div>
        <div class="cart_block_price_container">
          <strong class="totals_cart_table">
            € <span id="total_prices"><?php echo $total_prices ?></span>
          </strong>
        </div>

        <div class="cart_block_price_container_label">
        TOTALE COSTI
        </div>
        <div class="cart_block_price_container">
          <strong class="totals_cart_table">
            € <span id="total_costs"><?php echo $total_costs ?></span>
          </strong>
        </div>

        <div class="cart_block_price_container_label">
        TOTALE RICAVI
        </div>
        <div class="cart_block_price_container">
          <strong class="totals_cart_table_big">
            € <span id="total_gain"><?php echo $total_gain ?></span>
          </strong>
        </div>

      </div>
    </div>

<?php
include('templates/ajax_print_saved.php');
?>

<span id="refresh_cart" style="display:none"></span>
<span id="refresh_saved_cart" style="display:none"></span>


  </div>
  </div>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>
<script type="text/javascript">
// quando finisce ajax update right block prezzi
$(function() {
    $(document).ajaxComplete(function(){
        $('#update_right_bar').trigger('click');
    });
});
</script>
</body>
</html>
