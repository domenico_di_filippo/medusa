<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);

define('__ROOT__', dirname(dirname(__FILE__)));
require(__ROOT__.'/config/config.php');

// classe principale.
class Main {

  // connessione database.
  function connectDB() {
    $config = new Config;
    $db_user = Config::$db_user;
    $db_pass = Config::$db_pass;
    $db_name = Config::$db_name;
    $db_host = Config::$db_host;
    $conn = new mysqli($db_host, $db_user, $db_pass, $db_name);
    return $conn;
  }

  // installa tabelle di servizio.
  function installTable() {
    $config = new Config;

    // crate superadmin
    $sup_admin_user = Config::$sup_admin_user;
    $sup_admin_pass = Config::$sup_admin_pass;

    $conn = $this->connectDB();

      if ($conn->connect_errno) {
      $reports[] = '<p style="color:red">'.$conn->connect_error.'</p>';
      }
      else {
      $reports[] = '<p>Connessione DB stabilita.</p>';
      }

    $admin = $conn->query("CREATE TABLE IF NOT EXISTS `admin_user` (
    `id` int(8) NOT NULL AUTO_INCREMENT,
    `user` VARCHAR(255) NOT NULL,
    `pass` VARCHAR(255) NOT NULL,
    `role` int(1) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`, `user`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($admin) {
      $reports[] = '<p>Tabella `admin_user` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $query = "SELECT user FROM admin_user";

      if ($conn->query($query)->num_rows == 0) {
      $conn->query("INSERT INTO admin_user
      (user, pass, role) VALUES ('$sup_admin_user', '$sup_admin_pass', '0')"
      );
      }

    $cart = $conn->query("CREATE TABLE IF NOT EXISTS `cart` (
    `id` int(16) NOT NULL AUTO_INCREMENT,
    `user_id` int(8) NOT NULL,
    `id_product` VARCHAR(255) NOT NULL,
    `qty` int(8) NOT NULL DEFAULT '0',
    `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($cart) {
      $reports[] = '<p>Tabella `cart` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $customers = $conn->query("CREATE TABLE IF NOT EXISTS `customers` (
    `id` int(8) NOT NULL AUTO_INCREMENT,
    `user_id` int(8) NOT NULL,
    `ragione_sociale` VARCHAR(1024) NOT NULL,
    `nome` VARCHAR(512) NOT NULL,
    `cognome` VARCHAR(512) NOT NULL,
    `indirizzo` VARCHAR(1024) NOT NULL,
    `cap` VARCHAR(25) NOT NULL,
    `prov` VARCHAR(64) NOT NULL,
    `citta` VARCHAR(64) NOT NULL,
    `p_iva` VARCHAR(64) NOT NULL,
    `ali_iva` decimal(5,2) NOT NULL DEFAULT '0.00',
    `auto_notifica` int(1) NOT NULL DEFAULT '0',
    `cod_fisc` VARCHAR(64) NOT NULL,
    `cad_fattura` VARCHAR(64) NOT NULL,
    `note_fattura` VARCHAR(4096) NOT NULL,
    `cod_ipa` VARCHAR(64) NOT NULL,
    `email` VARCHAR(512) NOT NULL,
    `email_amministr` VARCHAR(512) NOT NULL,
    `telefono_1` VARCHAR(64) NOT NULL,
    `telefono_2` VARCHAR(64) NOT NULL,
    `indirizzo_1` VARCHAR(1024) NOT NULL,
    `indirizzo_2` VARCHAR(1024) NOT NULL,
    `indirizzo_3` VARCHAR(1024) NOT NULL,
    `indirizzo_4` VARCHAR(1024) NOT NULL,
    `indirizzo_5` VARCHAR(1024) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($customers) {
      $reports[] = '<p>Tabella `customers` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $saved_carts = $conn->query("CREATE TABLE IF NOT EXISTS `saved_carts` (
    `id_order` int(16) NOT NULL AUTO_INCREMENT,
    `id_user` int(8) NOT NULL,
    `id_customer` int(8) NOT NULL,
    `date` VARCHAR(20) NOT NULL,
    `notif` int(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id_order`),
    UNIQUE KEY `id_order` (`id_order`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($saved_carts) {
      $reports[] = '<p>Tabella `saved_carts` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $saved_cart_items = $conn->query("CREATE TABLE IF NOT EXISTS `saved_cart_items` (
    `id` int(16) NOT NULL AUTO_INCREMENT,
    `id_order` int(16) NOT NULL,
    `id_product` VARCHAR(255) NOT NULL,
    `qty` int(8) NOT NULL DEFAULT '0',
    `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($saved_cart_items) {
      $reports[] = '<p>Tabella `saved_cart_items` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $saved_orders = $conn->query("CREATE TABLE IF NOT EXISTS `saved_orders` (
    `id_order` int(16) NOT NULL AUTO_INCREMENT,
    `id_user` int(8) NOT NULL,
    `id_customer` int(8) NOT NULL,
    `date` VARCHAR(20) NOT NULL,
    `send` int(1) NOT NULL DEFAULT '0',
    `notif` int(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id_order`),
    UNIQUE KEY `id_order` (`id_order`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($saved_orders) {
      $reports[] = '<p>Tabella `saved_orders` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $saved_order_items = $conn->query("CREATE TABLE IF NOT EXISTS `saved_order_items` (
    `id` int(16) NOT NULL AUTO_INCREMENT,
    `id_order` int(16) NOT NULL,
    `id_product` VARCHAR(255) NOT NULL,
    `qty` int(8) NOT NULL DEFAULT '0',
    `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($saved_order_items) {
      $reports[] = '<p>Tabella `saved_order_items` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $saved_estimates = $conn->query("CREATE TABLE IF NOT EXISTS `saved_estimates` (
    `id_order` int(16) NOT NULL AUTO_INCREMENT,
    `id_user` int(8) NOT NULL,
    `id_customer` int(8) NOT NULL,
    `date` VARCHAR(20) NOT NULL,
    `notif` int(1) NOT NULL DEFAULT '0',
    `ordered` int(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id_order`),
    UNIQUE KEY `id_order` (`id_order`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($saved_estimates) {
      $reports[] = '<p>Tabella `saved_estimates` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $saved_estimate_items = $conn->query("CREATE TABLE IF NOT EXISTS `saved_estimate_items` (
    `id` int(16) NOT NULL AUTO_INCREMENT,
    `id_order` int(16) NOT NULL,
    `id_product` VARCHAR(255) NOT NULL,
    `qty` int(8) NOT NULL DEFAULT '0',
    `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($saved_estimate_items) {
      $reports[] = '<p>Tabella `saved_estimate_items` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $send_orders = $conn->query("CREATE TABLE IF NOT EXISTS `send_orders` (
    `id` int(16) NOT NULL AUTO_INCREMENT,
    `id_order` int(16) NOT NULL,
    `id_user` int(8) NOT NULL,
    `id_customer` int(8) NOT NULL,
    `date` VARCHAR(20) NOT NULL,
    `shipping_address` VARCHAR(1048) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($send_orders) {
      $reports[] = '<p>Tabella `send_orders` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $send_order_items = $conn->query("CREATE TABLE IF NOT EXISTS `send_order_items` (
    `id` int(16) NOT NULL AUTO_INCREMENT,
    `id_order` int(16) NOT NULL,
    `id_product` VARCHAR(255) NOT NULL,
    `qty` int(8) NOT NULL DEFAULT '0',
    `qty_invoiced` int(8) NOT NULL DEFAULT '0',
    `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
    `removed` int(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($send_order_items) {
      $reports[] = '<p>Tabella `send_order_items` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $invoices = $conn->query("CREATE TABLE IF NOT EXISTS `invoices_".date('Y')."` (
    `id` int(16) NOT NULL AUTO_INCREMENT,
    `id_user` int(8) NOT NULL,
    `id_customer` int(8) NOT NULL,
    `dd` int(2) ZEROFILL NOT NULL,
    `mm` int(2) ZEROFILL NOT NULL,
    `yyyy` int(4) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($invoices) {
      $reports[] = '<p>Tabella `invoices_'.date('Y').'` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $invoice_items = $conn->query("CREATE TABLE IF NOT EXISTS `invoice_items_".date('Y')."` (
    `id` int(16) NOT NULL AUTO_INCREMENT,
    `id_order` int(16) NOT NULL,
    `id_customer` int(8) NOT NULL,
    `id_fattura` int(8) NOT NULL,
    `id_product` VARCHAR(255) NOT NULL,
    `qty` int(8) NOT NULL DEFAULT '0',
    `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($invoice_items) {
      $reports[] = '<p>Tabella `invoice_items_'.date('Y').'` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $no_invoices = $conn->query("CREATE TABLE IF NOT EXISTS `noinvoices_".date('Y')."` (
    `id` int(16) NOT NULL AUTO_INCREMENT,
    `id_user` int(8) NOT NULL,
    `id_customer` int(8) NOT NULL,
    `dd` int(2) ZEROFILL NOT NULL,
    `mm` int(2) ZEROFILL NOT NULL,
    `yyyy` int(4) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($no_invoices) {
      $reports[] = '<p>Tabella `noinvoices_'.date('Y').'` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $no_invoice_items = $conn->query("CREATE TABLE IF NOT EXISTS `noinvoice_items_".date('Y')."` (
    `id` int(16) NOT NULL AUTO_INCREMENT,
    `id_order` int(16) NOT NULL,
    `id_customer` int(8) NOT NULL,
    `id_fattura` int(8) NOT NULL,
    `id_product` VARCHAR(255) NOT NULL,
    `qty` int(8) NOT NULL DEFAULT '0',
    `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($no_invoice_items) {
      $reports[] = '<p>Tabella `noinvoice_items_'.date('Y').'` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    mysqli_close($conn);

    return implode('', $reports);
  }

  // installa tebelle products_*.
  function createTables() {

    $conn = $this->connectDB();

      if ($conn->connect_errno) {
      $reports[] = '<p style="color:red">'.$conn->connect_error.'</p>';
      }
      else {
      $reports[] = '<p>Connessione DB stabilita.</p>';
      }

    $categories = $conn->query("CREATE TABLE IF NOT EXISTS `categories` (
    `id` int(8) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(512) NOT NULL,
    `granpa` int(8) NOT NULL,
    `dad` int(8) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($categories) {
      $reports[] = '<p>Tabella `categories` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $info = $conn->query("CREATE TABLE IF NOT EXISTS `product_info` (
    `id` VARCHAR(255) NOT NULL,
    `name` VARCHAR(1024) DEFAULT NULL,
    `description` VARCHAR(4096) DEFAULT NULL,
    `categories` VARCHAR(25) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($info) {
      $reports[] = '<p>Tabella `product_info` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $prices = $conn->query("CREATE TABLE IF NOT EXISTS `product_prices` (
    `id` VARCHAR(255) NOT NULL,
    `cost` decimal(9,4) NOT NULL DEFAULT '0.0000',
    `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
    `variation` VARCHAR(20) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($prices) {
      $reports[] = '<p>Tabella `product_prices` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $inventory = $conn->query("CREATE TABLE IF NOT EXISTS `product_inventory` (
    `id` VARCHAR(255) NOT NULL,
    `qty` int(8) NOT NULL DEFAULT '0',
    `min_qty` int(8) NOT NULL DEFAULT '0',
    `inc_qty` int(8) NOT NULL DEFAULT '0',
    `reorder` int(8) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($inventory) {
      $reports[] = '<p>Tabella `product_inventory` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    $attributes = $conn->query("CREATE TABLE IF NOT EXISTS `product_attributes` (
    `id` VARCHAR(255) NOT NULL,
    `brand` VARCHAR(512) DEFAULT NULL,
    `ean` VARCHAR(512) DEFAULT NULL,
    `mpn` VARCHAR(512) DEFAULT NULL,
    `cod_alt_1` VARCHAR(512) DEFAULT NULL,
    `cod_alt_2` VARCHAR(512) DEFAULT NULL,
    `weight` decimal(9,4) NOT NULL DEFAULT '0.0000',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($attributes) {
      $reports[] = '<p>Tabella `product_attributes` OK.</p>';
      }
      else {
      $reports[] = '<p>'.$conn->error.'</p>';
      }

    $media = $conn->query("CREATE TABLE IF NOT EXISTS `product_media` (
    `id` VARCHAR(255) NOT NULL,
    `image` VARCHAR(1024) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`))
    ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

      if ($media) {
      $reports[] = '<p>Tabella `product_media` OK.</p>';
      }
      else {
      $reports[] = '<p style="color:red">'.$conn->error.'</p>';
      }

    mysqli_close($conn);

    return implode('', $reports);
  }

  // controlla login in index.php e restituisce response per $_SESSION['login '].
  function login($user, $pass, $table, $field) {
    $conn = $this->connectDB();

    $query = "SELECT * FROM $table WHERE $field = '$user'";

    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $username = $row['user'];
      $password = $row['pass'];
      }

    mysqli_close($conn);

      if (password_verify($pass, $password) && $username == $user) {
      $response = array('value' => '1', 'message' => '');
      }
      else {
      $response = array('value' => '0', 'message' => 'User e Password non validi!');
      }
    return $response;
  }

  // funzione generica per prelevare dato singolo da una tabella.
  function getTableValue($fields, $table, $field, $find, $value) {
    $conn = $this->connectDB();

    $query = "SELECT $fields FROM $table WHERE $field = '$value'";

    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $val = $row[$find];
      }

    mysqli_close($conn);

    return $val;
  }

  // restituisce le categorie dall'xml in base alla parent category.
  function getCategoriesXML($dad_id) {
  $xml = simplexml_load_file('config/categories.xml');

    foreach($xml as $category) {
      if ($category->dad == $dad_id) {
      $name = isset($category->name) ? (string) $category->name : "";
      $id = isset($category->id) ? (string) $category->id : "";
      $image = isset($category->image) ? (string) $category->image : "";
      $array[$id] = array('name' => $name, 'image' => $image);
      }
    }
  return $array;
  }

  // restituisce le categorie dalla tabella categories figle.
  /*function getCategoryChildren($category_id) {
    $conn = $this->connectDB();

    $query = "SELECT * FROM categories WHERE dad = '$category_id' ORDER BY name ASC";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $info[$row['id']] = $row['name'];
      }
    mysqli_close($conn);
    return $info;
  }

  function getCategoryDad($category_id) {
    $conn = $this->connectDB();

    $query = "SELECT * FROM categories WHERE granpa = '$category_id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $info[$row['id']] = $row['name'];
      }
    mysqli_close($conn);
    return $info;
  }*/

  // restutuisce i chunks della category collection.
  function getCategorySize($category_id) {
    $config = new Config;
    $pagination = Config::$pagination;
    $conn = $this->connectDB();

    $query = "SELECT id, categories FROM product_info";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $categories = explode('/', $row['categories']);
        if (in_array($category_id, $categories)) {
        $size[] = $row['id'];
        }
      }
    mysqli_close($conn);

    if (is_array($size)) {
    $sizeSplit = array_chunk($size, $pagination);
    $sizeSplit = array_combine(range(1, count($sizeSplit)), array_values($sizeSplit));
    }
    else {
    $size = array();
    }



    return $sizeSplit;
  }

  // restuisce i prodotti per id categoria ordinati per nome o prezzo, ASC o DESC.
  function getProductsByCategory($category_id, $page, $table_get, $sorting) {
    $config = new Config;
    $image_base_path = Config::$img_path;
    $pagination = Config::$pagination;

    $conn = $this->connectDB();

    $query = "SELECT id, categories FROM product_info";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $categories = explode('/', $row['categories']);

        if (in_array($category_id, $categories)) {
        $id_prod[] = $row['id'];
        }
      }

      if ($table_get == 'price') {
      $table = 'product_prices';
      $field = 'price';
      }
      else if ($table_get == 'name') {
      $table = 'product_info';
      $field = 'name';
      }
      else {
      $table = 'product_prices';
      $field = 'price';
      }

      if (is_array($id_prod)) {
      $id_prod = $id_prod;
      }
      else {
      $id_prod = array();
      }

      foreach ($id_prod as $prodId) {
      $query00 = "SELECT id, $field FROM $table WHERE id = '$prodId'";
      $result00 = mysqli_query($conn, $query00);

        while ($row00 = mysqli_fetch_array($result00)) {
        $id_prod00[$row00['id']] = $row00[$field];
        }
      }

      if (is_array($id_prod00)) {
      $id_prod00 = $id_prod00;
      }
      else {
      $id_prod00 = array();
      }

      if ($sorting == 'ASC') {
      asort($id_prod00);
      }
      else if ($sorting == 'DESC') {
      arsort($id_prod00);
      }

      foreach ($id_prod00 as $ord => $ordered) {
      $id_prod_order[] = $ord;
      }

      if (is_array($id_prod_order)) {
      $id_prod_order = $id_prod_order;
      }
      else {
      $id_prod_order = array();
      }

    $inputArr_0 = array_chunk($id_prod_order, $pagination);



    if (count($inputArr_0) > 0) {
    $inputArr = array_combine(range(1, count($inputArr_0)), array_values($inputArr_0));

    $array = array();

    foreach ($inputArr[$page] as $id) {
    $query = "SELECT * FROM product_info WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
        $info[$row['id']] = array(
        'id' => $row['id'],
        'name' => $row['name'],
        'description' => $row['description'],
        'categories' => $row['categories']
        );
      }

    $query = "SELECT * FROM product_inventory WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
        $inventory[$row['id']] = array(
        'qty' => $row['qty'],
        'min_qty' => $row['min_qty'],
        'inc_qty' =>  $row['inc_qty'],
        'reorder' =>  $row['reorder']
        );
      }

    $query = "SELECT * FROM product_prices WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
        $prices[$row['id']] = array(
        'cost' => number_format($row['cost'], 2, '.', ''),
        'price' => number_format($row['price'], 2, '.', ''),
        'variation' => $row['variation']
        );
      }

    $query = "SELECT * FROM product_attributes WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
        $attributes[$row['id']] = array(
        'brand' => $row['brand'],
        'ean' => $row['ean'],
        'mpn' => $row['mpn'],
        'cod_alt_1' => $row['cod_alt_1'],
        'cod_alt_2' => $row['cod_alt_2'],
        'weight' => number_format($row['weight'], 2, '.', '')
        );
      }

    $query = "SELECT * FROM product_media WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {

        $image_file = $image_base_path.$row['image'];

        /*$handle = curl_init($image_file);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
          if($httpCode == 404) {
          $product_image = '/media/images/placeholder.jpg';
          }
          else {
          $product_image = $image_base_path.$row['image'];
          }
          curl_close($handle);*/

        $media[$row['id']] = array(
        'image' => $image_file
        );
      }

      foreach ($prices as $key => $value) {
      $array[$key] = array(
      'info' => $info[$key],
      'inventory' => $inventory[$key],
      'prices' => $prices[$key],
      'attributes' => $attributes[$key],
      'media' => $media[$key]
      );
      }
    }

    }
    else {
    $array = NULL;
    }

    mysqli_close($conn);
    return $array;
  }

  // restituisce il name della categoria dal suo id.
  function getCategoryName($category_id) {
    $conn = $this->connectDB();

    $query = "SELECT id, name FROM categories WHERE id = '$category_id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $cat_name = $row['name'];
      }
    mysqli_close($conn);
    return $cat_name;
  }

  // inserisce o aggiorna i prodotti nella tabella cart
  function addToCartList($user, $id_product, $qty, $price) {
    $conn = $this->connectDB();

    $query = "SELECT id,user FROM admin_user WHERE user = '$user'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $user_id = $row['id'];
      }

    $exist = $conn->query("SELECT * FROM cart WHERE user_id = '$user_id' AND id_product = '$id_product'");

      if ($exist->num_rows == 0) {
      $conn->query("INSERT INTO cart (
      user_id, id_product, qty, price)
      VALUES ('$user_id', '$id_product', '$qty', '$price')");
      }
      else {
      $conn->query("UPDATE cart SET
      qty = '$qty', price = '$price'
      WHERE id_product = '$id_product'");
      }
    mysqli_close($conn);
  }

  // restituisce il numero di oggetti nel carrello dalla tabella cart in base all'id user.
  function getCartItems($user) {
    $conn = $this->connectDB();

    $query = "SELECT id,user FROM admin_user WHERE user = '$user'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $user_id = $row['id'];
      }

    $query = "SELECT * FROM cart WHERE user_id = '$user_id'";
    $result = mysqli_query($conn, $query);

      if ($conn->query($query)->num_rows > 0) {

        while ($row = mysqli_fetch_array($result)) {
        $user_rows[] = $row['user_id'] ;
        }
      $number = count($user_rows);
      }
      else {
      $number = 0;
      }
    mysqli_close($conn);
    return $number;
  }

  // restituisce le informazioni dei prodotti nel carrello per visualizzarle nel minicart.
  function getMinicartItems($user) {
    $config = new Config;
    $image_base_path = Config::$img_path;

    $conn = $this->connectDB();

    $query = "SELECT id,user FROM admin_user WHERE user = '$user'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $user_id = $row['id'];
      }

    $query = "SELECT * FROM cart WHERE user_id = '$user_id'";
    $result = mysqli_query($conn, $query);

      if ($conn->query($query)->num_rows > 0) {

        while ($row = mysqli_fetch_array($result)) {
        $cart[$row['id_product']] = array(
        'id' => $row['id_product'],
        'qty' => $row['qty'],
        'price' => number_format($row['price'], 2, '.', '')
        );

        $product_id[$row['id_product']] = $row['id_product'];
        }
      }
      else {
      $product_id = array();
      }

      foreach ($product_id as $pr_id) {

      $query = "SELECT id,name FROM product_info WHERE id = '$pr_id'";
      $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
        $info[$row['id']] = array('name' => $row['name']);
        }

      $query = "SELECT * FROM product_inventory WHERE id = '$pr_id'";
      $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
        $inventory[$row['id']] = array(
        'qty' => $row['qty'],
        'min_qty' => $row['min_qty'],
        'inc_qty' => $row['inc_qty']
        );
        }

      $query = "SELECT id,cost,price FROM product_prices WHERE id = '$pr_id'";
      $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
        $prices[$row['id']] = array(
        'cost' => number_format($row['cost'], 2, '.', ''),
        'price' => number_format($row['price'], 2, '.', '')
        );
        }

      $query = "SELECT * FROM product_media WHERE id = '$pr_id'";
      $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
        $image_file = $image_base_path.$row['image'];

        /*$handle = curl_init($image_file);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
          if($httpCode == 404) {
          $product_image = '/media/images/placeholder.jpg';
          }
          else {
          $product_image = $image_base_path.$row['image'];
          }
          curl_close($handle);*/

        $media[$row['id']] = array(
        'image' => $image_file
        );
        }
      }

    mysqli_close($conn);

      foreach ($product_id as $key) {
      $array[] = array(
      'cart' => $cart[$key],
      'info' => $info[$key],
      'inventory' => $inventory[$key],
      'prices' => $prices[$key],
      'media' => $media[$key]
      );
      }
    return $array;
  }

  // rimuove i prodotti dal carrello.
  function deleteItemMinicart($user, $id_product) {
  $conn = $this->connectDB();

  $query = "SELECT id,user FROM admin_user WHERE user = '$user'";
  $result = mysqli_query($conn, $query);

    while ($row = mysqli_fetch_array($result)) {
    $user_id = $row['id'];
    }
  $conn->query("DELETE FROM cart WHERE user_id = '$user_id' AND id_product = '$id_product'");

  mysqli_close($conn);

  return $id_product;
  }

  // restituisce tutte i campi prodotto in base al suo id.
  function getProduct($id) {
    $config = new Config;
    $image_base_path = Config::$img_path;

    $conn = $this->connectDB();

    $query = "SELECT * FROM product_info WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
        $info[$row['id']] = array(
        'id' => $row['id'],
        'name' => $row['name'],
        'description' => str_replace('. ', '.<br>', $row['description']),
        'categories' => $row['categories']
        );
      }

    $query = "SELECT * FROM product_inventory WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
        $inventory[$row['id']] = array(
        'qty' => $row['qty'],
        'min_qty' => $row['min_qty'],
        'inc_qty' =>  $row['inc_qty'],
        'reorder' =>  $row['reorder']
        );
      }

    $query = "SELECT * FROM product_prices WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
        $prices[$row['id']] = array(
        'cost' => number_format($row['cost'], 2, '.', ''),
        'price' => number_format($row['price'], 2, '.', ''),
        'variation' => $row['variation']
        );
      }


    $query = "SELECT * FROM product_attributes WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
        $attributes[$row['id']] = array(
        'brand' => $row['brand'],
        'ean' => $row['ean'],
        'mpn' => $row['mpn'],
        'cod_alt_1' => $row['cod_alt_1'],
        'cod_alt_2' => $row['cod_alt_2'],
        'weight' => number_format($row['weight'], 2, '.', '')
        );
      }

    $query = "SELECT * FROM product_media WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {

        $image_file = $image_base_path.$row['image'];

        /*$handle = curl_init($image_file);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
          if($httpCode == 404) {
          $product_image = '/media/images/placeholder.jpg';
          }
          else {
          $product_image = $image_base_path.$row['image'];
          }
          curl_close($handle);*/

        $media[$row['id']] = array(
        'image' => $image_file
        );
      }

    mysqli_close($conn);

      foreach ($prices as $key => $value) {
      $array[$key] = array(
      'info' => $info[$key],
      'inventory' => $inventory[$key],
      'prices' => $prices[$key],
      'attributes' => $attributes[$key],
      'media' => $media[$key]
      );
      }

  return $array;
  }

  // controlla se un prodotto è nel carrello e modifica il prezzo in catalogo
  // con il prezzo del carrello.
  function getProductCartPrice($id, $user) {
    $conn = $this->connectDB();

    $query = "SELECT id,user FROM admin_user WHERE user = '$user'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $user_id = $row['id'];
      }

    $query = "SELECT * FROM cart WHERE user_id = '$user_id' AND id_product = '$id'";
    $result = mysqli_query($conn, $query);

      if ($conn->query($query)->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
        $product_price = number_format($row['price'], 2, '.', '');
        }
      }
      else {
      $product_price = NULL;
      }
    mysqli_close($conn);
    return $product_price;
  }

  // restituisce l'array degli id dei prodotti tramite ricerca.
  function searchEngine($query, $category) {

    $explodeQuery_all = explode(' ', $query);

    $array_exclude = array('per', 'di', 'da', 'in', 'ad', 'un', 'ed');

      foreach ($explodeQuery_all as $query_all) {
        if (strlen($query_all) > 1 && !in_array($query_all, $array_exclude)) {
        $explodeQuery[] = trim($query_all);
        }
      }

    if ($category !== "") {
    $queryCategory = "AND (categories LIKE '%/$category'
    OR categories LIKE '$category/%'
    OR categories LIKE '%/$category/%')";
    }
    else if ($category == NULL) {
    $queryCategory = "";
    }

    $conn = $this->connectDB();

    $query = "SELECT * FROM product_attributes WHERE
    brand LIKE '%$query%'
    OR ean = '$query'
    OR mpn = '$query'
    OR id = '$query'
    OR cod_alt_1 = '$query'
    OR cod_alt_2 = '$query'";

    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $results_attr[] = $row['id'];
      }

      if (is_array($results_attr)) {
      $results_attr = $results_attr;
      }
      else {
      $results_attr = array();
      }

      foreach ($results_attr as $attr_id) {
      $query = "SELECT id,categories FROM product_info WHERE id = '$attr_id' $queryCategory";
      $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
        $results_attributes[] = $row['id'];
        }
      }

    if (count($results_attributes) == 0) {

      if (is_array($explodeQuery)) {
      $explodeQuery = $explodeQuery;
      }
      else {
      $explodeQuery = array();
      }

    foreach ($explodeQuery as $word) {

    $query = "SELECT * FROM product_info WHERE
    (name LIKE '% $word %'
    OR name LIKE '$word %'
    OR name LIKE '% $word'
    OR name LIKE '% $word'
    OR description LIKE '% $word %'
    OR description LIKE '$word %'
    OR description LIKE '% $word') $queryCategory";

    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $results_arr[] = $row['id'];
      $names_arr[$row['id']] = $row['name'];
      }
    }

    if (is_array($names_arr)) {
    $names_arr = $names_arr;
    }
    else {
    $names_arr = array();
    }

    $countIterations = count($explodeQuery);
    $count = 0;
    foreach ($names_arr as $key => $name) {

      $ct = 0;

        foreach ($explodeQuery as $find) {

          if (strpos(strtolower($name), strtolower($find)) !== FALSE) {
            $ct++;
          }
        }

          if ($ct == $countIterations) {
          $count++;
          $results[] = $key;
          $names[$key] = $name;
          }
      }

    if ($count <= 4 && $countIterations == 1 || $count <= 4 && $countIterations > 1) {  //decide quali dei risultati prendere

    foreach ($explodeQuery as $word) {

      if (strlen($word) > 2) {
      $wordTR = substr($word, 0, -1);
      $expQuery[] = $wordTR;
      }
      else {
      $wordTR = $word;
      $expQuery[] = $wordTR;
      }

    $multiquery[] = "'%$wordTR%'";
    }

    $query_name = '(SELECT * FROM product_info WHERE (name LIKE '.implode(' AND name LIKE ', $multiquery).') '.$queryCategory.')';
    $query_description = '(SELECT * FROM product_info WHERE (description LIKE '.implode(' AND description LIKE ', $multiquery).') '.$queryCategory.')';

    $final_query = $query_name.' UNION '.$query_description;
    $result = mysqli_query($conn, $final_query);

      while ($row = mysqli_fetch_array($result)) {
      $results[] = $row['id'];
      $names[$row['id']] = $row['name'];
      }
    }

    mysqli_close($conn);

      if (is_array($results)) {
      $results = $results;
      }
      else {
      $results = array();
      }

    $resultCollection = array_unique($results);
    }
    else {
    $resultCollection = $results_attributes;
    }
  return $resultCollection;
  }

  // restituisce i chunks della search collection.
  function getSearchSize($query, $sort_by, $cat) {
    $config = new Config;
    $pagination = Config::$pagination;

    if ($cat == NULL) {
    $cat = "";
    }

    $resultCollection = $this->searchEngine($query, $cat);

      if ($sort_by == 'price') {
      $table = 'product_prices';
      $field = 'price';
      }
      else if ($sort_by == 'name') {
      $table = 'product_info';
      $field = 'name';
      }
      else {
      $table = 'product_prices';
      $field = 'price';
      }

      foreach ($resultCollection as $prodId) {
      $conn = $this->connectDB();

      $query00 = "SELECT id, $field FROM $table WHERE id = '$prodId'";
      $result00 = mysqli_query($conn, $query00);

        while ($row00 = mysqli_fetch_array($result00)) {
        $id_prod00[$row00['id']] = $row00[$field];
        }
      mysqli_close($conn);
      }

      if (is_array($id_prod00)) {

        foreach ($id_prod00 as $ord => $ordered) {
        $id_prod_order[] = $ord;
        }
        $sizeSplit = array_chunk($id_prod_order, $pagination);
        $sizeSplit = array_combine(range(1, count($sizeSplit)), array_values($sizeSplit));
      }
      else {
      $sizeSplit = array();
      }

    return $sizeSplit;
  }

  // restituisce le informazioni sui prodotti da ricerca e li ordina per nome o prezzo, ASC o DESC.
  function getSearchResuts($query, $page, $sort_by, $sorting, $cat) {
    $config = new Config;
    $image_base_path = Config::$img_path;
    $pagination = Config::$pagination;

      if ($sort_by == 'price') {
      $table = 'product_prices';
      $field = 'price';
      }
      else if ($sort_by == 'name') {
      $table = 'product_info';
      $field = 'name';
      }
      else {
      $table = 'product_prices';
      $field = 'price';
      }

    if ($cat == NULL) {
    $cat = "";
    }

    $resultCollection = $this->searchEngine($query, $cat);

      foreach ($resultCollection as $prodId) {
      $conn = $this->connectDB();

      $query00 = "SELECT id, $field FROM $table WHERE id = '$prodId'";
      $result00 = mysqli_query($conn, $query00);

        while ($row00 = mysqli_fetch_array($result00)) {
        $id_prod00[$row00['id']] = $row00[$field];
        }
      mysqli_close($conn);
      }

      if (is_array($id_prod00)) {
        if ($sorting == 'ASC') {
        asort($id_prod00);
        }
        else if ($sorting == 'DESC') {
        arsort($id_prod00);
        }

        foreach ($id_prod00 as $ord => $ordered) {
        $id_prod_order[] = $ord;
        }
      }
      else {
      $id_prod_order = array();
      }

    $array = array();

    if (count($resultCollection) > 0) { // if is_array
    $resultCollection = array_chunk($id_prod_order, $pagination);
    $resultCollection = array_combine(range(1, count($resultCollection)), array_values($resultCollection));
    $resultCollection = $resultCollection[$page];
    }
    else {
    $resultCollection = array();
    }

    foreach ($resultCollection as $id) {

    $conn = $this->connectDB();

    $query = "SELECT * FROM product_info WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
        $info[$row['id']] = array(
        'id' => $row['id'],
        'name' => $row['name'],
        'description' => str_replace('. ', '.<br>', $row['description']),
        'categories' => $row['categories']
        );
      }

    $query = "SELECT * FROM product_inventory WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
        $inventory[$row['id']] = array(
        'qty' => $row['qty'],
        'min_qty' => $row['min_qty'],
        'inc_qty' =>  $row['inc_qty'],
        'reorder' =>  $row['reorder']
        );
      }

    $query = "SELECT * FROM product_prices WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
        $prices[$row['id']] = array(
        'cost' => number_format($row['cost'], 2, '.', ''),
        'price' => number_format($row['price'], 2, '.', ''),
        'variation' => $row['variation']
        );
      }

    $query = "SELECT * FROM product_attributes WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
        $attributes[$row['id']] = array(
        'brand' => $row['brand'],
        'ean' => $row['ean'],
        'mpn' => $row['mpn'],
        'cod_alt_1' => $row['cod_alt_1'],
        'cod_alt_2' => $row['cod_alt_2'],
        'weight' => number_format($row['weight'], 2, '.', '')
        );
      }

    $query = "SELECT * FROM product_media WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {

        $image_file = $image_base_path.$row['image'];

        /*$handle = curl_init($image_file);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
          if($httpCode == 404) {
          $product_image = '/media/images/placeholder.jpg';
          }
          else {
          $product_image = $image_base_path.$row['image'];
          }
          curl_close($handle);*/

        $media[$row['id']] = array(
        'image' => $image_file
        );
      }

    mysqli_close($conn);
    }

    if (is_array($info)) {

      foreach ($info as $key => $value) {
      $array[$key] = array(
      'info' => $info[$key],
      'inventory' => $inventory[$key],
      'prices' => $prices[$key],
      'attributes' => $attributes[$key],
      'media' => $media[$key]
      );
      }
    }
    else {
    $array = array();
    }

  return $array;
  }

  // restituisce le categorie dei prodotti presenti nei risultati di ricerca.
  function getCategoryFilter($query, $cat) {

    if (!isset($_GET['filter'])) {
    $cat = '';
    }

    $resultCollection = $this->searchEngine($query, $cat);

    foreach ($resultCollection as $id) {
    $conn = $this->connectDB();
    $query = "SELECT id,categories FROM product_info WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $categories[] = $row['categories'];
      }
    mysqli_close($conn);
    }

    if (is_array($categories)) {

      $categories = array_unique($categories);

        foreach ($categories as $catpath) {
        $catgorySing = explode('/', $catpath);

        $lastcategory = $catgorySing[2];

        $conn = $this->connectDB();

        $query = "SELECT id,name FROM categories WHERE id = '$lastcategory'";
        $result = mysqli_query($conn, $query);

          while ($row = mysqli_fetch_array($result)) {
          $categs[$row['id']] = $row['name'];
          }

      mysqli_close($conn);
      }
    }
    else {
    $categs = array();
    }
    if (is_array($categs)) {
    asort($categs);
    }
  return $categs;
  }

  // restituisce il prezzo incluso di tasse.
  function getPriceTax($price, $value) {
    $price_including_tax = $price * (1 + $value / 100);
    return number_format($price_including_tax, 2, '.', '');
  }

  // update info clienti nella tabella con RESERVED_ID
  function addUpdateCustomer($array) {
    $customer_id = $array['customer_id'];
    $user_id = $array['user_id'];
    $ragione_sociale = str_replace("'", "\'", $array['ragione_sociale']);
    $nome = str_replace("'", "\'", $array['nome']);
    $cognome = str_replace("'", "\'", $array['cognome']);
    $indirizzo = str_replace("'", "\'", $array['indirizzo']);
    $cap = str_replace("'", "\'", $array['cap']);
    $prov = str_replace("'", "\'", $array['prov']);
    $citta = str_replace("'", "\'", $array['citta']);
    $p_iva = str_replace("'", "\'", $array['p_iva']);
    $ali_iva = str_replace("'", "\'", $array['ali_iva']);
    $auto_notifica = str_replace("'", "\'", $array['auto_notifica']);
    $cod_fisc = str_replace("'", "\'", $array['cod_fisc']);
    $cod_ipa = str_replace("'", "\'", $array['cod_ipa']);
    $cad_fattura = str_replace("'", "\'", $array['cad_fattura']);
    $note_fattura = str_replace("'", "\'", $array['note_fattura']);
    $email = str_replace("'", "\'", $array['email']);
    $email_amministr = str_replace("'", "\'", $array['email_amministr']);
    $telefono_1 =  str_replace("'", "\'", $array['telefono_1']);
    $telefono_2 =  str_replace("'", "\'", $array['telefono_2']);
    $indirizzo_1 = str_replace("'", "\'", $array['indirizzo_1']);
    $indirizzo_2 = str_replace("'", "\'", $array['indirizzo_2']);
    $indirizzo_3 = str_replace("'", "\'", $array['indirizzo_3']);
    $indirizzo_4 = str_replace("'", "\'", $array['indirizzo_4']);
    $indirizzo_5 = str_replace("'", "\'", $array['indirizzo_5']);

    $conn = $this->connectDB();

    /*$query = "SELECT id FROM customers WHERE id = '$customer_id'";
    $result = mysqli_query($conn, $query);

      if ($conn->query($query)->num_rows == 0) {
      $customer_query = $conn->query("INSERT INTO customers
                    (user_id, ragione_sociale, nome, cognome, indirizzo, cap, prov, citta, p_iva,
                    ali_iva, cod_fisc, cod_ipa, cad_fattura, note_fattura, email, email_amministr,
                    telefono_1, telefono_2, indirizzo_1, indirizzo_2, indirizzo_3, indirizzo_4, indirizzo_5)
                    VALUES
                    ('$user_id', '$ragione_sociale', '$nome', '$cognome', '$indirizzo', '$cap',
                    '$prov', '$citta', '$p_iva', '$ali_iva', '$cod_fisc', '$cod_ipa', '$cad_fattura',
                    '$note_fattura', '$email', '$email_amministr', '$telefono_1', '$telefono_2',
                    '$indirizzo_1', '$indirizzo_2', '$indirizzo_3', '$indirizzo_4', '$indirizzo_5')");
      }
      else {*/
      $customer_query = $conn->query("UPDATE customers SET
                    ragione_sociale = '$ragione_sociale', nome = '$nome', cognome = '$cognome',
                    indirizzo = '$indirizzo', cap = '$cap', prov = '$prov', citta = '$citta', p_iva = '$p_iva',
                    ali_iva = '$ali_iva', auto_notifica = '$auto_notifica', cod_fisc = '$cod_fisc', cod_ipa = '$cod_ipa',
                    cad_fattura = '$cad_fattura', note_fattura = '$note_fattura', email = '$email',
                    email_amministr = '$email_amministr', telefono_1 = '$telefono_1',
                    telefono_2 = '$telefono_2', indirizzo_1 = '$indirizzo_1',
                    indirizzo_2 = '$indirizzo_2', indirizzo_3 = '$indirizzo_3',
                    indirizzo_4 = '$indirizzo_4', indirizzo_5 = '$indirizzo_5'
                    WHERE id = '$customer_id'");
      //}

      if ($customer_query) {
      $report = '<span style="color:green"><strong>Cliente salvato.</strong></span>';
      }
      else {
      $report = '<span style="color:red">'.$conn->error.'</span>';
      }
    return $report;
    mysqli_close($conn);
  }

  // toglie i RESERVE_ID non salvati
  function cleanCustomerTable($user_id) {
    $conn = $this->connectDB();
    $conn->query("DELETE FROM customers WHERE user_id = '$user_id' AND nome = 'RESERVED_ID'");
    mysqli_close($conn);
  }

  // cancella cliente
  function deleteCustomer($user_id, $delete) {
    $conn = $this->connectDB();
    $conn->query("DELETE FROM customers WHERE user_id = '$user_id' AND id = '$delete'");
    mysqli_close($conn);
  }

  // motore di ricerca dei clienti
  function getCustomersResults($user_id, $query_customer) {

    if ($query_customer == '') {
    $query_search = '';
    }
    else {
    $query_search = " AND (ragione_sociale LIKE '%$query_customer%'
                    OR nome LIKE '%$query_customer%'
                    OR cognome LIKE '%$query_customer%'
                    OR p_iva LIKE '%$query_customer%'
                    OR email LIKE '%$query_customer%'
                    OR citta LIKE '%$query_customer%')";
    }

    $conn = $this->connectDB();

    $query = "SELECT id,user_id FROM customers WHERE user_id = '$user_id' $query_search ORDER BY ragione_sociale ASC";
    $result = mysqli_query($conn, $query);

      if ($conn->query($query)->num_rows == 0) {
      $customers_ids = array();
      }
      else {
        while ($row = mysqli_fetch_array($result)) {
        $customers_ids[] = $row['id'];
        }
      }
      if (count($customers_ids) !== 0) {

        foreach ($customers_ids as $customer_id) {
          $query = "SELECT id,nome,cognome,ragione_sociale,p_iva,email,citta,telefono_1 FROM customers WHERE (id = '$customer_id')";
          $result = mysqli_query($conn, $query);

            if ($conn->query($query)->num_rows == 0) {
            $customers = array();
            }
            else {
              while ($row = mysqli_fetch_array($result)) {
              $customers[$row['id']] = array('ragione_sociale' => $row['ragione_sociale'],
                                             'citta' => $row['citta'],
                                             'email' => $row['email'],
                                             'tel' => $row['telefono_1']);
              }
            }
        }
      }
      else {
      $customers = array();
      }
    mysqli_close($conn);
    return $customers;
  }

  // motore di ricerca dei clienti da fatturare
  function getToInvoiceCustomersResults($user_id, $array_customer_id, $query_customer) {

    if ($query_customer == '') {
    $query_search = '';
    }
    else {
    $query_search = " AND (ragione_sociale LIKE '%$query_customer%'
                    OR nome LIKE '%$query_customer%'
                    OR cognome LIKE '%$query_customer%'
                    OR p_iva LIKE '%$query_customer%'
                    OR email LIKE '%$query_customer%'
                    OR citta LIKE '%$query_customer%')";
    }

    $conn = $this->connectDB();
      foreach ($array_customer_id as $customer_id) {

      $query = "SELECT id,nome,cognome,ragione_sociale,p_iva,email,citta,telefono_1 FROM customers WHERE id = '$customer_id' $query_search";
      $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
        $customers[$row['id']] = array('ragione_sociale' => $row['ragione_sociale'],
                                       'citta' => $row['citta'],
                                       'email' => $row['email'],
                                       'tel' => $row['telefono_1']);
        }
      }
    mysqli_close($conn);
    return $customers;
  }

  // inserisce e restituisce l'id prenotato RESERVED_ID
  function getNewCustomerId($user_id) {
    $conn = $this->connectDB();
    $conn->query("INSERT INTO customers (nome, user_id) VALUES ('RESERVED_ID', '$user_id')");

    $query = "SELECT id,nome,user_id FROM customers WHERE nome = 'RESERVED_ID' AND user_id = '$user_id'";
    $result = mysqli_query($conn, $query);
    $row = mysqli_fetch_assoc($result);

    return $row['id'];
    mysqli_close($conn);
  }

  // restituisce le info cliente
  function getCustomer($id) {
    $conn = $this->connectDB();
    $query = "SELECT * FROM customers WHERE id = '$id'";
    $result = mysqli_query($conn, $query);
    $row = mysqli_fetch_assoc($result);


    $array = array('customer_id' => $row['id'],
                   'ragione_sociale' => $row['ragione_sociale'],
                   'nome' => $row['nome'],
                   'cognome' => $row['cognome'],
                   'indirizzo' => $row['indirizzo'],
                   'cap' => $row['cap'],
                   'prov' => $row['prov'],
                   'citta' => $row['citta'],
                   'p_iva' => $row['p_iva'],
                   'ali_iva' => $row['ali_iva'],
                   'auto_notifica' => $row['auto_notifica'],
                   'cod_fisc' => $row['cod_fisc'],
                   'cod_ipa' => $row['cod_ipa'],
                   'cad_fattura' => $row['cad_fattura'],
                   'note_fattura' => $row['note_fattura'],
                   'email' => $row['email'],
                   'email_amministr' => $row['email_amministr'],
                   'telefono_1' =>  $row['telefono_1'],
                   'telefono_2' => $row['telefono_2'],
                   'indirizzo_1' => $row['indirizzo_1'],
                   'indirizzo_2' => $row['indirizzo_2'],
                   'indirizzo_3' => $row['indirizzo_3'],
                   'indirizzo_4' => $row['indirizzo_4'],
                   'indirizzo_5' => $row['indirizzo_5']);
    mysqli_close($conn);
    return $array;
  }

  // motore ricerca clienti nel carrello
  function getSearchCustomerCart($user_id, $query_customer) {

    if ($query_customer == '') {
    $query_search = '';
    }
    else {
    $query_search = " AND (ragione_sociale LIKE '%$query_customer%'
                    OR nome LIKE '%$query_customer%'
                    OR cognome LIKE '%$query_customer%'
                    OR p_iva LIKE '%$query_customer%'
                    OR email LIKE '%$query_customer%'
                    OR citta LIKE '%$query_customer%')";
    }

    $conn = $this->connectDB();

    $query = "SELECT id,nome,cognome,ragione_sociale,p_iva,email,citta,user_id,ali_iva FROM customers WHERE (user_id = '$user_id') $query_search ORDER BY ragione_sociale ASC";
    $result = mysqli_query($conn, $query);

      if ($conn->query($query)->num_rows == 0) {
      $customers_ids = array();
      }
      else {
        while ($row = mysqli_fetch_array($result)) {
        $customers_ids[$row['id']] = array('ragione_sociale' => $row['ragione_sociale'],
                                           'ali_iva' => $row['ali_iva']);
        }
      }
    mysqli_close($conn);
    return $customers_ids;
  }

  // rimuove oggetti dal carrello
  function flushCart($user_id) {
    $conn = $this->connectDB();
    $conn->query("DELETE FROM cart WHERE user_id = '$user_id'");
    mysqli_close($conn);
  }

  // aggiorna le quantià editate nel carrello quando si cancella un prodotto dal carrello.
  function updateChangedCartData($array, $user_id) {
    $conn = $this->connectDB();

    if (is_array($array)) {
    $array = $array;
    }
    else {
    $array = array();
    }

    foreach ($array as $key => $value) {
    $price = $value['price'];
    $qty = $value['qty'];
    $conn->query("UPDATE cart SET price = '$price', qty = '$qty' WHERE user_id = '$user_id' AND id_product = '$key'");
    }
    mysqli_close($conn);

  }

  // aggiorna le quantià editate nel carrello quando si cancella un prodotto dal carrello.
  function updateChangedSavedData($array, $order_id, $table) {
    $conn = $this->connectDB();

    foreach ($array as $key => $value) {
    $price = $value['price'];
    $qty = $value['qty'];
    $conn->query("UPDATE $table SET price = '$price', qty = '$qty' WHERE id_order = '$order_id' AND id_product = '$key'");
    }
    mysqli_close($conn);

  }

  // restituisce l'id del carrello, preventivo e ordine appena salvati
  function writeSavedCart($customer_id, $date, $user_id, $order_id, $table) {
    $conn = $this->connectDB();
    $query = "SELECT id_order,id_user FROM $table WHERE id_order = '$order_id' AND id_user = '$user_id'";

      if ($conn->query($query)->num_rows == 0 || $order_id == NULL) {
      $resultQuery = $conn->query("INSERT INTO $table (id_user, id_customer, date) VALUES ('$user_id', '$customer_id', '$date')");
      }

    $resQuery = "SELECT id_order,id_user FROM $table WHERE id_user = '$user_id' ORDER BY id_order DESC LIMIT 1";
    $resultQuery = mysqli_query($conn, $resQuery);
    $row = mysqli_fetch_assoc($resultQuery);

    return $row['id_order'];
    mysqli_close($conn);
  }


  // scrive tutte le info del carrello salvato, ordini e preventivi
  function writeSavedCartItems($array, $table_1, $table_2) {

    $order_id_array = $array['order_id'];
    $user_id = $array['user_id'];

      if ($order_id_array == NULL || $order_id_array == '') {
      $order_id = $this->writeSavedCart($array['customer_id'], $array['date'], $array['user_id'], NULL, $table_1);
      }
      else {
      $order_id = $array['order_id'];
      }

      foreach ($array['items'] as $product_id => $value) {

      $price = $value['price'];
      $qty = $value['qty'];

      $conn = $this->connectDB();

      $query = "SELECT id_order,id_product FROM $table_2 WHERE id_order = '$order_id' AND id_product = '$product_id'";

        if ($conn->query($query)->num_rows == 0) {
        $resultQuery = $conn->query("INSERT INTO $table_2 (id_order, id_product, price, qty) VALUES ('$order_id', '$product_id', '$price', '$qty')");
        }
        else {
        $resultQuery = $conn->query("UPDATE $table_2 SET price = '$price', qty = '$qty' WHERE id_order = '$order_id' AND id_product = '$product_id'");
        }

        if ($resultQuery) {
        $reportQuery[] = 1;
        }
        else {
        $reportQuery[] = 0;
        }
      mysqli_close($conn);
      }

      if (!in_array(0, $reportQuery)) {
      $report = 'Salvato correttamente';
      }
      else {
      $report = '<span style="color:red">ERRORE: '.$conn->error.'</span>';
      }
    $conn = $this->connectDB();
    $conn->query("DELETE FROM cart WHERE user_id = '$user_id'");
    mysqli_close($conn);
    return $report;
  }

  // restituisce array di ordini, carrelli salvati e preventivi
  function getSavedTable($table, $table_child, $user_id) {
    $conn = $this->connectDB();
    $result = $conn->query("SELECT * FROM $table WHERE id_user = '$user_id' ORDER BY id_order DESC");

       while ($row = mysqli_fetch_array($result)) {

       $order = $row['id_order'];

       $arry_items = array();

        $resultItems = $conn->query("SELECT * FROM $table_child WHERE id_order = '$order'");

          while ($row00 = mysqli_fetch_array($resultItems)) {
          $arry_items[$row00['id_product']] = array('qty' => $row00['qty'], 'price' => $row00['price']);
          }

       $array_order[$row['id_order']] = array('id_customer' => $row['id_customer'], 'date' => $row['date'], 'notif' => $row['notif'], 'items' => $arry_items);
       }

    return $array_order;
    mysqli_close($conn);
  }

  // restituisce array delle fatture
  function getInvoices($table, $table_child, $user_id) {
    $flag = explode('_', $table)[0];

    $conn = $this->connectDB();
    $result = $conn->query("SELECT * FROM $table ORDER BY id DESC");

       while ($row = mysqli_fetch_array($result)) {
       $order = $row['id'];

       $arry_items = array();

        $resultItems = $conn->query("SELECT * FROM $table_child WHERE id_fattura = '$order'");

          while ($row00 = mysqli_fetch_array($resultItems)) {
          $id_prod = $row00['id_product'];

          $resName = $conn->query("SELECT id,name FROM product_info WHERE id = '$id_prod'");
          $rowAs = mysqli_fetch_assoc($resName);
          $name = $rowAs['name'];

          $resCost = $conn->query("SELECT id,cost FROM product_prices WHERE id = '$id_prod'");
          $rowCost = mysqli_fetch_assoc($resCost);
          $cost = $rowCost['cost'];

          $arry_items[] = array('id_product' => $row00['id_product'], 'name' => $name, 'qty' => $row00['qty'], 'price' => number_format($row00['price'], 2, '.', ''), 'cost' => $cost);
          }

       $array_order[$row['id']] = array('id' => $row['id'],
                                        'sorting' => $row['yyyy'].$row['mm'].$row['dd'].'.'.$row['id'],
                                        'flag' => $flag,
                                        'id_customer' => $row['id_customer'],
                                        'id_user' => $row['id_user'],
                                        'dd' => $row['dd'],
                                        'mm' => $row['mm'],
                                        'yyyy' => $row['yyyy'],
                                        'items' => $arry_items);
       }

    return $array_order;
    mysqli_close($conn);
  }


  // restituisce array di ordini, carrelli salvati e preventivi di un cliente
  function getSavedOrdersCustomer($table, $table_child, $user_id, $customer_id) {
    $conn = $this->connectDB();
    $result = $conn->query("SELECT * FROM $table WHERE id_user = '$user_id' AND id_customer = '$customer_id' ORDER BY id_order DESC");

       while ($row = mysqli_fetch_array($result)) {

       $order = $row['id_order'];

       $arry_items = array();

        $resultItems = $conn->query("SELECT * FROM $table_child WHERE id_order = '$order'");

          while ($row00 = mysqli_fetch_array($resultItems)) {
          $arry_items[$row00['id_product']] = array('qty' => $row00['qty'], 'price' => $row00['price']);
          }

       $array_order[$row['id_order']] = array('id_customer' => $row['id_customer'], 'date' => $row['date'], 'notif' => $row['notif'], 'items' => $arry_items);
       }

    return $array_order;
    mysqli_close($conn);
  }

  // cancella carrello salvato, preventivi e ordini con prodotti
  function deleteSavedCarts($table_1, $table_2, $user_id, $delete)  {
    $conn = $this->connectDB();
    $conn->query("DELETE FROM $table_1 WHERE id_user = '$user_id' AND id_order = '$delete'");
    $conn->query("DELETE FROM $table_2 WHERE id_order = '$delete'");
    mysqli_close($conn);
  }


  // restituisce i dettagli dei prodotti nei carrelli salvati, ordini e preventivi
  function getSavedItemsDetails($pr_id, $table, $order_id) {
    $config = new Config;
    $image_base_path = Config::$img_path;

    $conn = $this->connectDB();

    $query = "SELECT id,name FROM product_info WHERE id = '$pr_id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $info[$row['id']] = array('name' => $row['name']);
      }

    $query = "SELECT * FROM product_inventory WHERE id = '$pr_id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $inventory[$row['id']] = array(
      'qty' => $row['qty'],
      'min_qty' => $row['min_qty'],
      'inc_qty' => $row['inc_qty']
      );
      }

    $query = "SELECT * FROM $table WHERE id_product = '$pr_id' AND id_order = '$order_id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $saved[$row['id_product']] = array(
      'qty' => $row['qty'],
      'price' => number_format($row['price'], 2, '.', '')
      );
      }

    $query = "SELECT id,cost,price FROM product_prices WHERE id = '$pr_id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $prices[$row['id']] = array(
      'cost' => number_format($row['cost'], 2, '.', ''),
      'price' => number_format($row['price'], 2, '.', '')
      );
      }

    $query = "SELECT * FROM product_media WHERE id = '$pr_id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $image_file = $image_base_path.$row['image'];

      /*$handle = curl_init($image_file);
      curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

      $response = curl_exec($handle);
      $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        if($httpCode == 404) {
        $product_image = '/media/images/placeholder.jpg';
        }
        else {
        $product_image = $image_base_path.$row['image'];
        }
        curl_close($handle);*/

      $media[$row['id']] = array(
      'image' => $image_file
      );
      }

    mysqli_close($conn);

      foreach ($saved as $key => $value) {
      $array = array(
      'saved' => $saved[$key],
      'info' => $info[$key],
      'inventory' => $inventory[$key],
      'prices' => $prices[$key],
      'media' => $media[$key]
      );
      }
    return $array;
  }


  // restituisce l'array con le informazioni del carrello salvato, preventivi e ordini e i dettagli dei prodotti
  function getSavedItems($order_id, $table) {
    $conn = $this->connectDB();
    $result = $conn->query("SELECT * FROM $table WHERE id_order = '$order_id'");

      while ($row = mysqli_fetch_array($result)) {
      $array[] = $row['id_product'];
      }

      foreach ($array as $value) {
      $array_final[$value] = $this->getSavedItemsDetails($value, $table, $order_id);
      }
    mysqli_close($conn);
    return $array_final;
  }

  // restituisce la data di un ordine, preventivo o carrello salvato
  function getSavedDate($order_id, $table) {
    $conn = $this->connectDB();
    $result = $conn->query("SELECT * FROM $table WHERE id_order = '$order_id'");
    $row = mysqli_fetch_assoc($result);
    return $row['date'];
    mysqli_close($conn);
  }

  // inserice i prodotti dei carrelli salvati, dei preventivi e degli ordini nel carrello live
  function addSavedToCart($user_id, $id_prod, $price, $qty, $id_order, $table) {
    $conn = $this->connectDB();
    $conn->query("INSERT INTO cart (user_id, id_product, qty, price) VALUES ('$user_id', '$id_prod', '$qty', '$price')");
    $conn->query("UPDATE $table SET qty = '$qty', price = '$price' WHERE id_order = '$id_order' AND id_product = '$id_prod'");
    mysqli_close($conn);
  }

  // salva il carrello live
  function saveCart($id_prod, $price, $qty, $id_order, $table) {
    $conn = $this->connectDB();
    $conn->query("UPDATE $table SET qty = '$qty', price = '$price' WHERE id_order = '$id_order' AND id_product = '$id_prod'");
    mysqli_close($conn);
  }

  // cancella un prodotto dal carrello salvato, dal preventivo o dall'ordine
  function deleteSavedItem($order_id, $table, $delete) {
    $conn = $this->connectDB();
    $conn->query("DELETE FROM $table WHERE id_order = '$order_id' AND id_product = '$delete'");
    mysqli_close($conn);
  }

   // rimuove un prodotto dalla lista fatture
  function deleteSavedItemToInvoice($order_id, $delete) {
    $conn = $this->connectDB();
    $conn->query("UPDATE send_order_items SET removed = '1' WHERE id_order = '$order_id' AND id_product = '$delete'");
    mysqli_close($conn);
  }

  // motore di ricerca dei carrelli salvati, ordini e preventivi (cerca per nome cliente)
  function getSavedResults($user_id, $query_saved, $table, $table_1) {

    $conn = $this->connectDB();

    if ($query_saved == '') {
    $theLike = '';
    }
    else {
    $theLike = " WHERE ragione_sociale LIKE '%$query_saved%'";
    }

    $res = $conn->query("SELECT ragione_sociale,id FROM customers $theLike");

      while ($row = mysqli_fetch_array($res)) {
      $customers_ids[] = $row['id'];
      }


      foreach ($customers_ids as $cust_id) {
      $query = "SELECT * FROM $table WHERE id_customer = '$cust_id'";
      $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
        $order_ids[] = $row['id_order'];
        }
      }

      arsort($order_ids); //reverse order

        foreach ($order_ids as $order_id) {
        $result = $conn->query("SELECT * FROM $table WHERE id_user = '$user_id' AND id_order = '$order_id'");

          while ($row = mysqli_fetch_array($result)) {
          $order = $row['id_order'];
          $arry_items = array();
          $resultItems = $conn->query("SELECT * FROM $table_1 WHERE id_order = '$order_id'");

            while ($row00 = mysqli_fetch_array($resultItems)) {
            $arry_items[$row00['id_product']] = array('qty' => $row00['qty'], 'price' => $row00['price']);
            }
            $array_order[$row['id_order']] = array('id_customer' => $row['id_customer'], 'date' => $row['date'], 'notif' => $row['notif'], 'items' => $arry_items);
          }
        }
      //}

    return $array_order;
    mysqli_close($conn);
  }

  // motore di ricerca per fatture (cerca per nome cliente)
  function getSavedInvoicesSearch($user_id, $query_saved, $table, $table_1) {
    $flag = explode('_', $table)[0];
    $conn = $this->connectDB();

    if ($query_saved == '') {
    $theLike = '';
    }
    else {
    $theLike = " WHERE ragione_sociale LIKE '%$query_saved%'";
    }

    $res = $conn->query("SELECT ragione_sociale,id FROM customers $theLike");

      while ($row = mysqli_fetch_array($res)) {
      $customers_ids[] = $row['id'];
      }


      foreach ($customers_ids as $cust_id) {
      $query = "SELECT * FROM $table WHERE id_customer = '$cust_id'";
      $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
        $order_ids[] = $row['id'];
        }
      }

      arsort($order_ids); //reverse order

        foreach ($order_ids as $order_id) {

        //$result = $conn->query("SELECT * FROM $table WHERE id_user = '$user_id' AND id = '$order_id'");
          $result = $conn->query("SELECT * FROM $table WHERE id = '$order_id'");

          while ($row = mysqli_fetch_array($result)) {
          $order = $row['id'];
          $arry_items = array();

          $resultItems = $conn->query("SELECT * FROM $table_1 WHERE id_fattura = '$order_id'");

            while ($row00 = mysqli_fetch_array($resultItems)) {
            $id_prod = $row00['id_product'];

            $resName = $conn->query("SELECT id,name FROM product_info WHERE id = '$id_prod'");
            $rowAs = mysqli_fetch_assoc($resName);
            $name = $rowAs['name'];

            $resCost = $conn->query("SELECT id,cost FROM product_prices WHERE id = '$id_prod'");
            $rowCost = mysqli_fetch_assoc($resCost);
            $cost = $rowCost['cost'];

            $arry_items[] = array('id_product' => $row00['id_product'], 'name' => $name, 'qty' => $row00['qty'], 'price' => $row00['price'], 'cost' => $cost);
            }
            $array_order[$row['id']] = array('id' => $row['id'],
                                        'sorting' => $row['yyyy'].$row['mm'].$row['dd'].'.'.$row['id'],
                                        'flag' => $flag,
                                        'id_customer' => $row['id_customer'],
                                        'id_user' => $row['id_user'],
                                        'dd' => $row['dd'],
                                        'mm' => $row['mm'],
                                        'yyyy' => $row['yyyy'],
                                        'items' => $arry_items);
          }
        }
      //}

    return $array_order;
    mysqli_close($conn);
  }

  // numero ordini, preventivi o carrelli salvati
  function getSavedSize($user_id, $table) {
    $conn = $this->connectDB();
    $query = "SELECT id_user FROM $table WHERE id_user = '$user_id'";
    $result = mysqli_query($conn, $query);
    $size = $conn->query($query)->num_rows;
    mysqli_close($conn);
    return $size;
  }

  // numero ordini, preventivi o carrelli salvati per cliente
  function getSavedCustomerSize($user_id, $table, $customer_id) {
    $conn = $this->connectDB();
    $query = "SELECT id_user,id_customer FROM $table WHERE id_user = '$user_id' AND id_customer = '$customer_id'";
    $result = mysqli_query($conn, $query);
    $size = $conn->query($query)->num_rows;
    mysqli_close($conn);
    return $size;
  }

  // check se customer ha indirizzo
  function getIfAddress($customer_id) {
    $conn = $this->connectDB();
    $query = "SELECT id,indirizzo_1,indirizzo_2,indirizzo_3,indirizzo_4,indirizzo_5
              FROM customers WHERE id = '$customer_id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $array_address = array('indirizzo_1' => str_replace("'", "\'", $row['indirizzo_1']),
                             'indirizzo_2' => str_replace("'", "\'", $row['indirizzo_2']),
                             'indirizzo_3' => str_replace("'", "\'", $row['indirizzo_3']),
                             'indirizzo_4' => str_replace("'", "\'", $row['indirizzo_4']),
                             'indirizzo_5' => str_replace("'", "\'", $row['indirizzo_5'])
                             );
      }
      mysqli_close($conn);
      $array_address = array_filter($array_address);
      return count($array_address);
    }

  // recupera gli indirizzi di spedizione dei clienti
  function getCustomerShippingAddress($customer_id, $order_id, $user_id) {
    $conn = $this->connectDB();
    $query = "SELECT id,indirizzo_1,indirizzo_2,indirizzo_3,indirizzo_4,indirizzo_5
              FROM customers WHERE id = '$customer_id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {
      $array_address = array('indirizzo_1' => str_replace("'", "\'", $row['indirizzo_1']),
                             'indirizzo_2' => str_replace("'", "\'", $row['indirizzo_2']),
                             'indirizzo_3' => str_replace("'", "\'", $row['indirizzo_3']),
                             'indirizzo_4' => str_replace("'", "\'", $row['indirizzo_4']),
                             'indirizzo_5' => str_replace("'", "\'", $row['indirizzo_5'])
                             );
      }

      $array_address = array_filter($array_address);

      $isSent = $this->getIfOrderIsSend($order_id, $user_id);

      if (count($array_address) == 0) {
      $content = '<span style="color:red">Inserisci un indirizzo di spedizione per questo cliente!</span>';
      }
      else if (count($array_address) == 1 || $isSent == $order_id) {
      $content = 'INDIRIZZO DI SPEDIZIONE:<br>';
      $content .= '<input type="hidden" id="address" name="address" value="'.array_keys($array_address)[0].'">';
      $content .= '<div style="margin-bottom:25px"><strong>'.array_values($array_address)[0].'</strong></div>';
      }

      else if (count($array_address) > 1) {
      $content = '<span id="label_address_search">SELEZIONA INDIRIZZO DI SPEDIZIONE:</span>';
      $content .= '<div style="margin-bottom:25px;display:none;" id ="label_hide_search_address"></div>';
      $content .= '<select name="address" id="address" class="input_modal">';
        $a = 0;
        foreach ($array_address as $key => $address) {
        $a++;
          if ($a == 1) {
          $option = 'selected="selected"';
          }
          else {
          $option = '';
          }
        $content .= '<option '.$option.' value="'.$key.'">'.$address.'</option>';
        }
      $content .= '</select>';
      }

    mysqli_close($conn);
    return $content;
  }


  //scrive tabella send_orders
  function writeSendOrder($array, $order_id, $user_id, $customer_id, $date, $address) {
    $conn = $this->connectDB();

    //$today = date('d-m-Y');

    $conn->query("INSERT INTO send_orders
              (id_order, id_user, id_customer, date, shipping_address)
              VALUES ('$order_id', '$user_id', '$customer_id', '$date', '$address')");

    mysqli_close($conn);

      foreach ($array as $product_id => $value) {
      $conn = $this->connectDB();

      $price = $value['price'];
      $qty = $value['qty'];

      $conn->query("INSERT INTO send_order_items
              (id_order, id_product, qty, price)
              VALUES ('$order_id', '$product_id', '$qty', '$price')");

      $conn->query("UPDATE saved_order_items SET
                    qty = '$qty', price = '$price' WHERE
                    id_order = '$order_id' AND id_product = '$product_id'");

      mysqli_close($conn);
      }

    $conn = $this->connectDB();
    $conn->query("UPDATE saved_orders SET send = '1', date = '$date' WHERE id_order = '$order_id' AND id_user = '$user_id'");
    mysqli_close($conn);
  }

  function getIfOrderIsSend($order_id, $user_id) {
    $conn = $this->connectDB();
    $res = $conn->query("SELECT id_order,id_user,id_customer,send FROM saved_orders WHERE id_order = '$order_id' AND id_user = '$user_id' AND send = '1'");
    $row = mysqli_fetch_assoc($res);
    return $row['id_order'];
    mysqli_close($conn);
  }

  function getIfEstimateIsOrder($order_id, $user_id) {
    $conn = $this->connectDB();
    $res = $conn->query("SELECT id_order,id_user,id_customer,ordered FROM saved_estimates WHERE id_order = '$order_id' AND id_user = '$user_id' AND ordered = '1'");
    $row = mysqli_fetch_assoc($res);
    return $row['id_order'];
    mysqli_close($conn);
  }

  function getOrdersStats($user_id, $customer_id) {
    $conn = $this->connectDB();
    $res = $conn->query("SELECT * FROM  send_orders WHERE id_user = '$user_id' AND id_customer = '$customer_id' ORDER BY id_order DESC");
      while ($row = mysqli_fetch_array($res)) {
      $orders[] = $row['id_order'];
      }


      $total_Array = array();
      foreach ($orders as $order) {
      $res = $conn->query("SELECT * FROM  send_order_items WHERE id_order = '$order'");

        $order_items = array();
        while ($row = mysqli_fetch_array($res)) {

        $id_product = $row['id_product'];
        $que = $conn->query("SELECT id,cost FROM product_prices WHERE id = '$id_product'");
        $row00 = mysqli_fetch_assoc($que);

        $cost = $row00['cost'];

        $order_items[$id_product] = array(
                               'price' => $row['price'],
                               'qty' => $row['qty'],
                               'qty_invoiced' => $row['qty_invoiced'],
                               'cost' => $cost,
                               'total_price' => number_format($row['price'] * $row['qty_invoiced'], 2 ,'.', ''),
                               'total_cost' => number_format($cost * $row['qty_invoiced'], 2 ,'.', ''),
                               'removed' => $row['removed']
                              );
        }
      $total_Array[$order] = array('items' => $order_items);
      }
    mysqli_close($conn);
    return $total_Array;
  }


  function getInvoicesStatsByCollection($array) {

    $arr = array();
    foreach ($array as $key => $orders) {
    $prices = array();
    $costs = array();
      foreach ($orders['items'] as $order) {
        $prices[] = $order['price'] * $order['qty'];
        $costs[] = $order['cost'] * $order['qty'];
      }
     $arr[$key] = array('total_prices' => array_sum($prices), 'total_costs' => array_sum($costs));
    }
  return $arr;
  }


  function getTopSellerStats($array) {
    $prices = array();
    $costs = array();
    foreach ($array as $order) {
    $prices[] = $order['inventory']['total_prices'];
    $costs[] = $order['prices']['cost'] * $order['inventory']['qty_invoiced'];
    }
    $arr = array('total_prices' => array_sum($prices), 'total_costs' => array_sum($costs));
  return $arr;
  }
  /*
  function getInvoicesStats($user_id) {
    $year = date('Y');
    $conn = $this->connectDB();
    $res = $conn->query("SELECT * FROM invoices_$year WHERE id_user = '$user_id' ORDER BY id DESC");
      while ($row = mysqli_fetch_array($res)) {
      $orders[] = $row['id'];
      }

      $total_Array = array();
      foreach ($orders as $order) {
      $res = $conn->query("SELECT * FROM  invoice_items_$year WHERE id_fattura = '$order'");

        $order_items = array();
        while ($row = mysqli_fetch_array($res)) {

        $id_product = $row['id_product'];
        $que = $conn->query("SELECT id,cost FROM product_prices WHERE id = '$id_product'");
        $row00 = mysqli_fetch_assoc($que);

        $cost = $row00['cost'];

        $order_items[] = array('id_product' => $id_product,
                               'price' => $row['price'],
                               'qty' => $row['qty'],
                               'cost' => $cost,
                               'total_price' => number_format($row['price'] * $row['qty'], 2 ,'.', ''),
                               'total_cost' => number_format($cost * $row['qty'], 2 ,'.', '')
                              );
        }
      $total_Array[$order] = array('items' => $order_items);
      }
    mysqli_close($conn);
    return $total_Array;
  } */

  // restuisce i prodotti ordinati per id cliente
  function getProductsOrderedByCustomer($id_customer, $searchquery, $filter) {
    $config = new Config;
    $image_base_path = Config::$img_path;

    $conn = $this->connectDB();


    if ($id_customer == NULL) {
    $result = $conn->query("SELECT id_order,id_customer,date FROM send_orders");
    }
    else {
    $result = $conn->query("SELECT id_order,id_customer,date FROM send_orders WHERE id_customer = '$id_customer'");
    }

      while ($row = mysqli_fetch_array($result)) {
      $id_orders[$row['id_order']] = $row['date'];
      }

      if (is_array($id_orders)) {
      $id_orders = $id_orders;
      }
      else {
      $id_orders = array();
      }

      foreach ($id_orders as $id_order => $date) {
      $res = $conn->query("SELECT id_product,id_order,price,qty,qty_invoiced FROM send_order_items WHERE id_order = '$id_order'");

        while ($row = mysqli_fetch_array($res)) {

          $qty_to_invoice = $row['qty'] - $row['qty_invoiced'];

            if ($row['qty'] == $row['qty_invoiced'] && $row['qty_invoiced'] > 0) {
            $products[] = array('id_product' => $row['id_product'], 'price' => $row['price'], 'date' => $date);
            $countID[] = $row['id_product'];
            }
        }

      }

      if (is_array($products)) {
      $products = $products;
      }
      else {
      $products = array();
      }

      foreach ($products as $product) {
      $id = $product['id_product'];

      $counts = array_count_values($countID);
      $occurr = $counts[$id];


      $res = $conn->query("SELECT id_product,qty_invoiced,price FROM send_order_items WHERE id_product = '$id'");
        $qty_inv = array();
        $price_tot = array();
        while ($row = mysqli_fetch_array($res)) {
        $qty_inv[] = $row['qty_invoiced'];
        $price_tot[] = $row['qty_invoiced'] * $row['price'];
        }
        $sum_qtys = array_sum($qty_inv);
        $sum_prices = array_sum($price_tot);
        $mid_price = $sum_prices / $sum_qtys;

      $query = "SELECT * FROM product_info WHERE id = '$id'";
      $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
        $info[$row['id']] = array(
        'id' => $row['id'],
        'occurr' => $occurr,
        'name' => $row['name'],
        'description' => $row['description'],
        'categories' => $row['categories'],
        'date' => $product['date']
        );
        }

      $query = "SELECT * FROM product_inventory WHERE id = '$id'";
      $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
        $inventory[$row['id']] = array(
        'qty' => $row['qty'],
        'min_qty' => $row['min_qty'],
        'inc_qty' =>  $row['inc_qty'],
        'qty_invoiced' => $sum_qtys,
        'total_prices' => number_format($sum_prices,2 ,'.', ''),
        'average_price' => number_format($mid_price,2 ,'.', ''),
        'reorder' =>  $row['reorder']
        );
        }

      $query = "SELECT * FROM product_prices WHERE id = '$id'";
      $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
        $prices[$row['id']] = array(
        'cost' => number_format($row['cost'], 2, '.', ''),
        'price' => number_format($product['price'], 2, '.', ''),
        'price_listino' => number_format($row['price'], 2, '.', ''),
        'variation' => $row['variation']
        );
        }

      $query = "SELECT * FROM product_attributes WHERE id = '$id'";
      $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
        $attributes[$row['id']] = array(
        'brand' => $row['brand'],
        'ean' => $row['ean'],
        'mpn' => $row['mpn'],
        'cod_alt_1' => $row['cod_alt_1'],
        'cod_alt_2' => $row['cod_alt_2'],
        'weight' => number_format($row['weight'], 2, '.', '')
        );
        }

    $query = "SELECT * FROM product_media WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

      while ($row = mysqli_fetch_array($result)) {

      $image_file = $image_base_path.$row['image'];

      /*$handle = curl_init($image_file);
      curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

      $response = curl_exec($handle);
      $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        if($httpCode == 404) {
        $product_image = '/media/images/placeholder.jpg';
        }
        else {
        $product_image = $image_base_path.$row['image'];
        }
        curl_close($handle);*/

      $media[$row['id']] = array('image' => $image_file);
      }
      }

      if ($filter == '-/-') {
      $filter = NULL;
      }

      $filter_start = explode('/', $filter)[0];
      $filter_m_start = explode('-', $filter_start)[0];
      $filter_y_start = explode('-', $filter_start)[1];

      $filter_end = explode('/', $filter)[1];
      $filter_m_end = intval(explode('-', $filter_end)[0]);
      $filter_y_end = intval(explode('-', $filter_end)[1]);

      if (is_array($prices)) {
      $prices = $prices;
      }
      else {
      $prices = array();
      }

      foreach ($prices as $key => $value) {
      $name = strtoupper($info[$key]['name']);
      $idSTR = strtoupper($info[$key]['id']);
      $searchSTR = strtoupper($searchquery);

      $date = explode(' ', $info[$key]['date'])[0];
      $month = intval(explode('-', $date)[1]);
      $year = intval(explode('-', $date)[2]);

      if (@strpos($name, $searchSTR) !== false || @strpos($idSTR, $searchSTR) !== false || $searchSTR == NULL) {

        if ($filter_m_start <= $month && $filter_m_end >= $month && $filter_y_start <= $year && $filter_y_end >= $year && $filter !== NULL) {
        //echo $filter_m_start.' '.$month;
        $array[$key] = array(
        'info' => $info[$key],
        'inventory' => $inventory[$key],
        'prices' => $prices[$key],
        'attributes' => $attributes[$key],
        'media' => $media[$key]
        );
        }
        else if ($filter == NULL) {
        $array[$key] = array(
        'info' => $info[$key],
        'inventory' => $inventory[$key],
        'prices' => $prices[$key],
        'attributes' => $attributes[$key],
        'media' => $media[$key]
        );
        }

      }
      else if ($searchquery == NULL && $filter == NULL) {
      $array[$key] = array(
      'info' => $info[$key],
      'inventory' => $inventory[$key],
      'prices' => $prices[$key],
      'attributes' => $attributes[$key],
      'media' => $media[$key]
      );
      }
    }
    mysqli_close($conn);
    return $array;
  }

  // restituisce il prezzo del prodotto dell'ultimo ordine in base al cliente
  function getIfReordered($product_id, $customer_id) {
    $conn = $this->connectDB();
    $result = $conn->query("SELECT id_order,id_customer FROM send_orders WHERE id_customer = '$customer_id'");
       while ($row = mysqli_fetch_array($result)) {
       $orders_id[] = $row['id_order'];
       }

       if (is_array($orders_id)) {
       $orders_id = $orders_id;
       }
       else {
       $orders_id = array();
       }

       foreach ($orders_id as $order_id) {
        $res = $conn->query("SELECT id_product,id_order,price FROM send_order_items
                            WHERE id_product = '$product_id' AND id_order = '$order_id'");
          if ($res) {
            while ($row = mysqli_fetch_array($res)) {
            $reordered_product[$row['id_product']] = array('last_price' => $row['price']);
            }
          }
          else {
          $reordered_product[$product_id] = array('last_price' => NULL);
          }
       }
    mysqli_close($conn);
    return $reordered_product;
  }

  function getCustomerEmail($customer_id, $user_id) {
    $conn = $this->connectDB();
    $result = $conn->query("SELECT id,email,email_amministr FROM customers WHERE id = '$customer_id' AND user_id = '$user_id'");

      if ($result->num_rows == 0) {
      $return = '<label>INSERISCI EMAIL CLIENTE</label>';
      $return .= '<input name="email" type="email" id="email_to_pdf_saved" class="input_modal">';
      }
      else if ($result->num_rows > 0) {

      $row = mysqli_fetch_assoc($result);
      $arraymail = array($row['email'], $row['email_amministr']);

      $array_filter = array_filter($arraymail);

        if (count($array_filter) == 0) {
        $return = '<label>INSERISCI EMAIL CLIENTE</label>';
        $return .= '<input name="email" type="email" id="email_to_pdf_saved" class="input_modal">';
        }
        else if (count($array_filter) == 1) {
        $return = '<label>EMAIL CLIENTE</label>';
        $return .= '<input name="email" type="email" id="email_to_pdf_saved" class="input_modal" value="'.$arraymail[0].'">';
        }
        else if (count($array_filter) > 1) {
        $return = '<label>SELEZIONA EMAIL CLIENTE</label>';
        $return .= '<select name="email" id="email_to_pdf_saved" class="input_modal">';
        $return .= '<option value="">NON INVIARE</option>';
          $a = 0;
          foreach ($array_filter as $option) {
          $a++;
            if ($a == 1) {
            $selectedOption = 'selected="selected"';
            }
            else {
            $selectedOption = '';
            }

          $return .= '<option '.$selectedOption.' value="'.$option.'">'.$option.'</option>';
          }
        $return .= '</select>';
        }
      }
    mysqli_close($conn);
    return $return;
  }

  function getCustomerEmailNoInvoice($customer_id, $user_id) {
    $conn = $this->connectDB();
    $result = $conn->query("SELECT id,email,email_amministr FROM customers WHERE id = '$customer_id' AND user_id = '$user_id'");

      if ($result->num_rows == 0) {
      $return = '<label>INSERISCI EMAIL CLIENTE</label>';
      $return .= '<input name="email" type="email" id="email_to_pdf_saved_black" class="input_modal">';
      }
      else if ($result->num_rows > 0) {

      $row = mysqli_fetch_assoc($result);
      $arraymail = array($row['email'], $row['email_amministr']);

      $array_filter = array_filter($arraymail);

        if (count($array_filter) == 0) {
        $return = '<label>INSERISCI EMAIL CLIENTE</label>';
        $return .= '<input name="email" type="email" id="email_to_pdf_saved_black" class="input_modal">';
        }
        else if (count($array_filter) == 1) {
        $return = '<label>EMAIL CLIENTE</label>';
        $return .= '<input name="email" type="email" id="email_to_pdf_saved_black" class="input_modal" value="'.$arraymail[0].'">';
        }
        else if (count($array_filter) > 1) {
        $return = '<label>SELEZIONA EMAIL CLIENTE</label>';
        $return .= '<select name="email" id="email_to_pdf_saved_black" class="input_modal">';
        $return .= '<option value="">NON INVIARE</option>';
          $a = 0;
          foreach ($array_filter as $option) {
          $a++;
            if ($a == 1) {
            $selectedOption = 'selected="selected"';
            }
            else {
            $selectedOption = '';
            }

          $return .= '<option '.$selectedOption.' value="'.$option.'">'.$option.'</option>';
          }
        $return .= '</select>';
        }
      }
    mysqli_close($conn);
    return $return;
  }

  // Conta gli elementi da mostrare nel left menu in new_customer.php
  function countItemsNewCustomerLeft($customer_id, $user_id, $table_1, $table_2) {
    $conn = $this->connectDB();
    $result = $conn->query("SELECT id_order,id_customer,id_user FROM $table_1 WHERE
                  id_customer = '$customer_id' AND id_user = '$user_id'");

      while ($row = mysqli_fetch_array($result)) {
      $id_orders[] = $row['id_order'];
      }

      if (is_array($id_orders)) {
      $id_orders = $id_orders;
      }
      else {
      $id_orders = array();
      }

      foreach ($id_orders as $id_order) {
      $result = $conn->query("SELECT * FROM $table_2 WHERE id_order = '$id_order'");

        while ($row = mysqli_fetch_array($result)) {
        $qty_to_invoice = $row['qty'] - $row['qty_invoiced'];
          if ($qty_to_invoice > 0) {
          $products[] = $row['id_product'];
          }
        }
      }
    mysqli_close($conn);
    $count = count($products);
    return $count;
  }


  // restituisce i prodotti ordinati e non ancora fatturati
  function getItemsForInvoice($customer_id, $user_id) {
    $conn = $this->connectDB();
    $result = $conn->query("SELECT id_order,id_customer,id_user FROM send_orders WHERE
                  id_customer = '$customer_id' AND id_user = '$user_id' ORDER BY id_order DESC");

      while ($row = mysqli_fetch_array($result)) {
      $id_orders[] = $row['id_order'];
      }

      if (is_array($id_orders)) {
      $id_orders = $id_orders;
      }
      else {
      $id_orders = array();
      }

      foreach ($id_orders as $id_order) {
      $result = $conn->query("SELECT * FROM send_order_items WHERE id_order = '$id_order' AND removed = '0'");
      //$result = $conn->query("SELECT * FROM send_order_items WHERE id_order = '$id_order'");

        while ($row = mysqli_fetch_array($result)) {
        $qty_to_invoice = $row['qty'] - $row['qty_invoiced'];
        //echo $qty_to_invoice.' - ';
          if ($qty_to_invoice > 0) {
          $products[] = array('id_product' => $row['id_product'],
                              'qty' => $row['qty'],
                              'qty_invoiced' => $row['qty_invoiced'],
                              'qty_to_ivoice' => $qty_to_invoice,
                              'price' => $row['price'],
                              'id_order' => $row['id_order']);
          }
          else {
          $products[] = NULL;
          }
        }
      }
    mysqli_close($conn);

      if (is_array($products)) {
      $products = $products;
      }
      else {
      $products = array();
      }

      foreach ($products as $key => $row) {
      $mid[$key]  = $row['id_product'];
      }

      if (is_array($mid)) {
      $mid = $mid;
      }
      else {
      $mid = array();
      }

    array_multisort($mid, SORT_ASC, $products);

    return array_filter($products);
  }



  function getNewInvoiceId($invoice_id, $customer_id, $id_user) {
    $conn = $this->connectDB();
      if ($invoice_id == '' || $invoice_id == NULL) {
      $year = date('Y');
      $month = date('m');
      $day = date('d');

      $conn->query("INSERT INTO invoices_$year (id_user, id_customer, dd, mm, yyyy)
                    VALUES ('$id_user', '$customer_id', '$day', '$month', '$year')");
      }
      $res = $conn->query("SELECT id,dd,mm,yyyy FROM invoices_$year ORDER BY id DESC LIMIT 1");
      $row = mysqli_fetch_assoc($res);
      $ret = array('id' => $row['id'], 'date' => $row['dd'].'-'.$row['mm'].'-'.$row['yyyy']);
    mysqli_close($conn);
    return $ret;
  }


  function invoiceItems($order_id, $customer_id, $codice_prodotto, $numFattura, $price, $qty) {
    $year = date('Y');
    $month = date('m');
    $day = date('d');

    $conn = $this->connectDB();

    if ($qty !== '0') {
    $conn->query("INSERT INTO invoice_items_$year (id_order, id_customer, id_fattura, id_product, qty, price)
                  VALUES ('$order_id', '$customer_id', '$numFattura', '$codice_prodotto', '$qty', '$price')");
    }
    $conn->query("UPDATE send_order_items SET qty_invoiced = '$qty', price = '$price'
                  WHERE id_order = '$order_id' AND id_product = '$codice_prodotto'");

    mysqli_close($conn);
  }

  /////////////
  function getNewNoInvoiceId($invoice_id, $customer_id, $id_user) {
    $conn = $this->connectDB();
      if ($invoice_id == '' || $invoice_id == NULL) {
      $year = date('Y');
      $month = date('m');
      $day = date('d');

      $conn->query("INSERT INTO noinvoices_$year (id_user, id_customer, dd, mm, yyyy)
                    VALUES ('$id_user', '$customer_id', '$day', '$month', '$year')");
      }
      $res = $conn->query("SELECT id,dd,mm,yyyy FROM noinvoices_$year ORDER BY id DESC LIMIT 1");
      $row = mysqli_fetch_assoc($res);
      $ret = array('id' => $row['id'], 'date' => $row['dd'].'-'.$row['mm'].'-'.$row['yyyy']);
    mysqli_close($conn);
    return $ret;
  }


  function NoInvoiceItems($order_id, $customer_id, $codice_prodotto, $numFattura, $price, $qty) {
    $year = date('Y');
    $month = date('m');
    $day = date('d');

    $conn = $this->connectDB();

    if ($qty !== '0') {
    $conn->query("INSERT INTO noinvoice_items_$year (id_order, id_customer, id_fattura, id_product, qty, price)
                  VALUES ('$order_id', '$customer_id', '$numFattura', '$codice_prodotto', '$qty', '$price')");
    }
    $conn->query("UPDATE send_order_items SET qty_invoiced = '$qty', price = '$price'
                  WHERE id_order = '$order_id' AND id_product = '$codice_prodotto'");

    mysqli_close($conn);
  }
  ///////////////

  function resetItemsToInvoice($customer_id) {
    $conn = $this->connectDB();
    $result = $conn->query("SELECT id_order,id_customer FROM send_orders WHERE id_customer = '$customer_id'");
      while ($row = mysqli_fetch_array($result)) {
      $orders[] = $row['id_order'];
      }

      foreach ($orders as $order) {
      $conn->query("UPDATE send_order_items SET removed = '0' WHERE id_order = '$order'");
      }

    mysqli_close($conn);
  }

  function markEstimateAsOrdered($order_id) {
    $conn = $this->connectDB();
    $conn->query("UPDATE saved_estimates SET ordered = '1' WHERE id_order = '$order_id'");
    mysqli_close($conn);
  }

  function getToInvoiceSize($user_id) {
    $conn = $this->connectDB();
    $res = $conn->query("SELECT id_product,id_order,price,qty,qty_invoiced,removed FROM send_order_items WHERE removed = '0'");

        while ($row = mysqli_fetch_array($res)) {

          $qty_to_invoice = $row['qty'] - $row['qty_invoiced'];

            if ($row['qty'] == $row['qty_invoiced'] && $row['qty_invoiced'] > 0) {
            $orders[] = $row['id_order'];
            }
        }
    $unique_orders = array_unique($orders);

      foreach ($unique_orders as $order_id) {
      $result = $conn->query("SELECT id_order,id_customer FROM send_orders WHERE id_order = '$order_id' AND id_user = '$user_id'");
        while ($row = mysqli_fetch_array($result)) {
        $customers[] = $row['id_customer'];
        }
      }
    $customer_uni = array_unique($customers);
    mysqli_close($conn);
    return $customer_uni;
  }

}

?>