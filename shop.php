<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Shop Prodotti Medusaufficio</title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
  <?php require_once ('templates/search.php') ?>
  </header>

  <div id="main_container">
    <?php
    $_categories = $comiteg->getCategoriesXML(0);
    foreach ($_categories as $key => $_category) {
      $second_category = $comiteg->getCategoriesXML($key);
      if ($second_category !== NULL) {
    ?>
    <div id="anchor_<?php echo $key ?>" style="margin-top:-30px;padding-top:30px">
      <button onclick="changeIcon(this)" class="category_container" data-toggle="collapse" data-target="#category_<?php echo $key ?>">
        <?php echo $_category['name'] ?>
        <span class="icon_category" id="span_<?php echo $key ?>"><i class="fa fa-minus" aria-hidden="true"></i></span>
      </button>

      <div id="category_<?php echo $key ?>" class="collapse in collapse_container">
        <?php

        foreach ($second_category as $second_id => $secondCat) {
        $third_categories = $comiteg->getCategoriesXML($second_id);
        if ($third_categories !== NULL) {
        reset($third_categories);
        $third_id = key($third_categories);
        }
        else {
        $third_id = NULL;
        }
        ?>
        <a href="/category.php?m=<?php echo $key ?>&c=<?php echo $second_id ?>&cat=<?php echo $third_id ?>&page=1">
          <div class="col-md-3 col-xs-12">
            <div class="col-xs-12 category_item effect2">
              <div class="col-xs-12">
                <img class="img_category" src="<?php echo  $secondCat['image'] ?>"  onerror="this.onerror=null;this.src='<?php echo $placeholder_img ?>';">
              </div>
              <div class="col-xs-12 category_name">
                <?php echo $secondCat['name'] ?>
              </div>
            </div>
          </div>
        </a>
        <?php
        }
        ?>
      </div>
    </div>
    <?php
    }
    }
    ?>
  </div>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>

</body>
</html>