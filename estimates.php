<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);

if ($_POST['table_1'] == 'saved_carts') {
$comiteg->deleteSavedCarts($_POST['table_1'], $_POST['table_2'], $user_id, $_POST['order_id']);
}

$table_1 = 'saved_estimates';
$table_2 = 'saved_estimate_items';
$link_page = '/estimates.php';

if (isset($_POST['customer_id'])) {
$date = date('d-m-Y H:i');
$customer_id = $_POST['customer_id'];
$product_name = str_replace("'", "\'", $_POST['name']);
$order_id = $_GET['order_id'];
$product_price = $_POST['price'];
$product_qty = $_POST['qty'];

foreach ($product_name as $key => $_item) {
$name = $product_name[$key];
$price = $product_price[$key];
$qty = $product_qty[$key];
$array_prod[$key] = array('name' => $name, 'price' => $price, 'qty' => $qty);

}
$array = array('customer_id' => $customer_id, 'user_id' => $user_id, 'order_id' => $id_order, 'date' => $date, 'items' => $array_prod);
// scrivi nella tabella saved_order_items e saved_carts
$resultInsertCart = $comiteg->writeSavedCartItems($array, $table_1, $table_2);
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Preventivi</title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>

  <div id="main_container">
  <div id="wrapper">
  <?php require_once('templates/orders_left.php') ?>
    <?php
    if (isset($_GET['order_id'])) {
    $customer_id = $comiteg->getTableValue('id_order,id_customer', $table_1, 'id_order', 'id_customer', $_GET['order_id']);
    $customerName = $comiteg->getTableValue('id,ragione_sociale', 'customers', 'id', 'ragione_sociale', $customer_id);
    ?>
    <div class="col-xs-12 breadcrumb_category">
      <a href="<?php echo $link_page ?>">
      <button class="btn btn-default button_back">
        <i class="fa fa-chevron-left" aria-hidden="true"></i>
      </button>
      </a>
      Preventivo # <?php echo $_GET['order_id'] ?> | <strong><?php echo $customerName ?></strong>
    </div>
    <?php
    include('cart/get_saved_items.php');
    }
    else {
    ?>
    <div class="col-xs-12 breadcrumb_category">
      <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
      </a>
      Preventivi
    </div>

        <?php
        //$_saved_carts_collection = $comiteg->getSavedTable($table_1, $table_2, $user_id);
        $_saved_carts_collection = $comiteg->getSavedResults($user_id, $_GET['str'], $table_1, $table_2);
        ?>


        <?php
        /*if (!isset($_GET['page'])) {
        $page = 1;
        }
        else {
        $page = $_GET['page'];
        }

        if (count($_saved_carts_collection) > 0) {
        $_saved_carts_collection = array_chunk($_saved_carts_collection, $page_size, true);
        $_saved_carts_collection = array_combine(range(1, count($_saved_carts_collection)), array_values($_saved_carts_collection));
        }*/
        ?>

        <?php //$comiteg->getCustomerPagination($_saved_carts_collection, '/estimates.php', $page) ?>


    <div class="col-md-9 scrolling_x_cart">
    <div class="fixed_width_700">
          <div class="col-md-1 col-xs-1 cart_label_tab" style="padding-left:0;text-align:right">
          PRE. #
          </div>
          <div class="col-md-4 col-xs-4">
          CLIENTE
          </div>
          <div class="col-md-2 col-xs-2" style="text-align:right">
          IMPORTO
          </div>
          <div class="col-md-3 col-xs-3" style="text-align:center">
          DATA
          </div>
          <div class="col-md-1 col-xs-1">
          &nbsp;
          </div>
          <div class="col-md-1 col-xs-1">
          &nbsp;
          </div>
          <div style="clear:both"></div>
          <hr class="hr_thin">

        <form method="POST" action="" class="saved_carts_form" id="saved_carts_form">

        <input type="hidden" name="table_1" value="<?php echo $table_1 ?>">
        <input type="hidden" name="table_2" value="<?php echo $table_2 ?>">
        <input type="hidden" name="is_page" id="is_page" value="<?php echo $link_page ?>">
        <input type="hidden" name="user_id" value="<?php echo $user_id ?>">

        <div id="savedCartsContent">
          <?php include('cart/list_saved_carts_orders.php') ?>
        </div>
        </form>
    </div>
    </div>
    <div class="col-md-3">
    CERCA PREVENTIVI
    <?php
    if (isset($_GET['str'])) {
    $valueCustomerSearch = $_GET['str'];
    }
    ?>
    <?php include('cart/search_input_saved.php') ?>
    <div style="color:green">
    <strong><?php echo $resultInsertCart ?></strong>
    </div>
    </div>

<?php
}
include('templates/ajax_print_saved.php');
?>

<span id="refresh_cart" style="display:none"></span>
<span id="refresh_saved_cart" style="display:none"></span>
  </div>
  </div>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>
<script type="text/javascript">
// quando finisce ajax update right block prezzi
$(function() {
    $(document).ajaxComplete(function(){
        $('#update_right_bar').trigger('click');
    });
});
</script>
</body>
</html>

