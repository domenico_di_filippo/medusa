<?php
// render del minicart.
session_start();
define('__ROOT__', dirname(dirname(__FILE__)));
require(__ROOT__.'/classes/class.main.php');
$comiteg = new Main;

$minicart_items = $comiteg->getMinicartItems($_SESSION['login']);
if (is_array($minicart_items)) {
$minicart_collection = $minicart_items;
}
else {
$minicart_collection = array();
}
?>
<form method="POST" action="" id="minicart_delete_items" class="minicart_delete_items">
<?php
foreach ($minicart_collection as $cart_item) {
$totalRowArray[] = $cart_item['cart']['price'] * $cart_item['cart']['qty'];
$redtitle = $cart_item['inventory']['qty'];
?>
  <div class="col-xs-12 minicart_item_modal">
    <div class="col-xs-1 minicart_item_image">
      <img style="width:100%" src="<?php echo $cart_item['media']['image'] ?>">
    </div>
    <div class="col-xs-11">
    <a class="link_no_decoration" href="/product.php?id=<?php echo $cart_item['cart']['id'] ?>">
      <strong <?php if ($redtitle <= 0) echo 'style="color:red"' ?>><?php echo $cart_item['cart']['id'].' | '.$cart_item['info']['name'] ?></strong>
      <div>
    </a>
        <?php
        $totalRow = $cart_item['cart']['price'] * $cart_item['cart']['qty'];
        echo '€ '.$cart_item['cart']['price'] ?> X <?php echo $cart_item['cart']['qty'] ?> <strong>TOT: € <?php echo number_format($totalRow, 2, '.', '') ?></strong>
      </div>
    </div>
    <div class="minicart_item_delete">
      <button type="submit" name="delete[]" class="invisible_btn minicart_delete_label" value="<?php echo $cart_item['cart']['id'] ?>">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
      </button>
    </div>
  </div>
<?php
}
?>
  <span id="refresh_minicart" style="display:none"></span>
</form>
<script type="text/javascript">
// invia il '#minicart_delete_items' per eliminare dal carrello
$(function () {
$('.invisible_btn').on('click', function () {

  var formData = $(this).closest('form').serializeArray();
  formData.push({ name: this.name, value: this.value });

  $('form.minicart_delete_items').on('submit', function (e) {
  e.preventDefault();

    $.ajax({
      type: 'post',
      url: '/cart/delete_minicart_items.php',
      data: formData,
      success: function (data) {
      $('#modal_response').trigger('click');
      $('#refresh_minicart').trigger('click');
      $('#update_minicart_response').trigger('click'); //footer.php
      },
      error: function () {
      alert("Problema con il carrello");
      }
    });
  });
});
});

// preleva info per render prodotti nel carrello
$(function () {
  $('#refresh_minicart').on('click', function () {
    $.ajax({
    url: '/cart/get_minicart_items.php',
    dataType: 'html',
    success: function(data) {
      var content = data;
      $('#modal_body_content').html(content);
    }
    });
  });
});
</script>

<?php
if (count($minicart_collection) == 0) {
?>
<div class="col-xs-12">
  Il tuo carrello è vuoto
</div>
<?php
}
else {
?>
<div class="col-xs-12 last_row_mini_cart">
  <strong class="total_mini_cart">TOTALE € <?php echo number_format(array_sum($totalRowArray), 2, '.', '') ?></strong>
</div>
<?php
}
?>