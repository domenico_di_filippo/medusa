<div id="test_arr"></div>
<?php
if (is_array($_items_collection)) {
$_items = $_items_collection;
}
else {
$_items = array();
}
if (count($_items) > 0) {

?>
      <div class="col-md-5 col-xs-5" style="padding-left:0">
        PRODOTTO
      </div>
      <div class="col-md-2 col-xs-2 cart_label_tab">
        PREZZO UNIT.
      </div>
      <div class="col-md-2 col-xs-2 cart_label_tab">
        QT&Agrave;
      </div>
      <div class="col-md-2 col-xs-2 cart_label_price_tab">
        SUBTOTALE
      </div>
      <div class="col-md-1 col-xs-1">
        &nbsp;
      </div>
      <div style="clear:both"></div>
      <hr class="hr_thin">

<?php

if ($link_page == '/orders.php') {
$isSent = $comiteg->getIfOrderIsSend($_GET['order_id'], $user_id);
  if ($isSent == $_GET['order_id']) {
  $styleNone = 'style="display:none"';
  $readonly = 'readonly';
  }
  else {
  $styleNone = $readonly = '';
  }
}

        foreach ($_items as $key => $_item) {

        ?>
        <div class="col-md-5 col-xs-5" style="padding-left:0">

          <div class="col-md-2 col-xs-2 product_image_container" style="padding:0;margin:0">
            <img style="width:100%" src="<?php echo $_item['media']['image'] ?>">
          </div>

          <div class="col-md-10 col-xs-10" style="padding-right:0">
            <input type="hidden" name="name[<?php echo $key ?>]" value="<?php echo str_replace('"', '\"', $_item['info']['name']) ?>">
            <a class="link_no_decoration" href="/product.php?id=<?php echo $key ?>">
            <strong <?php if ($_item['inventory']['qty'] <= 0) echo 'style="color:red"' ?>><?php echo $_item['info']['name'] ?></strong><br>
            </a>
            CODICE: <?php echo $key ?>
          </div>
        </div>
        <?php
        /*$isInCart = $comiteg->getProductCartPrice($_item['cart']['id'], $_SESSION['login']);
        if ($isInCart !== NULL) {
        $product_price = $isInCart;
        }
        else {*/
        $product_price = $_item['saved']['price'];
        //}

        $id_cust = $customer_id;

        $isReorderedArray = $comiteg->getIfReordered($key, $id_cust);
        if ($isReorderedArray[$key] == NULL) {

        $isReordered_class = '';
        $isReordered_div = '';
        }
        else {
        $reorderedPrice = number_format($isReorderedArray[$key]['last_price'], 2, '.', '');
        $isReordered_class = 'reordered';
        $isReordered_div = '<div><strong>R: € '.$reorderedPrice.'</strong></div>';
        }

        //echo $id_cust;
        ?>

        <div class="col-md-2 col-xs-2">

          <input type="hidden" name="image[<?php echo $key ?>]" value="<?php echo $_item['media']['image'] ?>">



          <input <?php if ($_item['prices']['cost'] > $product_price) echo 'style="color:red"' ?> <?php echo $readonly ?> onchange="addControlsCart(this); updateCartValues('.get_price')" onkeyup="addControlsCart(this); updateCartValues('.get_price')" id="price_<?php echo $key ?>" class="input_modal get_price <?php echo $isReordered_class ?>" type="number" name="price[<?php echo $key ?>]" min="0" step="0.01" value="<?php echo $product_price ?>">

          <div class="cost_info_cart" data-toggle="collapse" data-target="#prod_cost_<?php echo $key ?>"><i class="fa fa-info-circle" aria-hidden="true"></i></div>

            <div id="prod_cost_<?php echo $key ?>" class="collapse">
              <div>COSTO: € <?php echo $_item['prices']['cost'] ?></div>
              <?php echo $isReordered_div ?>
            </div>
          <input type="hidden" name="cost[<?php echo $key ?>]" id="cost_<?php echo $key ?>" value="<?php echo $_item['prices']['cost'] ?>">
        </div>
        <div class="col-md-2 col-xs-2">
          <?php
          if ($_item['inventory']['qty'] <= 0) {
          ?>
          <input <?php echo $readonly ?> style="color:red" onchange="updateCartValues('.get_qty')" onkeyup="updateCartValues('.get_qty')" class="input_modal get_qty" id="qty_<?php echo $key ?>" type="number" name="qty[<?php echo $key ?>]" min="<?php echo $_item['inventory']['min_qty'] ?>" step="<?php echo $_item['inventory']['inc_qty'] ?>" max="<?php echo $_item['inventory']['inc_qty'] * 100 ?>" value="<?php echo $_item['saved']['qty'] ?>">
          <?php
          }
          else {
          ?>
          <input <?php echo $readonly ?> onchange="updateCartValues('.get_qty')" onkeyup="updateCartValues('.get_qty')" class="input_modal get_qty" id="qty_<?php echo $key ?>" type="number" name="qty[<?php echo $key ?>]" min="<?php echo $_item['inventory']['min_qty'] ?>" step="<?php echo $_item['inventory']['inc_qty'] ?>" max="<?php echo $_item['inventory']['qty'] ?>" value="<?php echo $_item['saved']['qty'] ?>">
          <?php
          }
          ?>
          <input type="hidden" id="ctrl_qty_<?php echo $key ?>" name="ctrl_qty[]" value="<?php echo $_item['inventory']['qty'] ?>">

          <div>DISP: <?php echo $_item['inventory']['qty'] ?> pz</div>
          <div>CONF: <?php echo $_item['inventory']['inc_qty'] ?> pz</div>
        </div>
        <div class="col-md-2 col-xs-2">
          <div class="cart_label_price_tab" id="subtotal_row_<?php echo $key ?>">€ <?php echo number_format($_item['saved']['qty'] * $product_price, 2, '.', '') ?></div>
        </div>

        <div class="col-md-1 col-xs-1">

          <div class="minicart_item_delete" <?php echo $styleNone ?>>

            <?php
            if ($link_page == '/orders.php' && $key[0].$key[1] !== 'A-') {
            ?>
            <div>
              <label style="cursor:pointer" for="sendOD_<?php echo $key ?>">
              <span style="color:green;font-size:16px">OD:</span>
              </label>&nbsp;
              <input style="cursor:pointer" type="checkbox" id="sendOD_<?php echo $key ?>" checked name="send_od[<?php echo $key ?>]" value="OD">
            </div>
            <div style="clear:both"></div>
            <?php
            }
            ?>

            <button type="submit" name="delete[]" class="invisible_btn_item_list minicart_delete_label" value="<?php echo $key ?>">
              <i class="fa fa-times-circle" aria-hidden="true"></i>
            </button>

          </div>
        </div>
        <div style="clear:both"></div>
        <hr class="hr_thin">

        <?php
        $totalCost_arr[] = $_item['saved']['qty'] * $_item['prices']['cost'];
        $totalPrice_arr[] = $_item['saved']['qty'] * $product_price;
        }

        if (is_array($totalCost_arr) && is_array($totalPrice_arr)) {
        $totalCost = $totalCost_arr;
        $totalPrice = $totalPrice_arr;
        }
        else {
        $totalCost = array();
        $totalPrice = array();
        }

        ?>
<span id="refresh_cart" style="display:none"></span>

<?php
}
else {
?>
<div class="col-xs-12"><h2>Non ci sono prodotti</h2></div>
<script type="text/javascript">
// disabilita bottoni se vuoto
$("#salva_carrello").attr('disabled', 'disabled');
$("#rimetti_carrello").attr('disabled', 'disabled');
$("#chiudi_preventivo").attr('disabled', 'disabled');
$("#chiudi_ordine").attr('disabled', 'disabled');

$("#salva_carrello_savedcarts").attr('disabled', 'disabled');
$("#stampa_saved").attr('disabled', 'disabled');
$("#chiudi_preventivo_savedcarts").attr('disabled', 'disabled');
$("#rimetti_carrello_savedcarts").attr('disabled', 'disabled');
$("#chiudi_ordine_savedcarts").attr('disabled', 'disabled');
</script>
<?php
}
?>