<?php
session_start();
define('__ROOT__', dirname(dirname(__FILE__)));
require(__ROOT__.'/classes/class.main.php');
$comiteg = new Main;

// restituisce il numero dei prodotti nel carrello tramite richiesta ajax.
if (isset($_POST['qty']) && isset($_POST['price']) && isset($_POST['id_prod'])) {
$comiteg->addToCartList($_POST['user_name'], $_POST['id_prod'], $_POST['qty'], $_POST['price']);
}
$result = $comiteg->getCartItems($_SESSION['login']);
echo $result;
?>