<?php
if (is_array($_items_collection)) {
$_items = $_items_collection;
}
else {
$_items = array();
}
if (count($_items) > 0) {
//var_dump ($_items_collection);
?>
      <div class="col-md-5 col-xs-5" style="padding-left:0">
        PRODOTTO
      </div>
      <div class="col-md-2 col-xs-2 cart_label_tab">
        PREZZO UNIT.
      </div>
      <div class="col-md-2 col-xs-2 cart_label_tab">
        QT&Agrave;
      </div>
      <div class="col-md-2 col-xs-2 cart_label_price_tab">
        SUBTOTALE
      </div>
      <div class="col-md-1 col-xs-1">
        &nbsp;
      </div>
      <div style="clear:both"></div>
      <hr class="hr_thin">

<?php

if ($link_page == '/orders.php') {
$isSent = $comiteg->getIfOrderIsSend($_GET['order_id'], $user_id);
  if ($isSent == $_GET['order_id']) {
  $styleNone = 'style="display:none"';
  $readonly = 'readonly';
  }
  else {
  $styleNone = $readonly = '';
  }
}

        $a = 0;
        foreach ($_items as $_it) {
        $id = $_it['id_product'];
        $_item = $comiteg->getProduct($id);
        $key = $_item[$id]['info']['id'].'-0-'.$a++;

        $product_id = explode('-0-', $key)[0];

        //var_dump($_item[$id]['media']['image']);
        ?>
        <div class="col-md-5 col-xs-5" style="padding-left:0">

          <div class="col-md-2 col-xs-2 product_image_container" style="padding:0;margin:0">
            <img style="width:100%" src="<?php echo $_item[$id]['media']['image'] ?>" onerror="this.onerror=null;this.src='<?php echo $placeholder_img ?>';">
          </div>

          <div class="col-md-10 col-xs-10" style="padding-right:0">
            <input type="hidden" name="name[<?php echo $key ?>]" value="<?php echo str_replace('"', '\"', $_item[$id]['info']['name']) ?>">
            <a class="link_no_decoration" href="/product.php?id=<?php echo $product_id ?>">
            <strong><?php echo $_item[$id]['info']['name'] ?></strong><br>
            </a>
            CODICE: <?php echo $product_id ?> | ORDINE # <?php echo $_it['id_order'] ?>
          </div>
        </div>
        <?php
        /*$isInCart = $comiteg->getProductCartPrice($_item['cart']['id'], $_SESSION['login']);
        if ($isInCart !== NULL) {
        $product_price = $isInCart;
        }
        else {*/
        //$product_price = $_item['saved']['price'];
        //}



        //echo $id_cust;
        $product_price = number_format($_it['price'], 2, '.', '');
        ?>

        <div class="col-md-2 col-xs-2">
          <input type="hidden" name="id_order[<?php echo $key ?>]" value="<?php echo $_it['id_order'] ?>">
          <input type="hidden" name="image[<?php echo $key ?>]" value="<?php echo $_item[$id]['media']['image'] ?>">



          <input <?php if ($_item[$id]['prices']['cost'] > $product_price) echo 'style="color:red"' ?> onchange="addControlsCart(this); updateCartValues('.get_price')" onkeyup="addControlsCart(this); updateCartValues('.get_price')" id="price_<?php echo $key ?>" class="input_modal get_price" type="number" name="price[<?php echo $key ?>]" min="0" step="0.01" value="<?php echo $product_price ?>">

          <div class="cost_info_cart" data-toggle="collapse" data-target="#prod_cost_<?php echo $key ?>"><i class="fa fa-info-circle" aria-hidden="true"></i></div>

            <div id="prod_cost_<?php echo $key ?>" class="collapse">
              <div>COSTO: € <?php echo $_item[$id]['prices']['cost'] ?></div>
              <?php echo $isReordered_div ?>
            </div>
          <input type="hidden" name="cost[<?php echo $key ?>]" id="cost_<?php echo $key ?>" value="<?php echo $_item[$id]['prices']['cost'] ?>">
        </div>
        <div class="col-md-2 col-xs-2">

          <input onchange="updateCartValues('.get_qty')" onkeyup="updateCartValues('.get_qty')" class="input_modal get_qty" id="qty_<?php echo $key ?>" type="number" name="qty[<?php echo $key ?>]" min="0" step="<?php echo $_item[$id]['inventory']['inc_qty'] ?>" max="<?php echo $_it['qty'] ?>" value="<?php echo $_it['qty_to_ivoice'] ?>">


          <input type="hidden" id="ctrl_qty_<?php echo $key ?>" name="ctrl_qty[]" value="<?php echo $_item[$id]['inventory']['qty'] ?>">

          <div>DISP: <?php echo $_item[$id]['inventory']['qty'] ?> pz</div>
          <div>CONF: <?php echo $_item[$id]['inventory']['inc_qty'] ?> pz</div>
        </div>
        <div class="col-md-2 col-xs-2">
          <div class="cart_label_price_tab" id="subtotal_row_<?php echo $key ?>">€ <?php echo number_format($_it['qty_to_ivoice'] * $product_price, 2, '.', '') ?></div>
        </div>

        <div class="col-md-1 col-xs-1">
          <div class="minicart_item_delete">
            <button type="submit" name="delete[]" class="invisible_btn_to_invoice minicart_delete_label" value="<?php echo $key ?>">
              <i class="fa fa-trash" aria-hidden="true"></i>
            </button>
          </div>
        </div>
        <div style="clear:both"></div>
        <hr class="hr_thin">

        <?php
        $totalCost_arr[] = $_it['qty_to_ivoice'] * $_item[$id]['prices']['cost'];
        $totalPrice_arr[] = $_it['qty_to_ivoice'] * $product_price;
        }

        if (is_array($totalCost_arr) && is_array($totalPrice_arr)) {
        $totalCost = $totalCost_arr;
        $totalPrice = $totalPrice_arr;
        }
        else {
        $totalCost = array();
        $totalPrice = array();
        }

$tax_amount = $comiteg->getPriceTax(array_sum($totalPrice), $customer_tax) - array_sum($totalPrice);
$gain = array_sum($totalPrice) - array_sum($totalCost);
        ?>
<span id="refresh_cart" style="display:none"></span>

<?php
}
else {
?>
<div class="col-xs-12"><h2>Non ci sono prodotti</h2></div>
<script type="text/javascript">
// disabilita bottoni se vuoto
/*$("#salva_carrello").attr('disabled', 'disabled');
$("#rimetti_carrello").attr('disabled', 'disabled');
$("#chiudi_preventivo").attr('disabled', 'disabled');
$("#chiudi_ordine").attr('disabled', 'disabled');

$("#salva_carrello_savedcarts").attr('disabled', 'disabled');
$("#stampa_saved").attr('disabled', 'disabled');
$("#chiudi_preventivo_savedcarts").attr('disabled', 'disabled');
$("#rimetti_carrello_savedcarts").attr('disabled', 'disabled');
$("#chiudi_ordine_savedcarts").attr('disabled', 'disabled');*/
</script>
<?php
}
?>