<?php
session_start();
define('__ROOT__', dirname(dirname(__FILE__)));
require(__ROOT__.'/classes/class.main.php');
$comiteg = new Main;

$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);

// svuota carrello live esistente tramite richiesta ajax.
$comiteg->flushCart($user_id);

// aggiunge items salvati al carrello
foreach ($_POST['price'] as $key => $value) {
$comiteg->addSavedToCart($user_id, $key, $_POST['price'][$key], $_POST['qty'][$key], $_POST['order_id'], $_POST['table_2']);
}

// cancella carrello salvato quando rimetti nel carrello?
if ($_POST['table_1'] == 'saved_carts') {
$comiteg->deleteSavedCarts($_POST['table_1'], $_POST['table_2'], $user_id, $_POST['order_id']);
}
?>