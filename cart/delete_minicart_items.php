<?php
session_start();
define('__ROOT__', dirname(dirname(__FILE__)));
require(__ROOT__.'/classes/class.main.php');
$comiteg = new Main;

$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);
$product_price = $_POST['price'];
$product_qty = $_POST['qty'];

if (is_array($product_price)) {
$product_price = $product_price;
}
else {
$product_price = array();
}

foreach ($product_price as $key => $value) {
$array[$key] = array('price' => $product_price[$key], 'qty' => $product_qty[$key]);
}

// aggiorna edit carrello
$comiteg->updateChangedCartData($array, $user_id);

// rimuove dal carrello
$comiteg->deleteItemMinicart($_SESSION['login'], $_POST['delete'][0]);
?>