<?php
if (count($_saved_carts_collection) > 0 ) {

  /*echo '<pre>';
  print_r($_saved_carts_collection);
  echo '</pre>';*/

  /*echo '<pre>';
  print_r($_saved_carts_collection);
  echo '</pre>';*/

foreach ($_saved_carts_collection[$page] as $key => $order) {
$customerName = $comiteg->getTableValue('id,ragione_sociale', 'customers', 'id', 'ragione_sociale', $order['id_customer']);
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);

$prices = array();

foreach ($order['items'] as $_order_item) {
$prices[] = $_order_item['price'] * $_order_item['qty'];
}
$totalPrice = number_format(array_sum($prices), 2, '.', '');


if ($link_page == '/orders.php') {
$isSent = $comiteg->getIfOrderIsSend($key, $user_id);
$class = 'invisible_btn_orders';
}
else if ($link_page == '/saved_carts.php') {
$class = 'invisible_btn_savedcarts';
}
else if ($link_page == '/estimates.php') {
$isEstimateSent = $comiteg->getIfEstimateIsOrder($key, $user_id);
$class = 'invisible_btn_estimates';
}
?>
<div <?php if ($isSent == $key || $isEstimateSent == $key) echo 'class="orderSent"' ?> id="orderdiv_<?php echo $key ?>">
<div class="col-md-1 col-xs-1" style="padding-left:0;text-align:right">
<?php echo $key ?>
</div>
<div class="col-md-4 col-xs-4">
<!--a href="/new_customer.php?customer_id=<?php //echo $order['id_customer'] ?>" class="link_no_decoration"-->
<strong><?php echo $customerName ?></strong>
<!--/a-->
</div>
<div class="col-md-2 col-xs-2" style="text-align:right">
€ <?php echo $totalPrice ?>
</div>
<div class="col-md-3 col-xs-3" style="text-align:center">
<?php echo $order['date'] ?>
</div>
<div class="col-md-1 col-xs-1">
<a class="go_customer" href="<?php echo $link_page ?>?order_id=<?php echo $key ?>">
<button type="button" class="invisible_btn minicart_delete_label">
<i class="fa fa-pencil-square" aria-hidden="true"></i>
</button>
</a>
</div>
<div class="col-md-1 col-xs-1">
<div class="minicart_item_delete" <?php if ($isSent == $key) echo 'style="display:none"' ?>>
<button id="delete_<?php echo $key ?>" type="submit" name="delete[]" class="<?php echo $class ?> minicart_delete_label" value="<?php echo $key ?>">
<i class="fa fa-times-circle" aria-hidden="true"></i>
</button>
</div>
</div>
<div style="clear:both"></div>
</div>
<hr class="hr_thin">


<?php
/*---------------*/
if ($count == $page_size) {
break;
}
}
if (count($_saved_carts_collection) > $page_size ) {
  if ($q !== NULL) {
  $qu = '&str='.$q;
  }
?>
<div id="scrollAnchor"></div>
<a id="loadmore" class="btn btn-default" href="<?php echo $link_page.'?load='.$page_size.$qu ?>">
CARICA ALTRI
</a>
<?php
}
?>
<a class="btn btn-default" href="<?php echo $link_page ?>">
RESET
</a>

<?php
/*---------------*/


}
else {
?>
<div class="col-md-12" style="padding:0">
<h2>Nessuna corrispondenza trovata</h2>
</div>
<?php
}
?>