<?php
session_start();
define('__ROOT__', dirname(dirname(__FILE__)));
require(__ROOT__.'/classes/class.main.php');
$comiteg = new Main;

$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);

if (!isset($_POST['month_start'])) {
$month_start = 1;
}
else {
$month_start = intval($_POST['month_start']);
}
if (!isset($_POST['month_end'])) {
$month_end = 12;
}
else {
$month_end = intval($_POST['month_end']);
}
if (!isset($_POST['year_start'])) {
$year_start = intval(date('Y'));
}
else {
$year_start = intval($_POST['year_start']);
}
if (!isset($_POST['year_end'])) {
$year_end = intval(date('Y'));
}
else {
$year_end = intval($_POST['year_end']);
}


//$_saved_carts_collection = $comiteg->getSavedInvoicesSearch($user_id, $_POST['search_saved'], $_POST['table_1'], $_POST['table_2']);
$_saved_carts_collection = array();
$_saved_carts_invoices = $comiteg->getSavedInvoicesSearch($user_id, $_POST['search_saved'], $_POST['table_1'], $_POST['table_2'], $month_start, $month_end, $year_start, $year_end);
$_saved_carts_noinvoices = $comiteg->getSavedInvoicesSearch($user_id, $_POST['search_saved'], $_POST['table_3'], $_POST['table_4'], $month_start, $month_end, $year_start, $year_end);
if ($_saved_carts_noinvoices !== NULL && $_saved_carts_invoices !== NULL) {
$_saved_carts_collection = array_merge_recursive($_saved_carts_invoices, $_saved_carts_noinvoices);
}
else if ($_saved_carts_noinvoices == NULL && $_saved_carts_invoices !== NULL) {
$_saved_carts_collection = $_saved_carts_invoices;
}
else if ($_saved_carts_noinvoices !== NULL && $_saved_carts_invoices == NULL) {
$_saved_carts_collection = $_saved_carts_noinvoices;
}
//$_saved_carts_collection = array_merge_recursive($_saved_carts_invoices, $_saved_carts_noinvoices);

foreach ($_saved_carts_collection as $key => $row) {
$mid[$key]  = $row['sorting'];
}

if (count($_saved_carts_collection) > 1) {
array_multisort($mid, SORT_DESC, $_saved_carts_collection);
}


$link_page = $_POST['is_page'];
$table_1 = $_POST['table_1'];
$table_2 = $_POST['table_2'];
include('list_invoices.php');

?>