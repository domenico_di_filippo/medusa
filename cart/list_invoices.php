<?php
$array_stat = $comiteg->getInvoicesStatsByCollection($_saved_carts_collection);

$tot_price = array();
$tot_costs = array();
foreach ($array_stat as $stat) {
$tot_price[] = $stat['total_prices'];
$tot_costs[] = $stat['total_costs'];
}

$total_prices = number_format(array_sum($tot_price), 2, '.', '');
$total_costs = number_format(array_sum($tot_costs), 2, '.', '');
$total_gain = number_format(($total_prices - $total_costs), 2, '.', '');
$total_invoices = count($array_stat);

/*
echo '<pre>';
print_r($_saved_carts_collection);
echo '</pre>'; */

?>
<span id="total_span_invoices" style="display:none"><?php echo $total_invoices ?></span>
<span id="total_span_prices" style="display:none"><?php echo $total_prices ?></span>
<span id="total_span_costs" style="display:none"><?php echo $total_costs ?></span>
<span id="total_span_gain" style="display:none"><?php echo $total_gain ?></span>
<?php




if (count($_saved_carts_collection) > 0 ) {

foreach ($_saved_carts_collection as $key => $order) {

$key = $order['id'];

$customerName = $comiteg->getTableValue('id,ragione_sociale', 'customers', 'id', 'ragione_sociale', $order['id_customer']);
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);
$customer_tax = $comiteg->getTableValue('id,ali_iva', 'customers', 'id', 'ali_iva', $order['id_customer']);


$prices = array();

foreach ($order['items'] as $_order_item) {
$prices[] = $_order_item['price'] * $_order_item['qty'];
}
$totalPrice = number_format(array_sum($prices), 2, '.', '');


if ($order['flag'] == 'noinvoices') {
$classNoInvoice = 'flag_noinvoice';
}
else {
$classNoInvoice = '';
}

?>
<form method="POST" action="" class="invoices_form <?php echo $classNoInvoice ?>">

        <input type="hidden" name="table_1" value="<?php echo $table_1 ?>">
        <input type="hidden" name="table_2" value="<?php echo $table_2 ?>">
        <input type="hidden" name="fat_num" value="<?php echo $key ?>">
        <input type="hidden" name="id_customer" value="<?php echo $order['id_customer'] ?>">
        <input type="hidden" name="flag" value="<?php echo $order['flag'] ?>">
        <input type="hidden" name="customer_iva" value="<?php echo $customer_tax ?>">
        <input type="hidden" name="is_page" id="is_page" value="<?php echo $link_page ?>">
        <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
        <input type="hidden" name="date" value="<?php echo $order['dd'].'-'.$order['mm'].'-'.$order['yyyy'] ?>">

<?php
$a = 0;
foreach ($order['items'] as $_item) {
$a++;
?>
<input type="hidden" name="name[<?php echo $_item['id_product'].'-0-'.$a ?>]" value="<?php echo $_item['name'] ?>">
<input type="hidden" name="price[<?php echo $_item['id_product'].'-0-'.$a ?>]" value="<?php echo $_item['price'] ?>">
<input type="hidden" name="qty[<?php echo $_item['id_product'].'-0-'.$a ?>]" value="<?php echo $_item['qty'] ?>">
<?php
}
?>
<div class="col-md-1 col-xs-1" style="padding-left:0;text-align:right">
<?php
if ($order['flag'] == 'noinvoices') {
echo 'E_'.$key;
}
else {
echo $key;
}
?>
</div>
<div class="col-md-4 col-xs-4">
<!--a href="/new_customer.php?customer_id=<?php //echo $order['id_customer'] ?>" class="link_no_decoration"-->
<strong><?php echo $customerName ?></strong>
<!--/a-->
</div>
<div class="col-md-2 col-xs-2" style="text-align:right">
€ <?php echo $totalPrice ?>
</div>
<div class="col-md-3 col-xs-3" style="text-align:center">
<?php echo $order['dd'].'-'.$order['mm'].'-'.$order['yyyy'] ?>
</div>
<div class="col-md-1 col-xs-1">

<!--a class="go_customer" href="<?php echo $link_page ?>?order_id=<?php echo $key ?>"-->
<button type="button" data-toggle="modal" data-target="#invoice_<?php echo $key ?>" class="go_customer invisible_btn invisible_btn_invoice minicart_delete_label">
<i class="fa fa-print" aria-hidden="true"></i>
</button>
<!--/a-->
</div>
<div class="col-md-1 col-xs-1">
<button id="fastprint_<?php echo $key ?>" type="button" class="go_customer invisible_btn btn_stampa_invoice_fast minicart_delete_label">
<i class="fa fa-file" aria-hidden="true"></i>
</button>
</div>
<div style="clear:both"></div>
<hr class="hr_thin">


<?php
// risposta prodotto aggiunto al carrello.
?>
<div id="invoice_<?php echo $key ?>" class="modal fade modal_ok" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title cart_added_msg">Stampa o invia tramite email.</h4>
      </div>
      <div class="modal-body">

      <?php
      echo $comiteg->getCustomerEmail($order['id_customer'], $order['id_user']);
      ?>

      <input type="checkbox" name="check_my_mail" id="check_my_mail-<?php echo $key ?>" value="1">
      <label for="check_my_mail-<?php echo $key ?>">Invia una copia alla mia email.</label>


      <div id="get_invoice-<?php echo $key ?>" style="margin-top:15px;float:right"></div>
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-default close_invoice_creation" data-dismiss="modal">Chiudi</button>

        <button type="button" id="getId_<?php echo $key ?>" class="btn btn-default btn_stampa_invoice">Genera / Manda al Cliente</button>

      </div>
    </div>
  </div>
</div>





</form>

<?php
}
}
else {
?>
<div class="col-md-12" style="padding:0">
<h2>Non ci sono fatture</h2>
</div>
<?php
}
?>