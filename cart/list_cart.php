<?php
if (is_array($_items_collection)) {
$_items = $_items_collection;
}
else {
$_items = array();
}
if (count($_items) > 0) {
?>
      <div class="col-md-5 col-xs-5" style="padding-left:0">
        PRODOTTO
      </div>
      <div class="col-md-2 col-xs-2 cart_label_tab">
        PREZZO UNIT.
      </div>
      <div class="col-md-2 col-xs-2 cart_label_tab">
        QT&Agrave;
      </div>
      <div class="col-md-2 col-xs-2 cart_label_price_tab">
        SUBTOTALE
      </div>
      <div class="col-md-1 col-xs-1">
        &nbsp;
      </div>
      <div style="clear:both"></div>
      <hr class="hr_thin">

<?php

        foreach ($_items as $_item) {
        ?>
        <div class="col-md-5 col-xs-5" style="padding-left:0">

          <div class="col-md-2 col-xs-2 product_image_container" style="padding:0;margin:0">
            <img style="width:100%" src="<?php echo $_item['media']['image'] ?>" onerror="this.onerror=null;this.src='<?php echo $placeholder_img ?>';">
          </div>

          <div class="col-md-10 col-xs-10" style="padding-right:0">
            <input type="hidden" name="name[<?php echo $_item['cart']['id'] ?>]" value="<?php echo str_replace('"', '\"', $_item['info']['name']) ?>">
            <a class="link_no_decoration" href="/product.php?id=<?php echo $_item['cart']['id'] ?>">
            <strong <?php if ($_item['inventory']['qty'] <= 0) echo 'style="color:red"' ?>><?php echo $_item['info']['name'] ?></strong><br>
            </a>
            CODICE: <?php echo $_item['cart']['id'] ?>
          </div>
        </div>
        <?php
        $isInCart = $comiteg->getProductCartPrice($_item['cart']['id'], $_SESSION['login']);
        if ($isInCart !== NULL) {
        $product_price = $isInCart;
        }
        else {
        $product_price = $_item['prices']['price'];
        }

        if (!isset($_POST['customer_id'])) {
        $id_cust = $_GET['customer_id'];
        }

        $isReorderedArray = $comiteg->getIfReordered($_item['cart']['id'], $id_cust);
        if ($isReorderedArray[$_item['cart']['id']]['last_price'] == NULL) {

        $isReordered_class = '';
        $isReordered_div = '';
        }
        else {
        $reorderedPrice = number_format($isReorderedArray[$_item['cart']['id']]['last_price'], 2, '.', '');
        $isReordered_class = 'reordered';
        $isReordered_div = '<div><strong>R: € '.$reorderedPrice.'</strong></div>';
        }
        //var_dump($isReorderedArray);
        ?>
        <div class="col-md-2 col-xs-2">
          <input <?php if ($_item['prices']['cost'] > $product_price) echo 'style="color:red"' ?> onchange="addControlsCart(this); updateCartValues('.get_price')" onkeyup="addControlsCart(this); updateCartValues('.get_price')" id="price_<?php echo $_item['cart']['id'] ?>" class="input_modal get_price <?php echo $isReordered_class ?>" type="number" name="price[<?php echo $_item['cart']['id'] ?>]" min="0" step="0.01" value="<?php echo $product_price ?>">

          <div class="cost_info_cart" data-toggle="collapse" data-target="#prod_cost_<?php echo $_item['cart']['id'] ?>"><i class="fa fa-info-circle" aria-hidden="true"></i></div>

            <div id="prod_cost_<?php echo $_item['cart']['id'] ?>" class="collapse">
              <div>COSTO: € <?php echo  $_item['prices']['cost'] ?></div>
              <?php echo $isReordered_div ?>
            </div>
          <input type="hidden" name="cost[<?php echo $_item['cart']['id'] ?>]" id="cost_<?php echo $_item['cart']['id'] ?>" value="<?php echo $_item['prices']['cost'] ?>">
        </div>
        <div class="col-md-2 col-xs-2">
          <?php
          if ($_item['inventory']['qty'] <= 0) {
          ?>
          <input style="color:red" onchange="updateCartValues('.get_qty')" onkeyup="updateCartValues('.get_qty')" class="input_modal get_qty" id="qty_<?php echo $_item['cart']['id'] ?>" type="number" name="qty[<?php echo $_item['cart']['id'] ?>]" min="<?php echo $_item['inventory']['min_qty'] ?>" step="<?php echo $_item['inventory']['inc_qty'] ?>" max="<?php echo $_item['inventory']['inc_qty'] * 100 ?>" value="<?php echo $_item['cart']['qty'] ?>">
          <?php
          }
          else {
          ?>
          <input onchange="updateCartValues('.get_qty')" onkeyup="updateCartValues('.get_qty')" class="input_modal get_qty" id="qty_<?php echo $_item['cart']['id'] ?>" type="number" name="qty[<?php echo $_item['cart']['id'] ?>]" min="<?php echo $_item['inventory']['min_qty'] ?>" step="<?php echo $_item['inventory']['inc_qty'] ?>" max="<?php echo $_item['inventory']['qty'] ?>" value="<?php echo $_item['cart']['qty'] ?>">
          <?php
          }
          ?>
          <input type="hidden" id="ctrl_qty_<?php echo $_item['cart']['id'] ?>" name="ctrl_qty[]" value="<?php echo $_item['inventory']['qty'] ?>">

          <div>DISP: <?php echo $_item['inventory']['qty'] ?> pz</div>
          <div>CONF: <?php echo $_item['inventory']['inc_qty'] ?> pz</div>
        </div>
        <div class="col-md-2 col-xs-2">
          <div class="cart_label_price_tab" id="subtotal_row_<?php echo $_item['cart']['id'] ?>">€ <?php echo number_format($_item['cart']['qty'] * $product_price, 2, '.', '') ?></div>
        </div>
        <div class="col-md-1 col-xs-1">
          <div class="minicart_item_delete">
            <button type="submit" name="delete[]" class="invisible_btn minicart_delete_label" value="<?php echo $_item['cart']['id'] ?>">
              <i class="fa fa-times-circle" aria-hidden="true"></i>
            </button>
          </div>
        </div>
        <div style="clear:both"></div>
        <hr class="hr_thin">

        <?php
        $totalCost_arr[] = $_item['cart']['qty'] * $_item['prices']['cost'];
        $totalPrice_arr[] = $_item['cart']['qty'] * $product_price;
        }

        if (is_array($totalCost_arr) && is_array($totalPrice_arr)) {
        $totalCost = $totalCost_arr;
        $totalPrice = $totalPrice_arr;
        }
        else {
        $totalCost = array();
        $totalPrice = array();
        }

        ?>



<?php
}
else {
?>
<div class="col-xs-12"><h2>Il tuo carrello è vuoto</h2></div>
<script type="text/javascript">
$('#chiudi_ordine').attr('disabled', 'disabled');
$('#chiudi_preventivo').attr('disabled', 'disabled');
$('#flush_cart').attr('disabled', 'disabled');
$('#salva_carrello').attr('disabled', 'disabled');
$('#associato_a').css('display', 'none');
$('#search_customer_cart').css('display', 'none');
$('#rimuovi_associa').css('display', 'none');
</script>
<?php
}
?>