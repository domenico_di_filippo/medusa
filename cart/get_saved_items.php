<div class="col-md-9 scrolling_x_cart">
<div class="fixed_width_700">
<?php
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);
$customer_id = $comiteg->getTableValue('id_order,id_customer', $table_1, 'id_order', 'id_customer', $_GET['order_id']);
$customer_tax = $comiteg->getTableValue('id,ali_iva', 'customers', 'id', 'ali_iva', $customer_id);

$_items_collection = $comiteg->getSavedItems($_GET['order_id'], $table_2);
?>
<form method="POST" id="saved_cart_items" class="saved_cart_items" action="">


<input type="hidden" id="customer_id" name="customer_id" value="<?php echo $customer_id ?>">
<input type="hidden" id="order_id" name="order_id" value="<?php echo $_GET['order_id'] ?>">
<input type="hidden" id="customer_iva" name="customer_iva" value="<?php echo $customer_tax ?>">

<input type="hidden" name="table_1" value="<?php echo $table_1 ?>">
<input type="hidden" name="table_2" value="<?php echo $table_2 ?>">

<input type="hidden" name="address_send" id="address_send" value="">

<input type="hidden" name="email_send_to" id="email_send_to" value="">
<input type="hidden" name="email_send_me" id="email_send_me" value="">

        <div id="savedItemsContent">
          <?php include('cart/list_saved_items.php') ?>
        </div>
</form>
<?php
$tax_amount = $comiteg->getPriceTax(array_sum($totalPrice), $customer_tax) - array_sum($totalPrice);
$gain = array_sum($totalPrice) - array_sum($totalCost);
?>
</div>

</div>

<div class="col-md-3">
<?php
if ($link_page == '/orders.php') {
include('customers/get_customer_shipping_address.php');
}
?>
      <div class="col-md-12 totals_cart_block effect2">
        SUBTOTALE
        <div class="cart_block_price_container"><strong class="totals_cart_table" id="update_subtotal">€ <?php echo number_format(array_sum($totalPrice), 2, '.', '') ?></strong></div>
        <div class="cart_block_price_container_label">
          IVA
          <span id="update_ali_iva">
          <?php
          if ($customer_tax !== NULL) {
          echo '&nbsp;('.$customer_tax.'%)';
          }
          ?>
          </span>
        </div>
        <div class="cart_block_price_container"><strong class="totals_cart_table" id="update_tax" <?php if ($customer_tax == 0) echo 'style="text-decoration:line-through"' ?>>€ <?php echo number_format($tax_amount, 2, '.', '') ?></strong></div>
        <div class="cart_block_price_container_label">TOTALE</div>
        <div class="cart_block_price_container"><strong class="totals_cart_table_big" id="update_total">€ <?php echo $comiteg->getPriceTax(array_sum($totalPrice), $customer_tax) ?></strong></div>

        <button class="button_info_cart" data-toggle="collapse" data-target="#gain"><i class="fa fa-info-circle" aria-hidden="true"></i></button>

          <div id="gain" class="collapse">
            <div class="cart_block_price_container_label">
              RICAVO STIMATO
              <div class="cart_block_price_container"><strong class="totals_cart_table" id="update_gain">€ <?php echo number_format($gain, 2, '.', '') ?></strong></div>
            </div>
          </div>

      </div>

    <div style="color:green">
    <strong id="result_save_cart"><?php echo $resultInsertCart ?></strong>
    </div>

<?php
if ($link_page == '/saved_carts.php') { //carrelli salvati
?>
<button <?php if (!isset($_GET['order_id']) || count($_items_collection) == 0) echo 'disabled' ?> id="salva_carrello_savedcarts" class="button_action_cart_page btn btn-default">SALVA CARRELLO</button>

<button <?php if (!isset($_GET['order_id']) || count($_items_collection) == 0) echo 'disabled' ?> data-toggle="modal" data-target="#saved_print_pdf" class="button_action_cart_page btn btn-default" id="stampa_saved">STAMPA CARRELLO</button>

<button <?php if (!isset($_GET['order_id']) || count($_items_collection) == 0) echo 'disabled' ?> id="rimetti_carrello_savedcarts" class="button_action_cart_page btn btn-default">RIMETTI NEL CARRELLO</button>
<button <?php if (!isset($_GET['order_id']) || count($_items_collection) == 0) echo 'disabled' ?> id="chiudi_preventivo_savedcarts" class="button_action_cart_page btn btn-default">TRASFORMA IN PREVENTIVO</button>
<button <?php if (!isset($_GET['order_id']) || count($_items_collection) == 0) echo 'disabled' ?> id="chiudi_ordine_savedcarts" class="button_action_cart_page btn btn-default">TRASFORMA IN ORDINE</button>
<?php
}
else if ($link_page == '/estimates.php') { // preventivi
?>
<button <?php if (!isset($_GET['order_id']) || count($_items_collection) == 0) echo 'disabled' ?> id="salva_carrello_savedcarts" class="button_action_cart_page btn btn-default">SALVA PREVENTIVO</button>

<button <?php if (!isset($_GET['order_id']) || count($_items_collection) == 0) echo 'disabled' ?> data-toggle="modal" data-target="#saved_print_pdf" class="button_action_cart_page btn btn-default" id="stampa_saved">STAMPA PREVENTIVO</button>

<!--button <?php //if (!isset($_GET['order_id']) || count($_items_collection) == 0) echo 'disabled' ?> id="rimetti_carrello_savedcarts" class="button_action_cart_page btn btn-default">RIMETTI NEL CARRELLO</button-->
<button <?php if (!isset($_GET['order_id']) || count($_items_collection) == 0) echo 'disabled' ?> id="chiudi_ordine_savedcarts" class="button_action_cart_page btn btn-default">TRASFORMA IN ORDINE</button>
<?php
}
else if ($link_page == '/orders.php') { // ordini
$isSent = $comiteg->getIfOrderIsSend($_GET['order_id'], $user_id);
$haveAddress = $comiteg->getIfAddress($customer_id);
?>
<button <?php if (!isset($_GET['order_id']) || count($_items_collection) == 0 || $isSent == $_GET['order_id']) echo 'disabled' ?> id="salva_carrello_savedcarts" class="button_action_cart_page btn btn-default">SALVA ORDINE</button>

<button <?php if (!isset($_GET['order_id']) || count($_items_collection) == 0) echo 'disabled' ?> data-toggle="modal" data-target="#saved_print_pdf" class="button_action_cart_page btn btn-default" id="stampa_saved">STAMPA ORDINE</button>

<!--button <?php //if (!isset($_GET['order_id']) || count($_items_collection) == 0) echo 'disabled' ?> id="rimetti_carrello_savedcarts" class="button_action_cart_page btn btn-default">RIMETTI NEL CARRELLO</button-->
<button <?php if (!isset($_GET['order_id']) || count($_items_collection) == 0 || $isSent == $_GET['order_id'] || $haveAddress == 0) echo 'disabled' ?> id="invia_ordine" class="button_action_cart_page btn btn-default">INVIA ORDINE</button>
<?php
}
?>
</div>


<span onclick="updateCartValues('.get_price')" id="update_right_bar" class="update_right_bar" style="display:none"></span>
<?php require_once('templates/ajax_cart_message.php') // contiene il popup della risposta e il form per database ?>