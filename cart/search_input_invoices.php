<form method="POST" action="" class="form_search_invoices">
  <input type="hidden" name="table_1" value="<?php echo $table_1 ?>">
  <input type="hidden" name="table_2" value="<?php echo $table_2 ?>">
  <input type="hidden" name="table_3" value="<?php echo $table_3 ?>">
  <input type="hidden" name="table_4" value="<?php echo $table_4 ?>">
  <input type="hidden" name="is_page" value="<?php echo $link_page ?>">
  <input autocomplete="off" id="search_saved_invoices" class="input_modal left_input_search" type="text" name="search_saved" placeholder="Cerca Cliente" value="<?php echo $valueCustomerSearch ?>">

  <?php
  $min_range_y = 2017;
  $max_range_y = date('Y');
  $months = array('01','02','03','04','05','06','07','08','09','10','11','12');
  ?>
  <div class="col-xs-2 left_input_search" style="padding-left:0">
  DA:
  </div>

  <div class="col-xs-5 left_input_search" style="padding-left:0">
  <select name="month_start" class="input_modal filter_data" id="month_start">
  <option value="">MESE</option>
  <?php
  foreach ($months as $number) {
  ?>
  <option <?php if($month_start == $number) echo 'selected="selected"' ?> value="<?php echo $number ?>"><?php echo $number ?></option>
  <?php
  }
  ?>
  </select>
  </div>
  <div class="col-xs-5 left_input_search" style="padding-right:0">
  <select name="year_start" class="input_modal filter_data" id="year_start">
  <option value="">ANNO</option>
  <?php
  foreach (array_reverse(range($min_range_y, $max_range_y)) as $number) {
  ?>
  <option <?php if($year_start == $number) echo 'selected="selected"' ?> value="<?php echo $number ?>"><?php echo $number ?></option>
  <?php
  }
  ?>
  </select>
  </div>

  <div class="col-xs-2 left_input_search" style="padding-left:0">
  A:
  </div>

  <div class="col-xs-5 left_input_search" style="padding-left:0">
  <select name="month_end" class="input_modal filter_data" id="month_end">
  <option value="">MESE</option>
  <?php
  foreach ($months as $number) {
  ?>
  <option <?php if($month_end == $number) echo 'selected="selected"' ?> value="<?php echo $number ?>"><?php echo $number ?></option>
  <?php
  }
  ?>
  </select>
  </div>

  <div class="col-xs-5 left_input_search" style="padding-right:0">
  <select name="year_end" class="input_modal filter_data" id="year_end">
  <option value="">ANNO</option>
  <?php
  foreach (array_reverse(range($min_range_y, $max_range_y)) as $number) {
  ?>
  <option <?php if($year_end == $number) echo 'selected="selected"' ?> value="<?php echo $number ?>"><?php echo $number ?></option>
  <?php
  }
  ?>
  </select>
  </div>

  <button class="btn btn-default" style="float:right;margin-left:10px;" type="submit">CERCA</button>

  <a style="float:right" href="\invoices.php">
  <button class="btn btn-default" type="button">RESET</button>
  </a>
  <div id="get_customer"></div>
</form>
