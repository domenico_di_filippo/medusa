<?php
// contenitore del minicart.
?>
<div id="minicart" class="modal fade modal_cart" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Il tuo carrello</h4>
      </div>
      <div class="modal-body" id="modal_body_content"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
        <a href="/cart.php">
          <button id="go_to_cart" class="btn btn-default">Vai al carrello</button>
        </a>
      </div>
    </div>
  </div>
</div>
