<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once('../config/config.php');

// classe per update delle quantità sui prodotti importati da od, ogni due ore.
class Update {

  // connessione database.
  function connectDB() {
    $config = new Config;
    $db_user = Config::$db_user;
    $db_pass = Config::$db_pass;
    $db_name = Config::$db_name;
    $db_host = Config::$db_host;
    $conn = new mysqli($db_host, $db_user, $db_pass, $db_name);
    return $conn;
  }

  // scarica tracciato delle disponibilità e fa update sui prodotti.
  function downloadUpdateOD() {
    $config = new Config;

    $od_track_host = Config::$od_track_host;
    $od_track_user = Config::$od_track_user;
    $od_track_password = Config::$od_track_password;
    $od_track_file_upd = Config::$od_track_file_upd;
    $od_track_path_upd = Config::$od_track_path_upd;


    $ftp_conn = ftp_connect($od_track_host);
    $login_result = ftp_login($ftp_conn, $od_track_user, $od_track_password);

    ftp_pasv($ftp_conn, true);

    $local_file = 'zip/'.$od_track_file_upd;
    $server_file = $od_track_path_upd.'/'.$od_track_file_upd;

    $results = ftp_get($ftp_conn, $local_file, $server_file, FTP_BINARY);

    ftp_close($ftp_conn);

    $zip = new ZipArchive;
    $zip->open($local_file);
    $zip->extractTo('zip');
    $zip->close();

    $filepath = 'zip/'.str_replace('zip', 'xml', $od_track_file_upd);
    $xml = simplexml_load_file($filepath);

    $a = 0;

      foreach($xml as $articolo) {
      $sku = isset($articolo->codiceOD) ? (string) $articolo->codiceOD : "";
      $qty = isset($articolo->giacenzaDisponibile) ? (string) $articolo->giacenzaDisponibile : "";

      $conn = $this->connectDB();

      $query_sku = "SELECT * FROM product_inventory WHERE id = '$sku'";
      $result_sku = mysqli_query($conn, $query_sku);

        while ($row = mysqli_fetch_array($result_sku)) {
        $qty_table = $row['qty'];
        }

        $existSku = $conn->query("SELECT id FROM product_inventory WHERE id = '$sku'");

        if ($qty_table !== $qty && $existSku->num_rows > 0) {
        $a++;
        $conn->query("UPDATE product_inventory SET qty = '$qty' WHERE id = '$sku'");
        }
      mysqli_close($conn);
      }

    echo '<p>Qty aggiornata per '.$a.' prodotti.</p>';
  unlink($filepath);
  }

}
?>