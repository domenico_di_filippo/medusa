<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}

if (isset($_GET['c']) || isset($_GET['cat']) && isset($_GET['c'])) {
$idCat = $_GET['c'];

$_category = $comiteg->getCategoriesXML($idCat);

$cat_names = array_filter(array($_GET['m'], $_GET['c'], $_GET['cat']));
}
else {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
$lastId = end(array_filter($cat_names));
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Sfoglia Categoria <?php echo $comiteg->getCategoryName($lastId) ?></title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>

  <div id="main_container">
    <div id="wrapper">
      <?php require_once('templates/category_left.php') ?>
      <div class="col-xs-12 breadcrumb_category">

        <a class="btn btn-default button_back" href="/shop.php#anchor_<?php echo $_GET['m'] ?>">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </a>
        <?php
        foreach ($cat_names as $categoryIds) {
        $array_convert[] = $comiteg->getCategoryName($categoryIds);
        }
        echo implode('&nbsp;  <i class="fa fa-angle-right" aria-hidden="true"></i>  &nbsp;', $array_convert);
        ?>
      </div>

      <div class="col-xs-12" id="container_product_xs">
        <?php
        if (!isset($_GET['cat'])) {
        $id_cat_open = $arrayCat[0];
        }
        else {
        $id_cat_open = $_GET['cat'];
        }

        if (!isset($_GET['page'])) {
        $pageNumber = 1;
        }
        else {
        $pageNumber = $_GET['page'];
        }
        if (!isset($_GET['order_by']) && !isset($_GET['sort'])) {
        $order_by = 'price';
        $sort = 'ASC';
        }
        else if (isset($_GET['order_by']) && !isset($_GET['sort'])) {
        $order_by = $_GET['order_by'];
        $sort = 'ASC';
        }
        else if (!isset($_GET['order_by']) && isset($_GET['sort'])) {
        $order_by = 'price';
        $sort = $_GET['sort'];
        }
        else if (isset($_GET['order_by']) && isset($_GET['sort'])) {
        $order_by = $_GET['order_by'];
        $sort = $_GET['sort'];
        }
        $_poductCollection = $comiteg->getProductsByCategory($id_cat_open, $pageNumber, $order_by, $sort);
        include('templates/toolbar_category.php');

        if (count($_poductCollection) > 0) {
          foreach ($_poductCollection as $_product) {
          include('templates/list.php');
          }
        }
        else {
        ?>
        <div class="col-xs-12" style="margin-bottom:25px;">
          <h1>Nessun prodotto trovato</h1>
        </div>
        <?php
        }
        include('templates/toolbar_category.php');
        ?>
      </div>
    </div>
  </div>
<?php require_once('templates/ajax_cart_message.php') // contiene il popup della risposta e il form per database ?>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>

</body>
</html>