<?php
$_product = $_product[0];

/*echo '<pre>';
print_r($_product);
echo '</pre>';*/

$categories = $comiteg->getCategoriesTree();
$new_id = $_product['info']['id'];

$_product_category = explode('/', $_product['info']['categories']);

if (isset($_POST['submit'])) {

$tmpFilePath = $_FILES['image']['tmp_name'];
  if ($tmpFilePath != '') {
  $renameImg = $new_id.'.jpg';
  $newFilePath = 'media/'.$renameImg;
    if(move_uploaded_file($tmpFilePath, $newFilePath)) {
    $image = $newFilePath;
    }
    else {
    $image = $_product['media']['image'];
    }
  }
  else {
  $image = $_product['media']['image'];
  }

$id = $new_id;
$name = strtoupper(str_replace("'", "\'", $_POST['name']));
$description = strtoupper(str_replace("'", "\'", $_POST['description']));
$brand = strtoupper(str_replace("'", "\'", $_POST['brand']));
$ean = strtoupper(str_replace("'", "\'", $_POST['ean']));
$mpn = strtoupper(str_replace("'", "\'", $_POST['mpn']));
$weight = str_replace("'", "\'", $_POST['weight']);
$alt_1 = strtoupper(str_replace("'", "\'", $_POST['alt_1']));
$alt_2 = strtoupper(str_replace("'", "\'", $_POST['alt_2']));
$categories = implode('/', $_POST['cat']);
$image = $image;
$qty = str_replace("'", "\'", $_POST['qty']);
$min_qty = str_replace("'", "\'", $_POST['min_qty']);
$inc_qty = str_replace("'", "\'", $_POST['inc_qty']);
$cost = str_replace("'", "\'", $_POST['cost']);
$price = str_replace("'", "\'", $_POST['price']);

$array = array('id' => $id,
               'name' => $name,
               'description' => $description,
               'brand' => $brand,
               'ean' => $ean,
               'mpn' => $mpn,
               'weight' => $weight,
               'alt_1' => $alt_1,
               'alt_2' => $alt_2,
               'categories' => $categories,
               'image' => $image,
               'qty' => $qty,
               'min_qty' => $min_qty,
               'inc_qty' => $inc_qty,
               'cost' => $cost,
               'price' => $price);

/*echo '<pre>';
print_r($array);
echo '</pre>';*/
$comiteg->updateProduct($array);

  if ($name !== '') {
  ?>
  <script type="text/javascript">
  var id_page = "<?php echo $_GET['id'] ?>";
  window.location.href= '/admin.php?product=update&id='+id_page;
  </script>
  <?php
  }
  else  if ($name == '') {
  ?>
  <script type="text/javascript">
  window.location.href= '/admin.php?product=update';
  </script>
  <?php
  }
}
?>
<form method="POST" enctype="multipart/form-data">
  <div class="col-xs-12 col-md-6">
    <label>NOME PRODOTTO</label>&nbsp;
    <span style="color:red;cursor:pointer" data-toggle="collapse" data-target="#info_trash"><i class="fa fa-trash" aria-hidden="true"></i></span>
    <div id="info_trash" class="collapse" style="color:red;margin-bottom:10px;">
    Lascia vuoto il nome del prodotto per cancellarlo definitivamente.
    </div>
    <input class="input_modal" name="name" value="<?php echo $_product['info']['name'] ?>">

    <label class="new_prod_form">DESCRIZIONE</label>
    <textarea class="input_modal" name="description" rows="5"><?php echo $_product['info']['description'] ?></textarea>

    <div class="col-md-6" style="padding-left:0">
    <label class="new_prod_form">BRAND</label>
    <input class="input_modal" name="brand" value="<?php echo $_product['attributes']['brand'] ?>">

    <label class="new_prod_form">PART NUMBER</label>
    <input class="input_modal" name="mpn" value="<?php echo $_product['attributes']['mpn'] ?>">

    <label class="new_prod_form">COCDICE ALTERNATIVO #1</label>
    <input class="input_modal" name="alt_1" value="<?php echo $_product['attributes']['cod_alt_1'] ?>">
    </div>

    <div class="col-md-6" style="padding-right:0">
    <label class="new_prod_form">BARCODE</label>
    <input class="input_modal" name="ean" value="<?php echo $_product['attributes']['ean'] ?>">

    <label class="new_prod_form">PESO Kg</label>
    <input type="number" class="input_modal" name="weight" min="0.0000" step="0.0001" value="<?php echo $_product['attributes']['weight'] ?>">

    <label class="new_prod_form">COCDICE ALTERNATIVO #2</label>
    <input class="input_modal" name="alt_2" value="<?php echo $_product['attributes']['cod_alt_2'] ?>">
    </div>
  </div>

  <div class="col-xs-12 col-md-6">
    <label>ID: </label>&nbsp;<?php echo $new_id ?>
    <label class="input_modal">CATEGORIE</label>
    <select id="cat1" level="1" class="input_modal" name="cat[]">
    <option value="">---> Seleziona</option>
    <?php
    foreach ($categories as $first) {
    ?>
    <option <?php if ($_product_category[0] == $first['first']['id']) echo 'selected="selected"' ?> value="<?php echo $first['first']['id'] ?>"><?php echo $first['first']['name'] ?></option>
    <?php
    }
    ?>
    </select>


    <div id="cat2_cont">
      <?php
      $subcategories_2 = $comiteg->getSubcategories($_product_category[0], 1);
      ?>
      <select class="input_modal" name="cat[]" id="cat2" level="2" style="margin-top:15px">
        <option value="">---> Seleziona</option>
      <?php
        foreach ($subcategories_2 as $key => $value) {
        ?>
        <option <?php if ($_product_category[1] == $key) echo 'selected="selected"' ?> value="<?php echo $key?>"><?php echo $value['name'] ?></option>
        <?php
        }
      ?>
      </select>
  <script type="text/javascript">
  $(document).ready(function () {
  $('#cat2').on('change', function() {
     var id = $(this).val();
     var level = $(this).attr('level');
  	 var formData = {
  	 'id': $(this).val(),
  	 'level' : level
  	 }
  	 console.log($(this).val());
      $.ajax({
        type: 'POST',
        url: '/templates/ajax_get_subcategories.php',
        data: formData,
        success : function(data) {
          if (id == '') {
          $('#cat3_cont').html('');
  				}
  				else {
          $('#cat3_cont').html(data);
  				}
  			//$('#cat3_cont').html('');
        }
      });
  });
  });
  </script>
    </div>

    <div id="cat3_cont">
      <?php
      $subcategories_3 = $comiteg->getSubcategories($_product_category[1], 2);
      ?>
      <select class="input_modal" name="cat[]" id="cat3" level="3" style="margin-top:15px">
      <option value="">---> Seleziona</option>
      <?php
        foreach ($subcategories_3 as $key => $value) {
        ?>
        <option <?php if ($_product_category[2] == $key) echo 'selected="selected"' ?> value="<?php echo $key?>"><?php echo $value['name'] ?></option>
        <?php
        }
      ?>
      </select>
    </div>

    <div class="col-ms-12 col-md-6" style="padding-left:0">
    <label class="new_prod_form">IMMAGINE</label>
    <div style="width:150px">
      <img style="width:100%" src="/<?php echo $_product['media']['image'] ?>" onerror="this.onerror=null;this.src='<?php echo $placeholder_img ?>';">
    </div>
    </div>

    <div class="col-ms-12 col-md-6" style="padding-right:0">
    <label class="new_prod_form">CAMBIA IMMAGINE</label>
    <input type="file" class="input_modal" name="image" accept="image/x-png, image/jpeg">
    </div>

    <div style="clear:both"></div>

    <div class="col-md-4" style="padding:0">
      <label class="new_prod_form">QUANTIT&Agrave; DISP.</label>
      <input type="number" class="input_modal" name="qty" min="1" step="1" value="<?php echo $_product['inventory']['qty'] ?>">
    </div>
    <div class="col-md-4">
      <label class="new_prod_form">QUANTIT&Agrave; MIN.</label>
      <input type="number" class="input_modal" name="min_qty" min="1" step="1" value="<?php echo $_product['inventory']['min_qty'] ?>">
    </div>
    <div class="col-md-4" style="padding:0">
      <label class="new_prod_form">QUANTIT&Agrave; INCR.</label>
      <input type="number" class="input_modal" name="inc_qty" min="1" step="1" value="<?php echo $_product['inventory']['inc_qty'] ?>">
    </div>

    <div class="col-md-6" style="padding-left:0">
      <label class="new_prod_form">COSTO (IVA ESCLUSA)</label>
      <input type="number" class="input_modal" name="cost" min="0" step="0.01" value="<?php echo $_product['prices']['cost'] ?>">
    </div>

    <div class="col-md-6" style="padding-right:0">
      <label class="new_prod_form">PREZZO (IVA ESCLUSA)</label>
      <input type="number" class="input_modal" name="price" min="0" step="0.01" value="<?php echo $_product['prices']['price'] ?>">
    </div>

    <input type="hidden" name="id" value="<?php echo $new_id ?>">

  <div class="col-md-12" style="margin-top:25px;padding:0">
  <button class="btn btn-default" style="float:right;" type="submit" name="submit">SALVA</button>
  </div>
  </div>


  <div style="clear:both"></div>
</div>