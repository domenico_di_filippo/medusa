<?php
// finestra aggiungi al carrello.
?>
<div id="modal_<?php echo $_product['info']['id']?>" class="modal fade modal_cart" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Aggiungi al carrello</h4>
      </div>
      <div class="col-xs-12">
        <div class="col-xs-2 image_cart_popup">
          <img style="width:100%" src="<?php echo $_product['media']['image'] ?>">
        </div>
        <div class="col-xs-10 name_cart_popup">
          <strong><?php echo $_product['info']['id'] ?> | <?php echo $_product['info']['name'] ?></strong>
        </div>
      </div>

      <form method="POST" action="" class="form_cart">
        <div class="modal-body">
        <input type="hidden" name="user_name" id="user_<?php echo $_product['info']['id'] ?>" value="<?php echo $_SESSION['login'] ?>">
        <input type="hidden" name="id_prod" id="id_<?php echo $_product['info']['id'] ?>" value="<?php echo $_product['info']['id'] ?>">
          <div class="col-xs-4">
            <label>Quantità</label>
            <?php
            if ($_product['inventory']['qty'] <= 0) {
            ?>
            <input onchange="addControls(this)" onkeyup="addControls(this)" class="input_modal" type="number" id="qty_<?php echo $_product['info']['id'] ?>" name="qty" value="<?php echo $_product['inventory']['min_qty'] ?>" min="<?php echo $_product['inventory']['min_qty'] ?>" step="<?php echo $_product['inventory']['inc_qty'] ?>" max="<?php echo $_product['inventory']['inc_qty'] * 100 ?>">
            <?php
            $alert_qty = '<div class="col-xs-12 alert_qty">ATTENZIONE: Questo prodotto non è attualmente disponibile!</div>';
            }
            else {
            ?>
            <input onchange="addControls(this)" onkeyup="addControls(this)" class="input_modal" type="number" id="qty_<?php echo $_product['info']['id'] ?>" name="qty" value="<?php echo $_product['inventory']['min_qty'] ?>" min="<?php echo $_product['inventory']['min_qty'] ?>" step="<?php echo $_product['inventory']['inc_qty'] ?>" max="<?php echo $_product['inventory']['qty'] ?>">
            <?php
            $alert_qty = '';
            }
            ?>
          </div>
          <div class="col-xs-4">
            <label style="float:left;">Prezzo</label>
            <div style="width:20px;height:20px;float:right;color:#FFF;background:#444;border-radius:100%;text-align:center;cursor:pointer" data-toggle="collapse" data-target="#percent_price_<?php echo $_product['info']['id'] ?>">
            %
            </div>
            <div style="clear:both"></div>
            <div id="percent_price_<?php echo $_product['info']['id'] ?>" class="collapse" style="position:relative;margin-bottom:5px;">
            <input id="ch_percent_price_<?php echo $_product['info']['id'] ?>" onchange="changeProductPricePercent('<?php echo $_product['info']['id'] ?>')" onkeyup="changeProductPricePercent('<?php echo $_product['info']['id'] ?>')" step="0.01" class="input_modal" type="number">
            <div style="position:absolute;top:2px;right:25px">%</div>
            </div>

            <input onchange="changeProductPrice(this)" onkeyup="changeProductPrice(this)" class="input_modal" type="number" id="price_<?php echo $_product['info']['id'] ?>" name="price" value="<?php echo $product_price ?>" step="0.01" min="0.00">
            <input type="hidden" id="hidden_price_<?php echo $_product['info']['id'] ?>" value="<?php echo $product_price ?>">
          </div>

          <div class="col-xs-4">
            <label class="info_cost_cart" id="labelinfocost_<?php echo $_product['info']['id'] ?>" data-toggle="collapse" data-target="#infocost_<?php echo $_product['info']['id'] ?>">+ info</label>
            <div class="collapse" id="infocost_<?php echo $_product['info']['id'] ?>">
              <input class="input_modal" id="cost_<?php echo $_product['info']['id'] ?>" readonly="readonly" type="number" name="cost" value="<?php echo $_product['prices']['cost'] ?>">
            </div>
          </div>


          <?php echo $alert_qty ?>
          <div class="col-xs-12 alert_qty" id="cart_alert_keuyup_<?php echo $_product['info']['id'] ?>"></div>
          <div class="col-xs-12 message_cart" id="message_cart_<?php echo $_product['info']['id'] ?>"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
          <button onclick="addToCart(this)" id="btn_<?php echo $_product['info']['id'] ?>" type="submit" class="btn btn-default">Aggiungi al carrello</button>
        </div>
      </form>
    </div>
  </div>
</div>