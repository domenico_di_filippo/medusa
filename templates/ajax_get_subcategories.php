<?php
session_start();
require_once ('../classes/class.main.php');
$comiteg = new Main;

$categories = $comiteg->getSubcategories($_POST['id'], $_POST['level']);

  if ($_POST['level'] == 1) {
  ?>
  <select class="input_modal" name="cat[]" id="cat2" level="2" style="margin-top:15px">
    <option value="">---> Seleziona</option>
  <?php
    foreach ($categories as $key => $value) {
    ?>
    <option value="<?php echo $key?>"><?php echo $value['name'] ?></option>
    <?php
    }
  ?>
  </select>
  <script type="text/javascript">
  $(document).ready(function () {
  $('#cat2').on('change', function() {
     var id = $(this).val();
     var level = $(this).attr('level');
  	 var formData = {
  	 'id': $(this).val(),
  	 'level' : level
  	 }
  	 console.log($(this).val());
      $.ajax({
        type: 'POST',
        url: '/templates/ajax_get_subcategories.php',
        data: formData,
        success : function(data) {
          if (id == '') {
          $('#cat3_cont').html('');
  				}
  				else {
          $('#cat3_cont').html(data);
  				}
  			//$('#cat3_cont').html('');
        }
      });
  });
  });
  </script>
  <?php
  }

  else if ($_POST['level'] == 2) {
  ?>
  <select class="input_modal" name="cat[]" id="cat3" level="3" style="margin-top:15px">
    <option value="">---> Seleziona</option>
  <?php
    foreach ($categories as $key => $value) {
    ?>
    <option value="<?php echo $key?>"><?php echo $value['name'] ?></option>
    <?php
    }
  ?>
  </select>
  <?php
  }
?>