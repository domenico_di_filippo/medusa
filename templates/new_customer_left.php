<?php
// navbar sinistra per navigazione risultati ricerca.
?>

<div role="navigation" onclick="showHideLeftMobile()">
  <span class="navbar-brand-category">
    <a id="menu-toggle" href="#" class="btn-menu toggle">
      <i class="fa fa-bars" aria-hidden="true"></i><span style="padding:0px 15px"></span>
    </a>
  </span>
</div>
<div id="sidebar-wrapper">
  <nav id="spy">


    <div id="anagrafica_left" class="col-xs-12 category_item_sx <?php if ($active_page == '/new_customer.php') echo 'active' ?>">
      <a class="a_category" href="/new_customer.php?customer_id=<?php echo $_GET['customer_id'] ?>">
        <div class="category_tab">
          ANAGRAFICA
        </div>
      </a>
    </div>

    <!--div id="anagrafica_left" onclick="anchorCustomer(this)" class="col-xs-12 category_item_sx">
      <a class="a_category" href="#anagrafica">
        <div class="category_tab">
          ANAGRAFICA
        </div>
      </a>
    </div-->

    <!--div id="carrelli_left" onclick="anchorCustomer(this)" class="col-xs-12 category_item_sx">
      <a class="a_category" href="#carrelli">
        <div class="category_tab">
          CARRELLI SALVATI (<?php echo $comiteg->getSavedCustomerSize($user_id, 'saved_carts', $_GET['customer_id']) ?>)
        </div>
      </a>
    </div-->

    <!--div id="estimates_left" onclick="anchorCustomer(this)" class="col-xs-12 category_item_sx">
      <a class="a_category" href="#estimates">
        <div class="category_tab">
          PREVENTIVI (<?php echo $comiteg->getSavedCustomerSize($user_id, 'saved_estimates', $_GET['customer_id']) ?>)
        </div>
      </a>
    </div-->

    <div id="orders_left" class="col-xs-12 category_item_sx <?php if ($active_page == '/customer_estimates.php') echo 'active' ?>">
      <a class="a_category" href="/customer_estimates.php?customer_id=<?php echo $_GET['customer_id'] ?>">
        <div class="category_tab">
          PREVENTIVI (<?php echo $comiteg->getSavedCustomerSize($user_id, 'saved_estimates', $_GET['customer_id']) ?>)
        </div>
      </a>
    </div>

    <!--div id="orders_left" onclick="anchorCustomer(this)" class="col-xs-12 category_item_sx">
      <a class="a_category" href="#orders">
        <div class="category_tab">
          ORDINI (<?php echo $comiteg->getSavedCustomerSize($user_id, 'saved_orders', $_GET['customer_id']) ?>)
        </div>
      </a>
    </div-->

    <div id="orders_left" class="col-xs-12 category_item_sx <?php if ($active_page == '/customer_orders.php') echo 'active' ?>">
      <a class="a_category" href="/customer_orders.php?customer_id=<?php echo $_GET['customer_id'] ?>">
        <div class="category_tab">
          ORDINI (<?php echo $comiteg->getSavedCustomerSize($user_id, 'saved_orders', $_GET['customer_id']) ?>)
        </div>
      </a>
    </div>

    <!--div id="products_left" onclick="anchorCustomer(this)" class="col-xs-12 category_item_sx">
      <a class="a_category" href="#products">
        <div class="category_tab">
        <?php
        $_poductBuy = $comiteg->getProductsOrderedByCustomer($_GET['customer_id'], NULL, NULL);
        ?>
          PRODOTTI ACQUISTATI (<?php echo count($_poductBuy) ?>)
        </div>
      </a>
    </div-->

    <div id="orders_left" class="col-xs-12 category_item_sx <?php if ($active_page == '/customer_buy.php') echo 'active' ?>">
      <a class="a_category" href="/customer_buy.php?customer_id=<?php echo $_GET['customer_id'] ?>">
        <div class="category_tab">
          <?php
          $_poductBuy = $comiteg->getProductsOrderedByCustomer($_GET['customer_id'], NULL, NULL);
          ?>
          PRODOTTI ACQUISTATI (<?php echo count($_poductBuy) ?>)
        </div>
      </a>
    </div>


    <!--div id="invoiceproducts_left" onclick="anchorCustomer(this)" class="col-xs-12 category_item_sx">
      <a class="a_category" href="#invoiceproducts">
        <div class="category_tab">
        <?php
        $_item_invoices = $comiteg->getItemsForInvoice($_GET['customer_id'], $user_id);
        $countProducts = $comiteg->countItemsNewCustomerLeft($_GET['customer_id'], $user_id, 'send_orders', 'send_order_items');
        ?>
          PRODOTTI DA FATTURARE (<?php echo $countProducts ?>)
        </div>
      </a>
    </div-->

    <div id="orders_left" class="col-xs-12 category_item_sx <?php if ($active_page == '/customer_to_invoice.php') echo 'active' ?>">
      <a class="a_category" href="/customer_to_invoice.php?customer_id=<?php echo $_GET['customer_id'] ?>">
        <div class="category_tab">
          <?php
          $_item_invoices = $comiteg->getItemsForInvoice($_GET['customer_id'], $user_id);
          $countProducts = $comiteg->countItemsNewCustomerLeft($_GET['customer_id'], $user_id, 'send_orders', 'send_order_items');
          ?>
          PRODOTTI DA FATTURARE (<?php echo $countProducts ?>)
        </div>
      </a>
    </div>


  </nav>
</div>
