<?php
//session_start();
define('__ROOT__', dirname(dirname(__FILE__)));
require(__ROOT__.'/classes/class.main.php');
$comiteg = new Main;

require('../fpdf/fpdf.php');

define('EURO',chr(128));


foreach ($_POST['price'] as $key => $value) {
  if ($_POST['qty'][$key] !== '0') {
  $ctrlArray[] = 'ctrl';
  }
}

if (count($ctrlArray) > 0) {
$numFattura = $comiteg->getNewInvoiceId($_POST['invoice_id'], $_POST['customer_id'], $_POST['user_id']);

$customer_info = $comiteg->getCustomer($_POST['customer_id']);

$data = 'Del '.$numFattura['date'];

$indirizzo = $customer_info['indirizzo'].' - '.$customer_info['cap'].' '.$customer_info['citta'].' ('.$customer_info['prov'].')';

$arrayIva = array('P.IVA: ' => $customer_info['p_iva'], 'C.F.: ' => $customer_info['cod_fisc']);
$filterIva = array_filter($arrayIva);
  if (is_array($filterIva)) {
  $ivafilt = $filterIva;
  }
  else {
  $ivafilt = array();
  }
  foreach ($ivafilt as $key => $value) {
  $array_cfiva[] = $key.$value;
  }

  if (is_array($array_cfiva)) {
  $arr = $array_cfiva;
  }
  else {
  $arr = array();
  }

$pivacf = implode(' - ', $arr);

$titolo = 'FATTURA # '.$numFattura['id'];
$custome_message = 'In allegato la fattura # '.$numFattura['id'].'.';
$name_of_file = 'fattura_'.$numFattura['date'].'_'.$numFattura['id'].'.pdf';
$save_file = 'fattura.pdf';



$pdf = new FPDF();
$pdf->AddPage();

// prendi intestazione
include('pdf_head.php');
$pdf->MultiCell(1, 12, '');

$pdf->SetFont('Arial','B',12);
$pdf->Cell(70,7,$titolo,0,0);
$pdf->SetFont('Arial','',10);
$pdf->Cell(120,7,'FATTURA A:',0,0);
$pdf->MultiCell(1, 7, ''); // clear:both

$pdf->Cell(70,7,$data,0,0);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(120,7,$customer_info['ragione_sociale'],0,0);
$pdf->MultiCell(1, 7, ''); // clear:both

$pdf->SetFont('Arial','',10);
$pdf->Cell(70,7,'',0,0);
$pdf->Cell(120,7,$indirizzo,0,0);
$pdf->MultiCell(1, 7, ''); // clear:both


$pdf->Cell(70,7,'',0,0);
$pdf->Cell(120,7,$pivacf,0,0);
$pdf->MultiCell(1, 10, ''); // clear:both

$pdf->Cell(115,7,'PRODOTTO',0,0);
$pdf->Cell(25,7,'P. UNI.',0,0,'R');
$pdf->Cell(25,7,'QTA\'',0,0,'R');
$pdf->Cell(25,7,'SUBTOTALE',0,0,'R');
$pdf->MultiCell(1, 7, ''); // clear:both
$pdf->MultiCell(190, 1, '', 'B', 'L', 0); //border-bottom
$pdf->MultiCell(190, 1, '', 'B', 'L', 0); //border-bottom

foreach ($_POST['price'] as $key => $value) {

if ($_POST['qty'][$key] !== '0') {

$codice_prodotto = explode('-0-', $key)[0];

$comiteg->invoiceItems($_POST['id_order'][$key], $_POST['customer_id'], $codice_prodotto, $numFattura['id'], $_POST['price'][$key], $_POST['qty'][$key]);

$pdf->SetFont('Arial','B',10);
$pdf->MultiCell(115,5,$_POST['name'][$key],0,'L');

$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->SetXY($x, $y);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(115,5,'CODICE: '.$codice_prodotto,0,'L');

$pdf->SetFont('Arial','',10);

$pdf->SetXY($x + 115, $y);
$pdf->MultiCell(25,5,EURO.' '.$_POST['price'][$key],0,'R');

$pdf->SetXY($x + 140, $y);
$pdf->MultiCell(25,5,$_POST['qty'][$key],0,'R');

$pdf->SetXY($x + 165, $y);
$subtot = number_format($_POST['qty'][$key] * $_POST['price'][$key], 2, '.', '');
$array_sum[] = $_POST['qty'][$key] * $_POST['price'][$key];
$pdf->MultiCell(25,5,EURO.' '.$subtot,0,'R');


$pdf->MultiCell(190, 1, '', 'B', 'L', 0); //border-bottom
}
}

$subtotal = number_format(array_sum($array_sum),2 ,'.', '');

$pdf->MultiCell(190, 1, '', 'B', 'L', 0); //border-bottom
$pdf->MultiCell(1, 5, ''); // clear:both

$pdf->Cell(115,7,'',0,0);
$pdf->Cell(15,7,'',0,0,'R');
$pdf->Cell(25,7,'SUBTOTALE',0,0,'R');

$pdf->SetFont('Arial','B',15);
$pdf->Cell(35,7,EURO.' '.$subtotal,0,0,'R');
$pdf->MultiCell(1, 7, ''); // clear:both

$pdf->SetFont('Arial','',10);
$pdf->Cell(115,7,'',0,0);
$pdf->Cell(15,7,'',0,0,'R');
$pdf->Cell(25,7,'IVA ('.$_POST['customer_iva'].'%)',0,0,'R');
$total = number_format(($subtotal * (1 + $_POST['customer_iva'] / 100)),2 ,'.', '');
$iva = number_format(($total - $subtotal),2 ,'.', '');
$pdf->SetFont('Arial','B',15);
$pdf->Cell(35,7,EURO.' '.$iva,0,0,'R');
$pdf->MultiCell(1, 7, ''); // clear:both

$pdf->SetFont('Arial','',10);
$pdf->Cell(115,7,'',0,0);
$pdf->Cell(15,7,'',0,0,'R');
$pdf->Cell(25,7,'TOTALE',0,0,'R');

$pdf->SetFont('Arial','B',15);
$pdf->Cell(35,7,EURO.' '.$total,0,0,'R');

//

$path = '/home/medusaufficio/public_html/fatture/'.$save_file;
$pdf->Output($path,'F');
$open_path = '/fatture/'.$save_file;


//invia email
$to = $_POST['email_send_to_invoice'];
$email_sito = 'info@medusaufficio.it';
$subject = $titolo;
$from = 'Medusaufficio <'.$email_sito.'>';
$boundary = md5(uniqid(time()));


$headers = "From: $from\r\n";
$headers .= "Reply-To: $email_sito\r\n";
$headers .= "X-Mailer: PHP/" . phpversion();
$headers .= "MIME-Version: 1.0\r\n"
  ."Content-Type: multipart/mixed; boundary=\"$boundary\"";

$message .= 'If you can see this MIME than your client doesn\'t accept MIME types!'."\r\n";
$message .= '--'.$boundary."\r\n";
$message .= 'Content-Type: text/html; charset="iso-8859-1"'."\r\n";
$message .= 'Content-Transfer-Encoding: 7bit'."\r\n\r\n";
// inizio messaggio
$message .= '<html><head></head><body>';
$message .= '<p>Gentile <b>'.$customer_info['ragione_sociale'].'</b>.</p>';
$message .= '<p>'.$custome_message.'</p>'."\r\n";
$message .= '<p>Grazie.</p>'."\r\n";
$message .= '</body></html>'."\r\n";
// fine messaggio
$message .= '--'.$boundary."\r\n";

$file = file_get_contents($path);
$message .= "Content-Type: application/pdf; name=\"$name_of_file\"\r\n"
  ."Content-Transfer-Encoding: base64\r\n"
  ."Content-disposition: attachment; file=\"$name_of_file\"\r\n"
  ."\r\n"
  .chunk_split(base64_encode($file))
  ."--$boundary--";

if ($_POST['email_send_to_invoice'] !== '') {
mail($to, $subject, $message, $headers);
}

if ($_POST['email_send_me_invoice'] == '1') {
mail($email_sito, $subject, $message, $headers);
}

?>
<a href="<?php echo $open_path ?>" download>
<button type="button" id="remove_invoice_creation" class="btn btn-default">SCARICA</button>
</a>

<?php
}
?>
