<?php
$categories = $comiteg->getCategoriesTree();
$new_id = 'A-'.date('YmdHis');

if (isset($_POST['submit'])) {



$tmpFilePath = $_FILES['image']['tmp_name'];

  if ($tmpFilePath != '') {
  $renameImg = $new_id.'.jpg';
  $newFilePath = 'media/'.$renameImg;
    if(move_uploaded_file($tmpFilePath, $newFilePath)) {
    $image = $newFilePath;
    }
    else {
    $image = NULL;
    }
  }

$id = $new_id;
$name = strtoupper(str_replace("'", "\'", $_POST['name']));
$description = strtoupper(str_replace("'", "\'", $_POST['description']));
$brand = strtoupper(str_replace("'", "\'", $_POST['brand']));
$ean = strtoupper(str_replace("'", "\'", $_POST['ean']));
$mpn = strtoupper(str_replace("'", "\'", $_POST['mpn']));
$weight = str_replace("'", "\'", $_POST['weight']);
$alt_1 = strtoupper(str_replace("'", "\'", $_POST['alt_1']));
$alt_2 = strtoupper(str_replace("'", "\'", $_POST['alt_2']));
$categories = implode('/', $_POST['cat']);
$image = $image;
$qty = str_replace("'", "\'", $_POST['qty']);
$min_qty = str_replace("'", "\'", $_POST['min_qty']);
$inc_qty = str_replace("'", "\'", $_POST['inc_qty']);
$cost = str_replace("'", "\'", $_POST['cost']);
$price = str_replace("'", "\'", $_POST['price']);

$array = array('id' => $id,
               'name' => $name,
               'description' => $description,
               'brand' => $brand,
               'ean' => $ean,
               'mpn' => $mpn,
               'weight' => $weight,
               'alt_1' => $alt_1,
               'alt_2' => $alt_2,
               'categories' => $categories,
               'image' => $image,
               'qty' => $qty,
               'min_qty' => $min_qty,
               'inc_qty' => $inc_qty,
               'cost' => $cost,
               'price' => $price);

/*echo '<pre>';
print_r($array);
echo '</pre>';*/
$comiteg->addNewProduct($array);

}
?>
<form method="POST" enctype="multipart/form-data">
  <div class="col-xs-12 col-md-6">
    <label>NOME PRODOTTO</label>
    <input class="input_modal" name="name">

    <label class="new_prod_form">DESCRIZIONE</label>
    <textarea class="input_modal" name="description" rows="5"></textarea>

    <div class="col-md-6" style="padding-left:0">
    <label class="new_prod_form">BRAND</label>
    <input class="input_modal" name="brand">

    <label class="new_prod_form">PART NUMBER</label>
    <input class="input_modal" name="mpn">

    <label class="new_prod_form">COCDICE ALTERNATIVO #1</label>
    <input class="input_modal" name="alt_1">
    </div>

    <div class="col-md-6" style="padding-right:0">
    <label class="new_prod_form">BARCODE</label>
    <input class="input_modal" name="ean">

    <label class="new_prod_form">PESO Kg</label>
    <input type="number" class="input_modal" name="weight" min="0.0000" step="0.0001">

    <label class="new_prod_form">COCDICE ALTERNATIVO #2</label>
    <input class="input_modal" name="alt_2">
    </div>
  </div>

  <div class="col-xs-12 col-md-6">
    <label>ID: </label>&nbsp;<?php echo $new_id ?>
    <label class="input_modal">CATEGORIE</label>
    <select id="cat1" level="1" class="input_modal" name="cat[]">
    <option value="">---> Seleziona</option>
    <?php
    foreach ($categories as $first) {
    ?>
    <option value="<?php echo $first['first']['id'] ?>"><?php echo $first['first']['name'] ?></option>
    <?php
    }
    ?>
    </select>
    <div id="cat2_cont"></div>
    <div id="cat3_cont"></div>

    <label class="new_prod_form">IMMAGINE</label>
    <input type="file" class="input_modal" name="image" accept="image/x-png, image/jpeg">

    <div class="col-md-4" style="padding:0">
      <label class="new_prod_form">QUANTIT&Agrave; DISP.</label>
      <input type="number" class="input_modal" name="qty" min="1" step="1" value="1">
    </div>
    <div class="col-md-4">
      <label class="new_prod_form">QUANTIT&Agrave; MIN.</label>
      <input type="number" class="input_modal" name="min_qty" min="1" step="1" value="1">
    </div>
    <div class="col-md-4" style="padding:0">
      <label class="new_prod_form">QUANTIT&Agrave; INCR.</label>
      <input type="number" class="input_modal" name="inc_qty" min="1" step="1" value="1">
    </div>

    <div class="col-md-6" style="padding-left:0">
      <label class="new_prod_form">COSTO (IVA ESCLUSA)</label>
      <input type="number" class="input_modal" name="cost" min="0" step="0.01">
    </div>

    <div class="col-md-6" style="padding-right:0">
      <label class="new_prod_form">PREZZO (IVA ESCLUSA)</label>
      <input type="number" class="input_modal" name="price" min="0" step="0.01">
    </div>

    <input type="hidden" name="id" value="<?php echo $new_id ?>">

  <div class="col-md-12" style="margin-top:25px;padding:0">
  <button class="btn btn-default" style="float:right;" type="submit" name="submit">SALVA</button>
  </div>
  </div>


  <div style="clear:both"></div>
</div>