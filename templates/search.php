<?php
// barra ricerca top.
?>
<div class="col-xs-12 top_search">
  <div class="search_field">
    <form method="GET" action="/search.php">
      <input onkeyup="getSearchLen(this)" id="input_search" class="input_search" type="text" name="q" value="<?php echo $_GET['q'] ?>" placeholder="Ricerca Articoli">
      <button id="search_btn_top" class="button_search" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
    </form>
  </div>
  <div class="logged_as_top">
    <i class="fa fa-user" aria-hidden="true"></i>&nbsp;<?php echo $_SESSION['login'] ?>
  </div>
</div>
<script type="text/javascript">
function getSearchLen(field) {
  var strlen = $(field).val().length;
  if (strlen < 2) {
  $('#search_btn_top').attr('type', 'button');
  }
  else {
  $('#search_btn_top').attr('type', 'submit');
  }
}
var fieldSize = $('#input_search').val();

  if (fieldSize == null) {
  $('#search_btn_top').attr('type', 'button');
  }
  else {
  $('#search_btn_top').attr('type', 'submit');
  }
</script>