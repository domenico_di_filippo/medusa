<?php
// navbar sinistra per navigazione ordini.
?>
<div role="navigation" onclick="showHideLeftMobile()">
  <span class="navbar-brand-category">
    <a id="menu-toggle" href="#" class="btn-menu toggle">
      <i class="fa fa-bars" aria-hidden="true"></i><span style="padding:0px 15px"></span>
    </a>
  </span>
</div>
<div id="sidebar-wrapper">
  <nav id="spy">

    <!--div class="col-xs-12 category_item_sx <?php // if ($link_page == '/saved_carts.php') echo 'active' ?>">
      <a class="a_category" href="/saved_carts.php">
        <div class="category_tab">
          CARRELLI SALVATI (<?php // echo $comiteg->getSavedSize($user_id, 'saved_carts') ?>)
        </div>
      </a>
    </div-->

    <div class="col-xs-12 category_item_sx <?php if ($link_page == '/admin.php?product=update') echo 'active' ?>">
      <a class="a_category" href="/admin.php?product=update">
        <div class="category_tab">
          MODIFICA PRODOTTI
        </div>
      </a>
    </div>

    <div class="col-xs-12 category_item_sx <?php if ($link_page == '/admin.php?product=new') echo 'active' ?>">
      <a class="a_category" href="/admin.php?product=new">
        <div class="category_tab">
          AGGIUNGI PRODOTTO
        </div>
      </a>
    </div>

    <div class="col-xs-12 category_item_sx <?php if ($link_page == '/admin.php?cat=g') echo 'active' ?>">
      <a class="a_category" href="/admin.php?cat=g">
        <div class="category_tab">
          GESTISCI CATEGORIE
        </div>
      </a>
    </div>

  </nav>
</div>