<?php
// costruisce la list view dei prodotti in categorie e ricerca.
if (is_array($product_collection)) {
$product_collection = $product_collection;
}
else {
$product_collection = array();
}

if (count($product_collection) > 0) {

foreach ($product_collection as $_product) {

if ($_product['inventory']['qty'] <= 0) {
$classQty = 'qtyred';
}
else if ($_product['inventory']['qty'] <= 10) {
$classQty = 'qtyorange';
}
else {
$classQty = 'qtynormal';
}
if ($_product['prices']['variation'] == '+') {
$priceIcon = '<span class="pluscircle"><i class="fa fa-plus-circle" aria-hidden="true"></i></span>';
}
else if ($_product['prices']['variation'] == '-') {
$priceIcon = '<span class="minuscircle"><i class="fa fa-minus-circle" aria-hidden="true"></i></span>';
}
else {
$priceIcon = '';
}
?>
<div class="col-xs-12 col-md-6 product_list_container" style="font-size:13px">
  <div class="col-xs-12 container_product_orders">
    <div class="col-xs-12 col-md-1 product_list_container_image">
      <img class="product_list_image" src="<?php echo $_product['media']['image'] ?>" onerror="this.onerror=null;this.src='<?php echo $placeholder_img ?>';">
    </div>
    <div class="col-xs-12 col-md-9">
      <div class="product_list_title_ordered">
        <?php

        if (strlen($_product['info']['name']) >= 70) {
        $_product_name = substr($_product['info']['name'], 0, 67).'...';
        }
        else {
        $_product_name = $_product['info']['name'];
        }
        ?>
        <?php echo $_product['info']['id'].' | <a href="/product.php?id='.$_product['info']['id'].'">'.$_product_name.'</a>' ?>
      </div>

      <div class="col-xs-12 col-md-5" style="padding-left:0px;">
        Conf: <span><strong><?php echo $_product['inventory']['min_qty'] ?> pz</strong></span> -
        Disp: <span class="<?php echo $classQty ?>"><strong><?php echo $_product['inventory']['qty'] ?> pz</strong></span>
        <?php
        if ($_product['inventory']['reorder'] !== '0') {
        ?>
        &nbsp;(+<span><strong><?php echo $_product['inventory']['reorder'] ?></strong></span>)
        <?php
        }
        ?>
      </div>

      </div>
      <div class="add_to_cart_links col-md-2 col-xs-12">
        <a href="/admin.php?product=update&id=<?php echo $_product['info']['id'] ?>">
          <span class="go_to_product">
            <i class="fa fa-external-link-square" aria-hidden="true"></i>
          </span>
        </a>

    </div>
  </div>
</div>
<?php
}
}
else {
?>
<div class="col-xs-12">
<h2>Non ci sono prodotti</h2>
</div>
<?php
}