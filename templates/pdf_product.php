<?php
//session_start();
define('__ROOT__', dirname(dirname(__FILE__)));
require(__ROOT__.'/classes/class.main.php');
$comiteg = new Main;

require('../fpdf/fpdf.php');

define('EURO',chr(128));

$pdf = new FPDF();
$pdf->AddPage();

// prendi intestazione
include('pdf_head.php');

$pdf->SetFont('Arial','B',15);

$pdf->MultiCell(1, 12, '');

$pdf->Cell(55,55,'',0,0);
$coordY = $pdf->getY() - 10;

if ($_POST['product_id'][1] == '-') {
$r_path = '..';
}

$img_exists = $comiteg->imgExistFtp($r_path.$_POST['product_image']);

if ($img_exists == true) {
$pdf->Image($r_path.$_POST['product_image'],13,$coordY,45);
}
else {
$pdf->Image('..'.$placeholder_img,13,$coordY,45);
}

$pdf->MultiCell(135, 8, str_replace('"', '-', $_POST['product_name']), 0, 'L');

$pdf->MultiCell(190, 20, '', 'B', 'L', 0); //border-bottom
$pdf->MultiCell(1, 1, '');
//prezzo
$pdf->SetFont('Arial','',12);
$pdf->Cell(55,7,'Prezzo:',0,0);
$pdf->SetFont('Arial','B',15);
$price = EURO.' '.$_POST['product_price'].' /pz';
$pdf->Cell(135,7,$price,0,0);
$pdf->MultiCell(1, 7, '');

$pdf->SetFont('Arial','',12);

//codice prodotto
$pdf->Cell(55,7,'Codice Prodotto:',0,0);
$pdf->Cell(135,7,$_POST['product_id'],0,0);
$pdf->MultiCell(1, 7, '');

//cod_alt_1
if ($_POST['product_cod_1'] !== '') {
$pdf->Cell(55,7,'Codice Alt. 1:',0,0);
$pdf->Cell(135,7,$_POST['product_cod_1'],0,0);
$pdf->MultiCell(1, 7, '');
}

//cod_alt_2
if ($_POST['product_cod_2'] !== '') {
$pdf->Cell(55,7,'Codice Alt. 2:',0,0);
$pdf->Cell(135,7,$_POST['product_cod_2'],0,0);
$pdf->MultiCell(1, 7, '');
}

//ean
if ($_POST['product_ean'] !== '') {
$pdf->Cell(55,7,'Codice Ean:',0,0);
$pdf->Cell(135,7,$_POST['product_ean'],0,0);
$pdf->MultiCell(1, 7, '');
}

//mpn
if ($_POST['product_mpn'] !== '') {
$pdf->Cell(55,7,'Codice Mpn:',0,0);
$pdf->Cell(135,7,$_POST['product_mpn'],0,0);
$pdf->MultiCell(1, 7, '');
}

//peso
if ($_POST['product_weight'] !== '') {
$pdf->Cell(55,7,'Peso:',0,0);
$peso = $_POST['product_weight'].' Kg';
$pdf->Cell(135,7,$peso,0,0);
$pdf->MultiCell(1, 7, '');
}

//brand
if ($_POST['product_brand'] !== '') {
$pdf->Cell(55,7,'Brand:',0,0);
$pdf->Cell(135,7,$_POST['product_brand'],0,0);
$pdf->MultiCell(1, 7, '');
}

$pdf->MultiCell(190, 1, '', 'B', 'L', 0); //border-bottom
$pdf->MultiCell(1, 1, '');

//description
if ($_POST['product_description'] !== '') {
$pdf->Cell(55,7,'Descrizione:',0,0);
$pdf->MultiCell(1, 7, '');
$pdf->MultiCell(190, 6, str_replace('<br>', ' ', $_POST['product_description']), 0, 'L');
}

$path = $pdf_path.'scheda_prodotto.pdf';
$pdf->Output($path,'F');
$open_path = '/download/scheda_prodotto.pdf';

//invia email
$to = $_POST['email'];
$email_sito = $manda_mail_generica;
$filename = 'scheda_prodotto_'.$_POST['product_id'].'.pdf';
$subject = 'Scheda Prodotto COD: '.$_POST['product_id'];
$from = 'Medusaufficio <'.$email_sito.'>';
$boundary = md5(uniqid(time()));


$headers = "From: $from\r\n";
$headers .= "Reply-To: $email_sito\r\n";
$headers .= "X-Mailer: PHP/" . phpversion();
$headers .= "MIME-Version: 1.0\r\n"
  ."Content-Type: multipart/mixed; boundary=\"$boundary\"";

$message .= 'If you can see this MIME than your client doesn\'t accept MIME types!'."\r\n";
$message .= '--'.$boundary."\r\n";
$message .= 'Content-Type: text/html; charset="iso-8859-1"'."\r\n";
$message .= 'Content-Transfer-Encoding: 7bit'."\r\n\r\n";
// inizio messaggio
$message .= '<html><head></head><body>';
$message .= '<p>Gentile Cliente,</p>';
$message .= '<p>In allegato la scheda prodotto <b>"'.$_POST['product_name'].'"</b></p>';
$message .= '<p>Grazie.</p>'."\r\n";
$message .= '</body></html>'."\r\n";
// fine messaggio
$message .= '--'.$boundary."\r\n";

/*
$headers = "From: $from\r\n";
$headers .= "Reply-To: $email_sito\r\n";
$headers .= "MIME-Version: 1.0\r\n"
  ."Content-Type: multipart/mixed; boundary=\"1a2a3a\"";
$message .= 'If you can see this MIME than your client doesn\'t accept MIME types!'."\r\n";
$message .= '--1a2a3a'."\r\n";
$message .= 'Content-Type: text/html; charset="iso-8859-1"'."\r\n";
$message .= 'Content-Transfer-Encoding: 7bit'."\r\n\r\n";
// inizio messaggio
$message .= '<p>In allegato la scheda prodotto <b>"'.$_POST['product_name'].'"</b></p>';
$message .= '<p>Grazie.</p>'."\r\n";
// fine messaggio
$message .= '--1a2a3a'."\r\n"; */

$file = file_get_contents($path);
$message .= "Content-Type: application/pdf; name=\"$filename\"\r\n"
  ."Content-Transfer-Encoding: base64\r\n"
  ."Content-disposition: attachment; file=\"$filename\"\r\n"
  ."\r\n"
  .chunk_split(base64_encode($file))
  ."--$boundary--";

if ($_POST['email'] !== '') {
mail($to, $subject, $message, $headers);
}

if ($_POST['inviami_copia'] == '1') {
mail($email_sito, $subject, $message, $headers);
}



?>
<a href="<?php echo $open_path ?>" download>
<button type="button" class="btn btn-default">SCARICA SCHEDA PRODOTTO</button>
</a>
