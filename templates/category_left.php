<?php
// navbar sinistra per navigazione categorie.
?>
<div role="navigation"  onclick="showHideLeftMobile()">
  <span class="navbar-brand-category">
    <a id="menu-toggle" href="#" class="btn-menu toggle">
      <i class="fa fa-bars" aria-hidden="true"></i><span style="padding:0px 15px">CATEGORIE</span>
    </a>
  </span>
</div>
<div id="sidebar-wrapper">
  <nav id="spy">
    <?php
    if ($_category !== NULL) {
    foreach ($_category as $key => $categoryName) {
    $arrayCat[] = $key;
    ?>
    <div class="col-xs-12 category_item_sx <?php if ($_GET['cat'] == $key) echo 'active' ?>">
      <a id="cat_<?php echo $key ?>" class="a_category" href="/category.php?m=<?php echo $_GET['m'] ?>&c=<?php echo $idCat ?>&cat=<?php echo $key ?>&page=1<?php if (isset($_GET['order_by'])) echo '&order_by='.$_GET['order_by']?><?php if (isset($_GET['sort'])) echo '&sort='.$_GET['sort']?> ">
        <div class="category_tab">
          <?php echo $categoryName['name'] ?>
        </div>
      </a>
    </div>
    <?php
    }
    }
    ?>
  </nav>
</div>