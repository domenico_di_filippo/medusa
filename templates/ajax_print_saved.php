<?php
// risposta prodotto aggiunto al carrello.
?>
<div id="saved_print_pdf" class="modal fade modal_ok" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title cart_added_msg">Stampa o invia tramite email.</h4>
      </div>
      <div class="modal-body">

      <?php

      echo $comiteg->getCustomerEmail($customer_id, $user_id);
      ?>

      <input type="checkbox" name="invia_copy_saved" id="invia_copy_saved" value="1">
      <label for="invia_copy_saved">Invia una copia alla mia email.</label>


      <div id="get_pdf_saved" style="margin-top:15px;float:right"></div>
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>

        <button id="stampa_saved_confirm" class="btn btn-default">Crea PDF e Invia Mail</button>

      </div>
    </div>
  </div>
</div>



<script type="text/javascript">

</script>