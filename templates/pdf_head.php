<?php
$pdf->Cell(70,45,'',0,0);

$pdf->Image($site_logo,13,10,60);
$pdf->SetFont('Arial','B',15);
$pdf->Cell(120, 6, $nome_azienda, 0, 'L');
$pdf->MultiCell(1, 6, '');
$pdf->Cell(70,6,'',0,0);
$pdf->SetFont('Arial','',12);
$pdf->Cell(120, 6, $indirizzo_azienda, 0, 'L');
$pdf->MultiCell(1, 6, '');
$pdf->Cell(70,6,'',0,0);
$pdf->Cell(120, 6, $tel_azienda, 0, 'L');
$pdf->MultiCell(1, 6, '');
$pdf->Cell(70,6,'',0,0);
$pdf->Cell(120, 8, $piva_azienda, 0, 'L');

$pdf->MultiCell(1, 1, '');
$pdf->MultiCell(190, 10, '', 'B', 'L', 0); //border-bottom

$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->SetXY($x, $y);
?>