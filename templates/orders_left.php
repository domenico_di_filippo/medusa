<?php
// navbar sinistra per navigazione ordini.
?>
<div role="navigation" onclick="showHideLeftMobile()">
  <span class="navbar-brand-category">
    <a id="menu-toggle" href="#" class="btn-menu toggle">
      <i class="fa fa-bars" aria-hidden="true"></i><span style="padding:0px 15px"></span>
    </a>
  </span>
</div>
<div id="sidebar-wrapper">
  <nav id="spy">

    <!--div class="col-xs-12 category_item_sx <?php // if ($link_page == '/saved_carts.php') echo 'active' ?>">
      <a class="a_category" href="/saved_carts.php">
        <div class="category_tab">
          CARRELLI SALVATI (<?php // echo $comiteg->getSavedSize($user_id, 'saved_carts') ?>)
        </div>
      </a>
    </div-->

    <div class="col-xs-12 category_item_sx <?php if ($link_page == '/estimates.php') echo 'active' ?>">
      <a class="a_category" href="/estimates.php">
        <div class="category_tab">
          PREVENTIVI (<?php echo $comiteg->getSavedSize($user_id, 'saved_estimates') ?>)
        </div>
      </a>
    </div>

    <div class="col-xs-12 category_item_sx <?php if ($link_page == '/orders.php') echo 'active' ?>">
      <a class="a_category" href="/orders.php">
        <div class="category_tab">
          ORDINI (<?php echo $comiteg->getSavedSize($user_id, 'saved_orders') ?>)
        </div>
      </a>
    </div>

    <div class="col-xs-12 category_item_sx <?php if ($link_page == '/invoices.php') echo 'active' ?>">
      <a class="a_category" href="/invoices.php">
        <div class="category_tab">
          FATTURE
        </div>
      </a>
    </div>

    <div class="col-xs-12 category_item_sx <?php if ($link_page == '/top_seller.php') echo 'active' ?>">
      <a class="a_category" href="/top_seller.php">
        <div class="category_tab">
          TOP SELLER
        </div>
      </a>
    </div>

    <div class="col-xs-12 category_item_sx <?php if ($link_page == '/to_invoice.php') echo 'active' ?>">
      <a class="a_category" href="/to_invoice.php">
        <div class="category_tab">
          DA FATTURARE (<?php  echo count($comiteg->getToInvoiceSize($user_id)) ?>)
        </div>
      </a>
    </div>

  </nav>
</div>