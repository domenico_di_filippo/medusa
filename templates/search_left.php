<?php
// navbar sinistra per navigazione risultati ricerca.
?>
<div role="navigation"  onclick="showHideLeftMobile()">
  <span class="navbar-brand-category">
    <a id="menu-toggle" href="#" class="btn-menu toggle">
      <i class="fa fa-bars" aria-hidden="true"></i><span style="padding:0px 15px">FILTRA CATEGORIE</span>
    </a>
  </span>
</div>
<div id="sidebar-wrapper">
  <nav id="spy">
    <?php
    $filterCategory = $comiteg->getCategoryFilter($query, $cat);
    foreach ($filterCategory as $key => $categoryName) {
    $arrayCat[] = $key;
    ?>
    <div class="col-xs-12 category_item_sx <?php if ($_GET['filter'] == $key) echo 'active' ?>">
      <a id="cat_<?php echo $key ?>" class="a_category" href="/search.php?q=<?php echo $querystring ?>&page=1<?php if (isset($_GET['order_by'])) echo '&order_by='.$_GET['order_by']?><?php if (isset($_GET['sort'])) echo '&sort='.$_GET['sort']?>&filter=<?php echo $key ?>">
        <div class="category_tab">
          <?php echo $categoryName ?>
        </div>
      </a>
    </div>
    <?php
    }
    ?>
  </nav>
</div>