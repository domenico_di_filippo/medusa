<?php
// popup invia o stampa scheda.
?>
<div id="product_print_pdf" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title cart_added_msg">Stampa o invia tramite email.</h4>
      </div>
      <div class="modal-body">
      <label>INSERISCI EMAIL DESTINATARIO</label>
      <input name="email" type="email" id="email_to_pdf_form" class="input_modal">
      <input type="checkbox" name="invia_copy" id="invia_copy" value="1">
      <label for="invia_copy">Invia una copia alla mia email.</label>
      <div id="get_pdf_product" style="margin-top:15px;float:right"></div>
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>

        <button id="stampa_scheda" class="btn btn-default">Crea PDF e Invia Mail</button>

      </div>
    </div>
  </div>
</div>

<?php
// form compilato in automatico per l'invio dei prodotti nella tabella cart.
?>

<form method="POST" action="" style="display:none" class="form_print" id="form_print">
  <input name="inviami_copia" id="inviami_copia" type="hidden" value="">
  <input name="email" id="product_email_pdf" type="hidden" value="">
  <input name="product_name" id="product_name_pdf" type="hidden" value="<?php echo $_product['info']['name'] ?>">
  <input name="product_image" id="product_image_pdf" type="hidden" value="<?php echo $_product['media']['image'] ?>">
  <input name="product_id" id="product_id_pdf" type="hidden" value="<?php echo $_product['info']['id'] ?>">
  <input name="product_ean" id="product_ean_pdf" type="hidden" value="<?php echo $_product['attributes']['ean'] ?>">
  <input name="product_mpn" id="product_mpn_pdf" type="hidden" value="<?php echo $_product['attributes']['mpn'] ?>">
  <input name="product_weight" id="product_weight_pdf" type="hidden" value="<?php echo $_product['attributes']['weight'] ?>">
  <input name="product_brand" id="product_brand_pdf" type="hidden" value="<?php echo $_product['attributes']['brand'] ?>">
  <input name="product_cod_1" id="product_cod_1_pdf" type="hidden" value="<?php echo $_product['attributes']['cod_alt_1'] ?>">
  <input name="product_cod_2" id="product_cod_2_pdf" type="hidden" value="<?php echo $_product['attributes']['cod_alt_2'] ?>">
  <input name="product_price" id="product_price_pdf" type="hidden" value="<?php echo $product_price ?>">
  <input name="product_description" id="product_description_pdf" type="hidden" value="<?php echo $_product['info']['description'] ?>">

</form>

<script type="text/javascript">

</script>