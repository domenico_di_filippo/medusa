<?php
//toolbar per navigazione risultati ricerca.
$pages = count($SearchSize) +1;
if ($_GET['page'] == null) {
$_GET['page'] = 1;
}
?>
<div class="col-xs-12 container_toolbar">
  <form method="GET" action="">
    <div class="col-xs-12 col-md-4 toolbar_container_sx">
      <span>Ordina per:</span>
      <input type="hidden" name="q" value="<?php echo $_GET['q'] ?>">
      <input type="hidden" name="filter" value="<?php echo $_GET['filter'] ?>">
      <!--input type="hidden" name="page" value="1"-->
      <select name="order_by" onchange="this.form.submit()">
        <option <?php if ($_GET['order_by'] == 'price') echo 'selected="selected"' ?> value="price">Prezzo</option>
        <option <?php if ($_GET['order_by'] == 'name') echo 'selected="selected"' ?> value="name">Nome</option>
      </select>
      <?php
      if ($_GET['sort'] == 'ASC' || !isset($_GET['sort'])) {
      ?>
      <input style="display:none" id="checkDESC" name="sort" type="checkbox" value="DESC" onclick="changeCheckName(this)" onchange="this.form.submit()">
      <label for="checkDESC" class="label_asc_desc">
        <i class="fa fa-arrow-down" aria-hidden="true"></i>
      </label>
      <?php
      }
      else {
      ?>
      <input style="display:none" id="checkASC" name="sort" type="checkbox" value="ASC" onclick="changeCheckName(this)" onchange="this.form.submit()">
      <label for="checkASC" class="label_asc_desc">
        <i class="fa fa-arrow-up" aria-hidden="true"></i>
      </label>
      <?php
      }
      ?>
    </div>
    <?php
    if (count($SearchSize) > 5) {
    ?>
    <div class="col-xs-12 col-md-8 toolbar_container_dx_search">
      <div class="label_page_search">di <?php echo count($SearchSize); ?></div>
        <select style="float:right" name="page" onchange="this.form.submit()">
          <?php
          if (is_array($SearchSize)) {
          $a = 0;
          foreach ($SearchSize as $page) {
          $a++;
          ?>
          <option value="<?php echo $a ?>" <?php if ($_GET['page'] == $a) echo 'selected="selected"' ?>"><?php echo $a ?></option>
          <?php
          }
          }
          else {
          ?>
          <option value="0">0</option>
          <?php
          }
          ?>
        </select>
        <?php
      }
      else {
      ?>
      <div class="col-xs-12 col-md-8 toolbar_container_dx">
        <?php
        foreach ($SearchSize as $page) {
        $pages--;
        ?>
        <div class="page_link_container">
          <a class="page_link <?php if ($_GET['page'] == $pages) echo 'active_page' ?>" href="/search.php?q=<?php echo $querystring ?>&page=<?php echo $pages ?><?php if (isset($_GET['filter'])) echo '&filter='.$_GET['filter']?><?php if (isset($_GET['order_by'])) echo '&order_by='.$_GET['order_by']?><?php if (isset($_GET['sort'])) echo '&sort='.$_GET['sort']?> ">
            <?php echo $pages ?>
          </a>
        </div>
        <?php
        }
      }
      ?>
      <div class="label_page_search">Pagina:</div>
    </div>
  </form>
</div>