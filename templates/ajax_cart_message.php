<?php
// risposta prodotto aggiunto al carrello.
?>
<div id="myModal" class="modal fade modal_ok" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title cart_added_msg">Prodotto aggiunto al carrello.</h4>
      </div>
      <div class="modal-footer">

        <button id="modal_response" type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>

        <a href="/cart.php">
          <button id="go_to_cart" class="btn btn-default">Vai al carrello</button>
        </a>

      </div>
    </div>
  </div>
</div>
<?php
// form compilato in automatico per l'invio dei prodotti nella tabella cart.
?>
<form method="POST" action="" style="display:none" class="form_cart" id="form_cart">
  <input name="user_name" id="user_form" type="hidden">
  <input name="id_prod" id="id_form" type="hidden">
  <input name="qty" id="qty_form" type="hidden">
  <input name="price" id="price_form" type="hidden">
  <input name="cost" id="cost_form" type="hidden">
</form>