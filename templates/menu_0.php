<?php
// bottom menu per utente con ruolo == 0
?>
<div class="col-xs-12 position_menu">

  <div class="menu_item_container">

    <a href="/shop.php">
      <div class="menu_item separator_left">
        <div class="menu_icon"><i class="fa fa-list" aria-hidden="true"></i></div>
        <span class="menu_text">Articoli</span>
      </div>
    </a>

    <a href="/customers.php">
      <div class="menu_item separator_left">
        <div class="menu_icon"><i class="fa fa-users" aria-hidden="true"></i></div>
        <span class="menu_text">Clienti</span>
      </div>
    </a>

    <!--div class="menu_item minicart_button separator_left" data-toggle="modal" data-target="#minicart">
      <div id="number_items_cart"><?php echo $comiteg->getCartItems($_SESSION['login']) ?></div>
      <div class="menu_icon"><i class="fa fa-shopping-basket" aria-hidden="true"></i></div>
      <span class="menu_text">Carrello</span>
    </div-->

    <a href="/cart.php">
      <div class="menu_item minicart_button separator_left">
      <div id="number_items_cart"><?php echo $comiteg->getCartItems($_SESSION['login']) ?></div>
        <div class="menu_icon "><i class="fa fa-shopping-basket" aria-hidden="true"></i></div>
        <span class="menu_text">Carrello</span>
      </div>
    </a>

    <a href="/orders.php">
      <div class="menu_item separator_left">
        <div class="menu_icon"><i class="fa fa-cube" aria-hidden="true"></i></div>
        <span class="menu_text">Ordini</span>
      </div>
    </a>

    <!--div class="menu_item separator_left">
      <div class="menu_icon"><i class="fa fa-user" aria-hidden="true"></i></div>
      <span class="menu_text">Utenti</span>
    </div-->

    <a href="/admin.php?product=update">
      <div class="menu_item separator_left">
        <div class="menu_icon"><i class="fa fa-cog" aria-hidden="true"></i></div>
        <span class="menu_text">Impostazioni</span>
      </div>
    </a>

    <a href="/logout.php">
      <div class="menu_item separator_left separator_right">
        <div class="menu_icon"><i class="fa fa-unlock-alt" aria-hidden="true"></i></div>
        <span class="menu_text">Logout</span>
      </div>
    </a>

  </div>
</div>
<?php include('cart/render_minicart.php') ?>