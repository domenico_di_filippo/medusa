<?php
// costruisce la list view dei prodotti in categorie e ricerca.
if (is_array($_poductCollection)) {
$_poductCollection = $_poductCollection;
}
else {
$_poductCollection = array();
}

if (count($_poductCollection) > 0) {

foreach ($_poductCollection as $_product) {

if ($_product['inventory']['qty'] <= 0) {
$classQty = 'qtyred';
}
else if ($_product['inventory']['qty'] <= 10) {
$classQty = 'qtyorange';
}
else {
$classQty = 'qtynormal';
}
if ($_product['prices']['variation'] == '+') {
$priceIcon = '<span class="pluscircle"><i class="fa fa-plus-circle" aria-hidden="true"></i></span>';
}
else if ($_product['prices']['variation'] == '-') {
$priceIcon = '<span class="minuscircle"><i class="fa fa-minus-circle" aria-hidden="true"></i></span>';
}
else {
$priceIcon = '';
}
?>
<div class="col-xs-12 col-md-12 product_list_container" style="font-size:13px">
  <div class="col-xs-12 container_product_orders">
    <div class="col-xs-12 col-md-1 product_list_container_image">
      <img class="product_list_image" src="<?php echo $_product['media']['image'] ?>" onerror="this.onerror=null;this.src='<?php echo $placeholder_img ?>';">
    </div>
    <div class="col-xs-12 col-md-10">
      <div class="product_list_title_ordered">
        <?php

        if (strlen($_product['info']['name']) >= 70) {
        $_product_name = substr($_product['info']['name'], 0, 67).'...';
        }
        else {
        $_product_name = $_product['info']['name'];
        }
        ?>
        <?php echo '<span class="occurr_product">'.$_product['info']['occurr'].'</span> '.$_product['info']['id'].' | '.$_product_name ?>
      </div>

      <div class="col-xs-12 col-md-5" style="padding-left:0px;">
        Conf: <span><strong><?php echo $_product['inventory']['min_qty'] ?> pz</strong></span> -
        Disp: <span class="<?php echo $classQty ?>"><strong><?php echo $_product['inventory']['qty'] ?> pz</strong></span>
        <?php
        if ($_product['inventory']['reorder'] !== '0') {
        ?>
        &nbsp;(+<span><strong><?php echo $_product['inventory']['reorder'] ?></strong></span>)
        <?php
        }
        ?>
      </div>
      <?php
      $isInCart = $comiteg->getProductCartPrice($_product['info']['id'], $_SESSION['login']);
      if ($isInCart !== NULL) {
      $product_price = $isInCart;
      }
      else {
      $product_price = $_product['inventory']['average_price'];
      }
      ?>
      <div class="col-xs-12 col-md-7">
        <span class="price_list" "><span id="changepr_<?php echo $_product['info']['id'] ?>"><?php echo $product_price ?></span> €/pz</span>
        <?php echo $priceIcon.' &nbsp; ULTIMO ACQUISTO: <strong>'.$_product['info']['date'].'</strong>' ?>
      </div>
      </div>
      <div class="add_to_cart_links col-md-1 col-xs-12">
        <!--a href="/product.php?id=<?php //echo $_product['info']['id'] ?>">
          <span class="go_to_product">
            <i class="fa fa-external-link-square" aria-hidden="true"></i>
          </span>
        </a-->
        <span class="open_cart"  data-toggle="modal" data-target="#modal_<?php echo $_product['info']['id']?>">
          <i class="fa fa-shopping-basket" aria-hidden="true"></i>
        </span>

    </div>
  </div>
</div>
<?php include(__ROOT__.'/templates/category_ajax_cart.php') // contiene il popup del carrello ajax ?>
<?php
}
}
else {
?>
<div class="col-xs-12">
<h2>Non ci sono prodotti</h2>
</div>
<?php
}
$array_stat = $comiteg->getTopSellerStats($_poductCollection);

$tot_price = number_format($array_stat['total_prices'], 2, '.', '');
$tot_costs = number_format($array_stat['total_costs'], 2, '.', '');

$total_gain = number_format(($tot_price - $tot_costs), 2, '.', '');
$total_invoices = count($_poductCollection);
/*
echo '<pre>';
print_r($array_stat);
echo '</pre>';
*/

?>
<span id="total_span_invoices" style="display:none"><?php echo $total_invoices ?></span>
<span id="total_span_prices" style="display:none"><?php echo $tot_price ?></span>
<span id="total_span_costs" style="display:none"><?php echo $tot_costs ?></span>
<span id="total_span_gain" style="display:none"><?php echo $total_gain ?></span>
<?php
?>