var array_1 = <?php echo $arr_01 ?>;
var array_2 = <?php echo $arr_02 ?>;
new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: array_1,
    datasets: [{
        data: array_2,
        label: "Africa",
        borderColor: "#3e95cd",
        backgroundColor: "#3e95cd",
        fill: false,
        borderWidth: 5
      }, /*{
        data: [282,350,411,502,635,809,947,1402,3700,5267],
        label: "Asia",
        borderColor: "#8e5ea2",
        fill: false
      }, {
        data: [168,170,178,190,203,276,408,547,675,734],
        label: "Europe",
        borderColor: "#3cba9f",
        fill: false
      }, {
        data: [40,20,10,16,24,38,74,167,508,784],
        label: "Latin America",
        borderColor: "#e8c3b9",
        fill: false
      }, {
        data: [6,3,2,2,7,26,82,172,312,433],
        label: "North America",
        borderColor: "#c45850",
        fill: false
      }*/
    ]
  },
  options: {
    title: {
      display: true,
      text: 'World population per region (in millions)'
    }
  }
});