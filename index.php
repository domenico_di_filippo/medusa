<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (isset($_POST['submit']) && !isset($_SESSION['login'])) {
  $response = $comiteg->login($_POST['user'], $_POST['pass'], 'admin_user', 'user');
  if ($response['value'] == '1') {
  $_SESSION['login'] = $_POST['user'];
  ?>
  <meta http-equiv= "Refresh" content="0;URL=/shop.php">
  <?php
  die;
  }
}
else if (isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/shop.php">
<?php
die;
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Medusaufficio | Login</title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>
<body>

  <header>
  </header>

  <div class="container">
    <div class="row">
      <div class="login_main">

        <div class="col-xs-12 title_h1">
          <h1 class="title_h1_border">Login</h1>
        </div>
        <form method="POST" action="">
          <div class="col-xs-12 input_login_div">
            <i class="fa fa-user login_icon" aria-hidden="true"></i>
            <input class="input_login" name="user" type="text" value="<?php echo $_POST['user'] ?>">
          </div>
          <div class="col-xs-12 input_login_div">
            <i class="fa fa-lock login_icon" aria-hidden="true"></i>
            <input class="input_login" name="pass" type="password">
          </div>
          <div class="col-xs-12 input_login_div">
            <button class="button_login" name="submit" type="submit">LOGIN</button>
          </div>
        </form>

        <div id="message_login" class="col-xs-12"><?php echo $response['message'] ?></div>

      </div>
    </div>
  </div>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>

</body>
</html>
