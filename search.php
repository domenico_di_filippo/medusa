<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
$cat = $_GET['filter'];

if (isset($_GET['q'])) {
$query = $_GET['q'];
$querystring = str_replace(' ', '+', $query);
}
else {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Risultati ricerca per "<?php echo $query ?>"</title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>

  <div id="main_container">
    <div id="wrapper">
      <?php
      $SearchSize = $comiteg->getSearchSize($query, $order_by, $cat);

      $counter = 0;
      foreach ($SearchSize as $array) {
        foreach ($array as $single) {
        $a++;
        }
      }
      if ($a == null) {
      $a = 0;
      }
      ?>
      <?php require_once('templates/search_left.php') ?>
      <div class="col-xs-12 breadcrumb_category">
        <?php

        if (!isset($_GET['filter'])) {
        $href_back = '/shop.php';
        }
        else {
        $href_back = '/search.php?q='.$querystring;
        }
        ?>
        <a class="btn btn-default button_back" href="<?php echo $href_back ?>">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </a>
        <?php
        if ($a == 1) {
        $res_plural = 'risultato';
        }
        else {
        $res_plural = 'risultati';
        }
        ?>
        Risultati di ricerca per : "<?php echo $query ?>" (<?php echo $a.' '.$res_plural ?>)
      </div>
      <div class="col-xs-12" id="container_product_xs">
        <?php
        if (!isset($_GET['page'])) {
        $pageNumber = 1;
        }
        else {
        $pageNumber = $_GET['page'];
        }
        if (!isset($_GET['order_by']) && !isset($_GET['sort'])) {
        $order_by = 'price';
        $sort = 'ASC';
        }
        else if (isset($_GET['order_by']) && !isset($_GET['sort'])) {
        $order_by = $_GET['order_by'];
        $sort = 'ASC';
        }
        else if (!isset($_GET['order_by']) && isset($_GET['sort'])) {
        $order_by = 'price';
        $sort = $_GET['sort'];
        }
        else if (isset($_GET['order_by']) && isset($_GET['sort'])) {
        $order_by = $_GET['order_by'];
        $sort = $_GET['sort'];
        }
        $_productCollection = $comiteg->getSearchResuts($query, $pageNumber, $order_by, $sort, $cat);

        include('templates/toolbar_search.php');

        if (count($_productCollection) == 0) {
        ?>
        <div class="col-xs-12 no_search_results">
          <?php echo 'La ricerca <strong>"'.$query.'"</strong> non ha prodotto risultati.'; ?>
        </div>
        <?php
        }
        foreach ($_productCollection as $_product) {
        include('templates/list.php');
        }
        include('templates/toolbar_search.php');
        ?>
      </div>
    </div>
  </div>
  <?php require_once('templates/ajax_cart_message.php') // contiene il popup della rsisposta e il form per database ?>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>

</body>
</html>