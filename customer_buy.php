<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);
$customerName = $comiteg->getTableValue('id,ragione_sociale', 'customers', 'id', 'ragione_sociale', $_GET['customer_id']);
//$customer_id = $comiteg->getNewCustomerId($user_id);
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Prodotti acquistati da <?php echo $customerName ?></title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>
  <div id="main_container">

  <div id="wrapper">

  <?php
  $active_page = '/customer_buy.php';
  require_once('templates/new_customer_left.php');
  ?>

<?php
$link_page = '/orders.php';
$_poductCollection = $comiteg->getProductsOrderedByCustomer($_GET['customer_id'], NULL, NULL);
?>

<div class="anchor_customer" id="products">

    <div class="col-xs-12 breadcrumb_category">

        <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">

          <i class="fa fa-chevron-left" aria-hidden="true"></i>

        </button>
        </a>
      Prodotti acquistati da <strong><?php echo $customerName ?></strong> (<?php echo count($_poductCollection) ?>)
    </div>

<div class="col-md-9 scrolling_x_cart" style="padding-left:0">
<div class="fixed_width_700">

  <div id="searchOrderedProductResult">
  <?php

  if (count($_poductCollection) > 0) {
  foreach ($_poductCollection as $key => $row) {
  $mid[$key]  = $row['info']['occurr'];
  }
  array_multisort($mid, SORT_DESC, $_poductCollection);
  }
  include('templates/list_ordered.php');

  ?>
  </div>

<?php require_once('templates/ajax_cart_message.php') // contiene il popup della risposta e il form per database ?>
</div>
</div>

<div class="col-md-3">
  CERCA PRODOTTI
<?php include('customers/search_input_ordered.php') ?>

</div>


</div>



  </div>



  </div>
  <div id="refresh_saved_cart_orders" style="display:none"></div>
  <div id="refresh_saved_cart_estimates" style="display:none"></div>
  <div id="refresh_saved_cart_saved" style="display:none"></div>
  <?php //require_once('templates/ajax_cart_message.php') // contiene il popup della risposta e il form per database ?>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>
<script type="text/javascript">
$(function() {
    $(document).ajaxComplete(function(){
        $('#update_right_bar').trigger('click');
    });
});
</script>

</body>
</html>
