<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
//$customer_tax = 22;
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Carrello</title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>


  <div id="main_container">
    <div class="col-xs-12 breadcrumb_category">
      <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
      </a>
      Carrello
    </div>
    <div class="col-md-9 scrolling_x_cart">
<div class="fixed_width_700">
<?php
$customer_tax = $comiteg->getTableValue('id,ali_iva', 'customers', 'id', 'ali_iva', $_GET['customer_id']);
$valueCustomerSearch = $comiteg->getTableValue('id,ragione_sociale', 'customers', 'id', 'ragione_sociale', $_GET['customer_id']);

$_items_collection = $comiteg->getMinicartItems($_SESSION['login']);
?>
<form method="POST" id="cart_delete_items" class="cart_delete_items" action="">

<input type="hidden" id="customer_id" name="customer_id" value="<?php echo $_GET['customer_id'] ?>">
<input type="hidden" id="customer_iva" name="customer_iva" value="<?php echo $customer_tax ?>">
        <div id="cartContent">
          <?php include('cart/list_cart.php') ?>
        </div>
</form>
<?php
$tax_amount = $comiteg->getPriceTax(array_sum($totalPrice), $customer_tax) - array_sum($totalPrice);
$gain = array_sum($totalPrice) - array_sum($totalCost);
?>
</div>
    </div>
    <div class="col-md-3">
    <?php
    if (count($_items_collection) > 0){
    if (!isset($_GET['customer_id'])) {
    ?>
      <span id="associato_a">ASSOCIA CARRELLO A CLIENTE</span>
      <button style="display:none;position:absolute;top:0;right:15px" id="rimuovi_associa" type="button" class="invisible_btn">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
      </button>
    <?php
    }
    else {
    ?>
      <span id="associato_a">CARRELLO ASSOCIATO A:<br>
      <strong><?php echo $valueCustomerSearch ?></strong>
      </span>
      <button style="display:block;position:absolute;top:0;right:15px" id="rimuovi_associa" type="button" class="invisible_btn">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
      </button>
    <?php
    }
    ?>
      <?php include('cart/search_input_cart.php') ?>
      <div style="position:relative">
      <div id="select_customer" style="display:none"></div>
      </div>
    <?php
    }
    ?>
      <div class="col-md-12 totals_cart_block effect2">
        SUBTOTALE
        <div class="cart_block_price_container"><strong class="totals_cart_table" id="update_subtotal">€ <?php echo number_format(array_sum($totalPrice), 2, '.', '') ?></strong></div>
        <div class="cart_block_price_container_label">
          IVA
          <span id="update_ali_iva">
          <?php
          if ($customer_tax !== NULL) {
          echo '&nbsp;('.$customer_tax.'%)';
          }
          ?>
          </span>
        </div>
        <div class="cart_block_price_container"><strong class="totals_cart_table" id="update_tax" <?php if ($customer_tax == 0) echo 'style="text-decoration:line-through"' ?>>€ <?php echo number_format($tax_amount, 2, '.', '') ?></strong></div>
        <div class="cart_block_price_container_label">TOTALE</div>
        <div class="cart_block_price_container"><strong class="totals_cart_table_big" id="update_total">€ <?php echo $comiteg->getPriceTax(array_sum($totalPrice), $customer_tax) ?></strong></div>

        <button class="button_info_cart" data-toggle="collapse" data-target="#gain"><i class="fa fa-info-circle" aria-hidden="true"></i></button>

          <div id="gain" class="collapse">
            <div class="cart_block_price_container_label">
              RICAVO STIMATO
              <div class="cart_block_price_container"><strong class="totals_cart_table" id="update_gain">€ <?php echo number_format($gain, 2, '.', '') ?></strong></div>
            </div>
          </div>

      </div>

        <button <?php if (count($_items_collection) == 0) echo 'disabled' ?> id="flush_cart" class="button_action_cart_page btn btn-default">SVUOTA CARRELLO</button>
        <button <?php if (!isset($_GET['customer_id']) || count($_items_collection) == 0) echo 'disabled' ?> id="chiudi_preventivo" class="button_action_cart_page btn btn-default">CHIUDI COME PREVENTIVO</button>
        <button <?php if (!isset($_GET['customer_id']) || count($_items_collection) == 0) echo 'disabled' ?> id="chiudi_ordine" class="button_action_cart_page btn btn-default">CHIUDI COME ORDINE</button>
        <!--button <?php // if (!isset($_GET['customer_id']) || count($_items_collection) == 0) echo 'disabled' ?> id="salva_carrello" class="button_action_cart_page btn btn-default">SALVA CARRELLO</button-->

    </div>
  </div>
  <span onclick="updateCartValues('.get_price')" id="update_right_bar" class="update_right_bar" style="display:none"></span>
  <span id="refresh_cart" style="display:none"></span>
  <span id="refresh_cart_numberItems" style="display:none"></span>

 <?php require_once('templates/ajax_cart_message.php') // contiene il popup della risposta e il form per database ?>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>
<script type="text/javascript">
// bottone carrello non cliccabile
$(document).ready(function() {
$('.minicart_button').css('pointer-events', 'none');
});

// quando finisce ajax update right block prezzi
$(function() {
    $(document).ajaxComplete(function(){
        $('#update_right_bar').trigger('click');
    });
});
</script>
</body>
</html>