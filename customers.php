<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Clienti</title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>

  <div id="main_container">
    <div class="col-xs-12 breadcrumb_category">

      <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
      </a>

      Clienti
    </div>
    <div class="col-md-9 scrolling_x_cart">
    <div class="fixed_width_700">
          <div class="col-md-4 col-xs-4" style="padding-left:0">
          RAGIONE SOCIALE
          </div>
          <div class="col-md-2 col-xs-2">
          CITT&Agrave;
          </div>
          <div class="col-md-2 col-xs-2">
          TEL
          </div>
          <div class="col-md-3 col-xs-3">
          EMAIL
          </div>
          <div class="col-md-1 col-xs-1">
          &nbsp;
          </div>

          <div style="clear:both"></div>
          <hr class="hr_thin">

        <form method="POST" action="" class="customers_form" id="customers_form">
        <div id="customerContent">

          <?php
          $customers_collection = $comiteg->getCustomersResults($user_id, '');
          include('customers/get_customers.php');
          ?>
        </div>
        </form>
    </div>
    </div>
    <div class="col-md-3">
    CERCA CLIENTI
    <?php include('customers/search_input_customers.php') ?>
    <a href="/new_customer.php">
      <button class="button_action_customer_page btn btn-default">AGGIUNGI CLIENTE</button>
    </a>
    </div>


<div id="cancel_customer" class="modal fade modal_ok" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title cart_added_msg">Sei sicuro?</h4>
      </div>
      <div class="modal-footer">

        <button id="modal_no" type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button id="modal_yes" type="button" class="btn btn-default" data-dismiss="modal">Sì</button>


      </div>
    </div>
  </div>
</div>



  </div>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>

</body>
</html>