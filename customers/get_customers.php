<?php
if ($dest_page == NULL) {
$dest_page = '/new_customer.php';
}
if (count($customers_collection) > 0 ) {
foreach ($customers_collection as $key => $customer) {
?>
<div class="col-md-4 col-xs-4" style="padding-left:0">
<strong><?php echo $customer['ragione_sociale'] ?></strong>
</div>
<div class="col-md-2 col-xs-2">
<?php echo $customer['citta'] ?>
</div>
<div class="col-md-2 col-xs-2">
<?php echo $customer['tel'] ?>
</div>
<div class="col-md-3 col-xs-3">
<?php echo $customer['email'] ?>
</div>
<div class="col-md-1 col-xs-1">
<a class="go_customer" href="<?php echo $dest_page ?>?customer_id=<?php echo $key ?><?php echo $to_inv ?>">
<button type="button" class="invisible_btn minicart_delete_label">
<i class="fa fa-pencil-square" aria-hidden="true"></i>
</button>
</a>
</div>
<!--div class="col-md-1 col-xs-1">
<div class="minicart_item_delete">
<button id="delete_<?php echo $key ?>" type="submit" name="delete[]" class="invisible_btn_customers minicart_delete_label" value="<?php echo $key ?>">
<i class="fa fa-times-circle" aria-hidden="true"></i>
</button>
</div>
</div-->
<div style="clear:both"></div>
<hr class="hr_thin">
<?php
}
}
else {
?>
<div class="col-md-12" style="padding:0">
<h2>Nessun risultato</h2>
</div>
<?php
}
?>