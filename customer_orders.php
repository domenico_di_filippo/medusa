<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
$user_id = $comiteg->getTableValue('*', 'admin_user', 'user', 'id', $_SESSION['login']);
$customerName = $comiteg->getTableValue('id,ragione_sociale', 'customers', 'id', 'ragione_sociale', $_GET['customer_id']);
//$customer_id = $comiteg->getNewCustomerId($user_id);
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title>Ordini di <?php echo $customerName ?></title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>
  <div id="main_container">

  <div id="wrapper">

  <?php
  $active_page = '/customer_orders.php';
  require_once('templates/new_customer_left.php');
  ?>

<?php
$link_page = '/orders.php';
$_saved_carts_collection = $comiteg->getSavedOrdersCustomer('saved_orders', 'saved_order_items', $user_id, $_GET['customer_id']);

if (!isset($_GET['page'])) {
$page = 1;
}
else {
$page = $_GET['page'];
}
?>

<div class="anchor_customer" id="orders">

    <div class="col-xs-12 breadcrumb_category">

        <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">

          <i class="fa fa-chevron-left" aria-hidden="true"></i>

        </button>
        </a>
      Ordini di <strong><?php echo $customerName ?></strong> (<?php echo count($_saved_carts_collection) ?>)
    </div>

    <?php
    /*echo '<pre>';
    print_r($_saved_carts_collection);
    echo '</pre>';*/
    if (!is_array($_saved_carts_collection)) {
    $_saved_carts_collection = array();
    }
    $_saved_carts_collection = array_chunk($_saved_carts_collection, $page_size, true);

    if (count($_saved_carts_collection) > 0) {
    $_saved_carts_collection = array_combine(range(1, count($_saved_carts_collection)), array_values($_saved_carts_collection));
    }
    ?>

    <?php $comiteg->getCustomerPagination($_saved_carts_collection, '/customer_orders.php', $page) ?>



<div class="col-xs-12">
<div class="col-md-9 scrolling_x_cart" style="padding-left:0">
<div class="fixed_width_700">
          <div class="col-md-1 col-xs-1 cart_label_tab" style="padding-left:0;text-align:right">
          ORD. #
          </div>
          <div class="col-md-4 col-xs-4">
          CLIENTE
          </div>
          <div class="col-md-2 col-xs-2" style="text-align:right">
          IMPORTO
          </div>
          <div class="col-md-3 col-xs-3" style="text-align:center">
          DATA
          </div>
          <div class="col-md-1 col-xs-1">
          &nbsp;
          </div>
          <div class="col-md-1 col-xs-1">
          &nbsp;
          </div>
          <div style="clear:both"></div>
          <hr class="hr_thin">
<form method="POST" action="" class="saved_carts_form saved_carts_form_orders" id="saved_carts_form">
<input type="hidden" name="table_1" value="saved_orders">
<input type="hidden" name="table_2" value="saved_order_items">
<input type="hidden" name="is_page" id="is_page" value="<?php echo $link_page ?>">
<input type="hidden" name="user_id" value="<?php echo $user_id ?>">
<input type="hidden" name="customer_id" value="<?php echo $_GET['customer_id'] ?>">
<div id="savedCartsContent_orders">
<?php include('cart/list_saved_carts.php') ?>
</div>
</form>
</div>
</div>

<?php
$array_stat = $comiteg->getOrdersStats($user_id, $_GET['customer_id']);
  $prices = array();
  $costs = array();
foreach ($array_stat as $orders) {

  foreach ($orders['items'] as $order) {
  $prices[] = $order['total_price'];
  $costs[] = $order['total_cost'];
  }
}

?>
      <div class="col-md-3 col-xs-12 totals_cart_block effect2">
        ORDINI EVASI
        <div class="cart_block_price_container">
          <strong class="totals_cart_table">
            <?php echo count($array_stat) ?>
          </strong>
        </div>

        <div class="cart_block_price_container_label">
        TOTALE VENDITE
        </div>
        <div class="cart_block_price_container">
          <strong class="totals_cart_table">
            € <?php echo number_format(array_sum($prices),2 ,'.', '') ?>
          </strong>
        </div>

        <div class="cart_block_price_container_label">
        TOTALE COSTI
        </div>
        <div class="cart_block_price_container">
          <strong class="totals_cart_table">
            € <?php echo number_format(array_sum($costs),2 ,'.', '') ?>
          </strong>
        </div>

        <div class="cart_block_price_container_label">
        TOTALE RICAVI
        </div>
        <div class="cart_block_price_container">
          <strong class="totals_cart_table_big">
            € <?php echo number_format((array_sum($prices) - array_sum($costs)),2 ,'.', '') ?>
          </strong>
        </div>

      </div>

</div>
</div>



  </div>



  </div>
  <div id="refresh_saved_cart_orders" style="display:none"></div>
  <div id="refresh_saved_cart_estimates" style="display:none"></div>
  <div id="refresh_saved_cart_saved" style="display:none"></div>
  <?php //require_once('templates/ajax_cart_message.php') // contiene il popup della risposta e il form per database ?>

  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>
<?php
if (!isset($_GET['customer_id'])) {
?>
<script type="text/javascript">
// cancella reserved_id nella tabella customers
$(window).bind('beforeunload', function() {
$.ajax({
url: '/customers/remove_reserved_id.php',
async:false
});
});

$('form#save_customer').submit(function() {
   $(window).unbind('beforeunload');
});

</script>
<?php
}
?>
<script type="text/javascript">
$(function() {
    $(document).ajaxComplete(function(){
        $('#update_right_bar').trigger('click');
    });
});
</script>
<?php

    if ($_GET['ref'] == 'to_invoice') {
    ?>
    <script type="text/javascript">
    $('#invoiceproducts_left').trigger('click');
    </script>
    <?php
    }

    if ($_GET['section'] == 'orders') {
    ?>
    <script type="text/javascript">
    $('#orders_left').trigger('click');
    </script>
    <?php
    }

    if ($_GET['section'] == 'estimates') {
    ?>
    <script type="text/javascript">
    $('#estimates_left').trigger('click');
    </script>
    <?php
    }

?>

</body>
</html>
