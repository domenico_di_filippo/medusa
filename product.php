<?php
session_start();
require_once ('classes/class.main.php');
$comiteg = new Main;

if (!isset($_SESSION['login'])) {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}

if (isset($_GET['id'])) {
$id_prod = $_GET['id'];
$_product_array = $comiteg->getProduct($id_prod);
$_product = $_product_array[$id_prod];
}
else {
?>
<meta http-equiv= "Refresh" content="0;URL=/">
<?php
die;
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
<title><?php echo $_product['info']['name'] ?></title>
<meta charset="utf-8">
<meta name="keywords" content="medusaufficio">
<meta name="description" content="medusaufficio">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<?php require_once('templates/head.php') ?>
</head>

<body>

  <header>
    <?php require_once ('templates/search.php') ?>
  </header>

  <div id="main_container">
    <div class="col-xs-12 breadcrumb_category">
      <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <button class="btn btn-default button_back">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>
      </a>
      <?php
      $cat_names = explode('/', $_product['info']['categories']);

      foreach ($cat_names as $categoryIds) {
      $array_convert[] = $comiteg->getCategoryName($categoryIds);
      }
      echo implode('&nbsp;  <i class="fa fa-angle-right" aria-hidden="true"></i>  &nbsp;', $array_convert);
      ?>
    </div>
    <div class="col-xs-12">
      <div class="col-md-4 col-xs-12 product_image_container effect2">
        <img style="width:100%" src="<?php echo $_product['media']['image'] ?>" onerror="this.onerror=null;this.src='<?php echo $placeholder_img ?>';">
      </div>

      <div class="col-md-8 col-xs-12 product_title">
        <h2 class="product_title"><?php echo $_product['info']['name'] ?></h2>
        <hr>
        <?php
        $isInCart = $comiteg->getProductCartPrice($_product['info']['id'], $_SESSION['login']);
        if ($isInCart !== NULL) {
        $product_price = $isInCart;
        }
        else {
        $product_price = $_product['prices']['price'];
        }
        if ($_product['inventory']['variation'] == '+') {
        $priceIcon = '<span class="pluscircle"><i class="fa fa-plus-circle" aria-hidden="true"></i></span>';
        }
        else if ($_product['inventory']['variation'] == '-') {
        $priceIcon = '<span class="minuscircle"><i class="fa fa-minus-circle" aria-hidden="true"></i></span>';
        }
        else {
        $priceIcon = '';
        }
        ?>
        <div class="col-xs-12 col-md-6">
          <h2 class="h2_price product_price">
            <span id="changepr_product_<?php echo $_product['info']['id'] ?>"><?php echo $product_price ?></span> €/pz <span class="icon_variation_product"><?php echo $priceIcon ?></span>
          </h2>


        <div class="product_info"><strong>CODICE: <?php echo $_product['info']['id'] ?></strong></div>
        <?php
        if ($_product['attributes']['cod_alt_1'] !== '') {
        ?>
        <div class="product_info">CODICE ALT. 1: <?php echo $_product['attributes']['cod_alt_1'] ?></div>
        <?php
        }
        if ($_product['attributes']['cod_alt_2'] !== '') {
        ?>
        <div class="product_info">CODICE ALT. 2: <?php echo $_product['attributes']['cod_alt_2'] ?></div>
        <?php
        }
        if ($_product['attributes']['ean'] !== '') {
        ?>
        <div class="product_info">EAN: <?php echo $_product['attributes']['ean'] ?></div>
        <?php
        }
        if ($_product['attributes']['mpn'] !== '') {
        ?>
        <div class="product_info">MPN: <?php echo $_product['attributes']['mpn'] ?></div>
        <?php
        }
        if ($_product['attributes']['weight'] !== '') {
        ?>
        <div class="product_info">PESO: <?php echo $_product['attributes']['weight'] ?> Kg/pz</div>
        <?php
        }
        if ($_product['attributes']['brand'] !== '') {
        ?>
        <div class="product_info">BRAND: <?php echo $_product['attributes']['brand'] ?></div>
        <?php
        }
        ?>




        </div>
        <div class="col-xs-12 col-md-6 cart_product_container">
          <span style="width:auto;" class="open_cart"  data-toggle="modal" data-target="#modal_<?php echo $_product['info']['id']?>">
            &nbsp;<i class="fa fa-shopping-basket" aria-hidden="true"></i> AGGIUNGI AL CARRELLO&nbsp;
          </span>
          <br>
          <button data-toggle="modal" data-target="#product_print_pdf" style="float:right" class="btn btn-default">STAMPA SCHEDA PRODOTTO</button>
        </div>
        <div style="clear:both"></div>
        <?php include('templates/category_ajax_cart.php') // contiene il popup del carrello ajax ?>
        <?php include('templates/ajax_print_pdf.php') ?>

        <hr>
        <div class="product_description"><?php echo $_product['info']['description'] ?></div>
      </div>
    </div>
  </div>
  <?php require_once('templates/ajax_cart_message.php') // contiene il popup della rsisposta e il form per database ?>
  <?php include('templates/ajax_print_pdf.php') ?>
<script type="text/javascript">
// popola '#form_cart' per scrittura database
/*function addToCart(el) {
  var idProd = $(el).attr('id').split('_')[1];

  var user = $('#user_'+idProd).val();
  var id = $('#id_'+idProd).val();
  var qty = $('#qty_'+idProd).val();
  var price = $('#price_'+idProd).val();
  var cost = $('#cost_'+idProd).val();

  $('#user_form').val(user);
  $('#id_form').val(id);
  $('#qty_form').val(qty);
  $('#price_form').val(price);
  $('#cost_form').val(cost);
}

// invia il '#form_cart' per scrittura e restituisce le risposte
$(function () {
  $('.form_cart').on('submit', function (e) {

  e.preventDefault();

  $.ajax({
    type: 'post',
    url: '/cart/add_cart_list.php',
    data: $('#form_cart').serialize(),
    success: function () {
      $('.modal_cart').modal('hide');

      setTimeout(function(){
        $('.modal_ok').modal('show');
        $('#update_minicart_response').trigger('click'); //footer.php
        },500);
      },
      error: function () {
        alert("Problema con il carrello");
      }
    });
  });
});*/
</script>
  <footer>
    <?php require_once('templates/footer.php') ?>
  </footer>

</body>
</html>